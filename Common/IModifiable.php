<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.01.2014
 * File: IModifiable.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Common;

/**
 * Description of IModifiable
 * 
 * @package WebStatic\Core
 * @name IModifiable
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
interface IModifiable
{

    public function getCreated();

    public function getModified();
}
