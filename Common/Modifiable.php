<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.01.2014
 * File: Modifiable.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Common;

/**
 * Description of Modifiable
 *
 * @package WebStatic\Core
 * @name Modifiable
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
abstract class Modifiable extends DatabaseObject implements IModifiable
{
    protected $Created;

    public function getCreated()
    {
        return $this->Created;
    }

    protected $Modified;

    public function getModified()
    {
        return $this->Modified;
    }

    protected function _Load(&$procedureName, array &$paramsArray, array &$memberRefArray)
    {
        $memberRefArray [] = &$this->Created;
        $memberRefArray [] = &$this->Modified;
    }
}
