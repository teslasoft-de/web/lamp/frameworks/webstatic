<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.01.2014
 * File: Expirable.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Common;

/**
 * Description of Expirable
 *
 * @package WebStatic\Core
 * @name Expirable
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
abstract class Expirable extends Modifiable implements IExpirable
{
    protected $Release;

    public function getRelease()
    {
        return $this->Release;
    }

    protected $Expire;

    public function getExpire()
    {
        return $this->Expire;
    }

    /**
     * @param       $procedureName
     * @param array $paramsArray
     * @param array $memberRefArray
     */
    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        $memberRefArray []= &$this->Release;
        $memberRefArray []= &$this->Expire;
        parent::_Load( $procedureName, $paramsArray, $memberRefArray );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        $paramsArray []= $this->Release;
        $paramsArray []= $this->Expire;
    }
}
