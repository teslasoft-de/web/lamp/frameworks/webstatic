<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 08.04.2017
 * Time: 19:02
 */

namespace WebStatic\Common;

use AppStatic\Collections\ArrayObjectPropertyBase;

class PropertyDictionary extends ArrayObjectPropertyBase
{
    /**
     * PropertyDictionary constructor.
     */
    public function __construct( array $input = null )
    {
        parent::__construct($input);
    }
}