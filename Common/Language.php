<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 08.04.2017
 * File: Language.php
 * Encoding: UTF-8
 * Project: WebStatic
 * */

namespace WebStatic\Common;

use AppStatic\Core\PropertyBase;
use AppStatic\IO\Path;
use AppStatic\Web\HttpUtility;

define( 'WebStatic\Common\LANGUAGE_SUFFIX', 'language_' );

abstract class Language extends PropertyBase
{
    /**
     * @var PropertyDictionary
     */
    public static $Default;

    /**
     * @var PropertyDictionary
     */
    public static $Error;

    /**
     * @var PropertyDictionary
     */
    public static $Menu;

    /**
     * @var Language
     */
    public static $Instance;

    function __construct()
    {
        $this->Load( 'Default' );
        $this->Load( 'Error' );
        $this->Load( 'Menu' );
    }

    public static function Register()
    {
        $class = new \ReflectionClass( self::getClassName() );
        self::$Instance = $class->newInstance();
    }

    public function Load( $locale )
    {
        $langVar = LANGUAGE_SUFFIX . strtolower( $locale );
        $langCode = '.';

        // Try get user language from webstatic system
        global $domain;

        if (defined( 'SITE_LANGUAGE' ))
            $lang = SITE_LANGUAGE;

        if (!isset($lang) && isset($domain) && $domain->getID()) {
            $site = $domain->getSite( false );
            $lang = $site->getUserDefinedLanguage()
                ? $site->getUserDefinedLanguage()
                : $site->getUserLanguage();
        };

        // Try get user language from HTTP accept languages
        if (!isset($lang)) {
            foreach (HttpUtility::GetHttpAcceptLanguages() as $lang) {
                switch ($lang->Country) {
                    case 'en':
                        break(2);
                    default:
                        $lang = $lang->Country;
                        break(2);
                }
            }
        }

        // Try loacate language file
        if (is_file( \WebStatic\LANGUAGE_PATH . strtolower( $locale ) . '.' . $lang . '.php' ))
            $langCode .= $lang . '.';

        // Load locale
        /** @noinspection PhpIncludeInspection */
        require_once \WebStatic\LANGUAGE_PATH . strtolower( $locale ) . $langCode . 'php';
        // TODO: Add exception handling for property access and point to missing property declaration.
        $this::$$locale = $$langVar;
    }
}

// Locate and load language resource.
$filename = DOCUMENT_ROOT . '/Common/' . Path::GetFileName( __FILE__ );
if (is_file( $filename ))
    /** @noinspection PhpIncludeInspection */
    require_once $filename;