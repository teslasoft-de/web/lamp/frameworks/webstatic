<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.01.2014
 * File: DatabaseObject.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Common;

use AppStatic\Collections\ArrayObjectPropertyBase;
use AppStatic\Core\ExceptionBase;
use AppStatic\Data\MySql\StoredProcedure;
use Exception;

/**
 * The Base class for all WebStatic database objects for querying and persisting.
 *
 * @package WebStatic\Core
 * @name DatabaseObject
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
abstract class DatabaseObject extends ArrayObjectPropertyBase
{
    protected $ID;

    /**
     * Returns the ID of the database object.
     * @return integer
     */
    public function getID()
    {
        return $this->ID;
    }

    abstract function getName();

    /**
     * @param $procedureName
     * @param array $paramsArray
     * @param array $memberRefArray
     * @return void
     */
    protected abstract function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray );

    /**
     * @throws DataBaseObjectException
     */
    public function Load()
    {
        $procedureName = '';
        $paramsArray = array();
        $memberRefArray = array();
        $this->_Load( $procedureName, $paramsArray, $memberRefArray );
        $this->QueryObject( $this, $procedureName, $paramsArray, $memberRefArray );
    }

    /**
     * Returns a DatabaseObject instance type of the calling object where this method has been invoked on.
     * @param int $id DataBaseObject ID and optional constructor parameters.
     * @return DatabaseObject
     * @throws DataBaseObjectException
     */
    public static function QueryByID( $id )
    {
        $className = self::getClassName();
        $args = func_get_args();
        array_shift( $args );
        try {
            $refClass = new \ReflectionClass( $className );
            /* @var $obj DataBaseObject */
            $obj = $refClass->newInstanceArgs( $args );
        } catch (Exception $ex) {
            throw new DataBaseObjectException( "$className cannot be instantiated. Specify all required constructor parameters or use an existing factory function from the parent class.", E_USER_ERROR, $ex );
        }

        $procedureName = '';
        $paramsArray = array();
        $memberRefArray = array();
        $obj->_Load( $procedureName, $paramsArray, $memberRefArray );

        // Set all remaining parameters to null.        
        $paramsArray = array_fill( 0, count( $paramsArray ), null );
        $paramsArray[ 0 ] = $id;

        $obj->QueryObject( $obj, $procedureName, $paramsArray, $memberRefArray, false );

        return $obj;
    }

    /**
     * @param StoredProcedure $procedure
     * @param                 $dbo
     * @param array           $memberRefArray
     * @param bool            $allowEmptyResult Determines if empty queries should be treated as errors.
     *
     * @return bool
     * @throws Exception
     */
    private static function Fetch( $procedure, $dbo, array $memberRefArray, $allowEmptyResult = true )
    {
        // Prepend object id member reference.
        array_unshift( $memberRefArray, null );
        $memberRefArray [0] = &$dbo->ID;

        // Replace all DatabaseObject instances with their id.
        $instances = [];
        $members = [];
        foreach ($memberRefArray as $key => &$value) {
            if ($value === 0) continue;
            else if ($value instanceof self) {
                $instances[ $key ] = $value;
                $value = $value->ID;
            }
            else $members[ $key ] = $value;
        }
        unset($value);

        try {
            // Bind members and fetch result.
            $procedure->BindResult( $memberRefArray );
            $procedure->Fetch( $allowEmptyResult );
        } catch (Exception $ex) {
            // Exception will be thrown after all members have been restored.
        }

        // Restore existing members.
        foreach ($instances as $key => $value)
            $memberRefArray[ $key ] = $value;

        if ($procedure->getStmt()->num_rows == 0)
            foreach ($members as $key => $value)
                $memberRefArray[ $key ] = $value;

        if (isset( $ex ))
            throw $ex;

        return $procedure->getFetched();
    }

    /**
     * Executes the specified stored procedure with the specified paramsArray and initializes
     * the specified dbo DatabaseObject through the memberRefArray parameter.
     * DatabaseObject instances inside the paramsArray will be replaced with their ID property.
     *
     * @param DatabaseObject $dbo The DatabaseObject whose ID property will be used as first stored procedure parameter.
     * @param string $procedureName The Stored Procedure name.
     * @param array $paramsArray List of Stored Procedure parameters which can also contain DatabaseObject instances.
     * @param array $memberRefArray List of referenced object properties
     * @throws DataBaseObjectException
     */
    protected static function QueryObject( $dbo, $procedureName, array $paramsArray, array $memberRefArray )
    {
        try {
            // Find DatabaseObject instances and replace them with their ID property value.        
            foreach ($paramsArray as &$value) {
                if ($value instanceof self)
                    $value = $value->ID;
            }
            unset( $value );

            // Init and execute procedure.
            $procedure = new StoredProcedure( $procedureName, \WebStatic\DATABASE_NAME );
            $procedure->_Prepare($paramsArray);
            //(call_user_func_array( array( $procedure, 'Prepare' ), $paramsArray );
            $procedure->Execute();
            if ($procedure->getStmt()->num_rows > 1)
                throw new DataBaseObjectException( 'Object query has multiple results.' );

            self::Fetch( $procedure, $dbo, $memberRefArray );
        } catch (Exception $ex) {
            throw new DataBaseObjectException( self::getClassName() . ' query failed!', E_USER_ERROR, $ex );
        }

        unset( $procedure );
    }

    /**
     * Executes the specified stored procedure with the specified paramsArray and returns
     * a list of DatabaseObject instances created through the memberRefCallback parameter.
     * DatabaseObject instances inside the paramsArray will be replaced with their ID property.
     *
     * @param string $procedureName The Stored Procedure name.
     * @param array $paramsArray List of Stored Procedure parameters which can also contain DatabaseObject instances.
     * @param \Closure $memberRefCallback delegate(out DatabaseObject dbo, string $objectKey, object[] $paramsArray, object[] $memberRefArray)
     * @return array A List of DatabaseObject instances.
     * @throws DataBaseObjectException
     */
    protected static function QueryList( $procedureName, array $paramsArray, $memberRefCallback )
    {
        $list = array();
        try {
            // Find DatabaseObject instances and replace them with their ID property value.        
            foreach ($paramsArray as &$value) {
                if ($value instanceof self)
                    $value = $value->ID;
            }

            // Init and execute procedure.
            $procedure = new StoredProcedure( $procedureName, \WebStatic\DATABASE_NAME );
            call_user_func_array( array( $procedure, 'Prepare' ), $paramsArray );
            $procedure->Execute();
            do {
                // Create DatabaseObject instances
                /** @var DatabaseObject $dbo */
                $dbo = null;
                $objectKey = null;
                $memberRefArray = array();
                $memberRefCallback( $dbo, $objectKey, $paramsArray, $memberRefArray );

                if (self::Fetch( $procedure, $dbo, $memberRefArray ))
                    $list [ $dbo->$objectKey ] = $dbo;
                else
                    unset( $dbo );
            }while (isset( $dbo ));
        } catch (Exception $ex) {
            throw new DataBaseObjectException( self::getClassName() . ' query failed!', E_USER_ERROR, $ex );
        }
        unset( $procedure );
        return $list;
    }

    protected abstract function _Save( &$procedureName, array &$paramsArray );

    /**
     * Stores the DatabaseObject instance.
     * @throws DataBaseObjectException
     */
    public function Save()
    {
        $procedureName = '';
        $paramsArray = array();
        $this->_Save( $procedureName, $paramsArray );
        // Find DatabaseObject instances and replace them with their ID property value.        
        foreach ($paramsArray as &$value)
            if ($value instanceof self)
                $value = $value->ID;

        $this->Insert( $procedureName, $paramsArray );
    }

    /**
     * Executes the specified stored procedure withe the specified parameters.
     * @param string $procedureName
     * @param array $paramsArray
     * @throws DataBaseObjectException
     */
    protected function Insert( $procedureName, array $paramsArray )
    {
        try {
            $procedure = new StoredProcedure( $procedureName, \WebStatic\DATABASE_NAME );
            call_user_func_array( array( $procedure, 'Prepare' ), $paramsArray );
            $procedure->Execute();
        } catch (Exception $ex) {
            throw new DataBaseObjectException( self::getClassName() . ' insert failed!', E_USER_ERROR, $ex );
        }
        try {
            $this->Load();
        } catch (Exception $ex) {
            throw new DataBaseObjectException( self::getClassName() . ' load failed!', E_USER_ERROR, $ex );
        }
        unset( $procedure );
    }

    public function __toString()
    {
        return $this->getName();
    }
}

final class DataBaseObjectException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}
