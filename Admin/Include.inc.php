<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 01.06.2014
 * File: Include.inc.php
 * Encoding: UTF-8
 * Project: WebStatic
 **/

use AppStatic\Autoload\Using;
use AppStatic\Web\DomainName;
use WebStatic\Core\Domain;

// Determine language parameter for admin session.
$lang = filter_input( INPUT_GET, 'lang', FILTER_SANITIZE_STRING );
if (!empty($lang) && !preg_match( '~\w{2}~', $lang ))
    trigger_error( 'Invalid language id specified. GET Parameter: lang', E_USER_ERROR );

define( 'SITE_LANGUAGE', empty($lang) ? 'en' : $lang );

require_once __DIR__ . '/../config/config.inc.php';

Using::Directory( DOCUMENT_ROOT . '/' );

\WebStatic\Configuration\mysqli_connect_www_siteadmin();

function CreateDomain( DomainName $domainName )
{
    $domain = new Domain( $domainName );
    $domain->Load();
    if (!$domain->getID())
        $domain->Save();

    return $domain;
}