<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 11.04.2017
 * File: Menu.php
 * Encoding: UTF-8
 * Project: WebStatic
 **/

namespace WebStatic\Admin;

use AppStatic\Core\PropertyBase;
use WebStatic\Common\Language;
use WebStatic\Core\Template;
use WebStatic\Core\TemplateItem;

class Menu extends PropertyBase
{
    /**
     * @var null|string
     */
    public $QueryPath = '/';

    /**
     * @var string
     */
    public $Name;

    /**
     * @var null|string
     */
    public $Title;

    /**
     * @var string
     */
    public $Icon;

    /**
     * @var TemplateItem
     */
    public $TemplateItem;

    /**
     * @var Menu
     */
    public $Parent;

    /**
     * @var \WebStatic\Core\MenuItem
     */
    public $MenuItem;

    /**
     * Menu constructor.
     *
     * @param string       $name
     * @param TemplateItem $menuItem
     * @param string       $icon
     * @param bool|string  $queryPath
     *  'true'   - Use default query path separator and generate query path from title.
     *  'false'  - Use default query path separator but don't generate query path from title.
     *  'string' - Override default query separator and generate query path from title.
     *  'null'   - Don't use query path separator nor generate query path from title.
     *
     * @internal param null $title
     */
    public function __construct( $name, TemplateItem $menuItem, $icon = null, $queryPath = true )
    {
        $this->Name = $name;
        $title = preg_split( '~\|~', Language::$Menu->offsetExists( $name ) ? Language::$Menu[ $name ] : $name );
        $this->Title = $title[0];
        $this->TemplateItem = $menuItem;

        // Set icon using default suffix and prefix
        if ($icon)
            $this->Icon = "glyphicon $icon fa-fw";

        // Initialize with default query path separator and return.
        if ($queryPath === false)
            return;

        // Override default query separator if query path is typeof string
        if ($queryPath === null || is_string( $queryPath ))
            $this->QueryPath = $queryPath;

        // Set query path using title
        if ($queryPath)
            $this->QueryPath .= preg_replace( '~[\s/]~', '-', strtolower( $title[ count( $title ) - 1 ] ) );
    }

    /**
     * @param string       $name
     * @param  string      $icon
     * @param TemplateItem $subMenuItem
     * @param Template     $pageTemplate
     * @param null         $pageTitle
     *
     * @param bool         $allowRobots
     *
     * @return Menu
     */
    function Add( $name, $icon, TemplateItem $subMenuItem, Template $pageTemplate = null, $pageTitle = null, $allowRobots = true )
    {
        $menu = new Menu( $name, $subMenuItem, $icon );

        return $this->AddMenu( $menu, $pageTemplate, $pageTitle, $allowRobots );
    }

    /**
     * @param Menu     $subMenu
     * @param Template $pageTemplate
     * @param string   $pageTitle
     *
     * @param bool     $allowRobots
     *
     * @return Menu
     */
    function AddMenu( Menu $subMenu, Template $pageTemplate = null, $pageTitle = null, $allowRobots = true )
    {
        $subMenu->Parent = $this;

        if ($this->QueryPath)
            $subMenu->QueryPath = $this->QueryPath . $subMenu->QueryPath;

        return $subMenu->Create( $pageTemplate ? new Page( $pageTemplate, $pageTitle, $allowRobots ) : null );
    }

    /**
     * @param Page|null $page
     *
     * @return Menu
     */
    public function Create( Page $page = null )
    {
        if (!$this->Parent) {
            global $site;
            $this->MenuItem = $site->getMenu()->SetItem(
                $this->TemplateItem, SITE_LANGUAGE,
                $this->QueryPath, $this->Name, $this->Title, $this->Icon );
        }
        else $this->MenuItem = $this->Parent->MenuItem->SetChild(
            $this->TemplateItem, SITE_LANGUAGE,
            $this->QueryPath, $this->Name, $this->Title, $this->Icon
        );

        if ($page) {
            $page->Menu = $this;
            $page->Generate();
        }

        return $this;
    }
}