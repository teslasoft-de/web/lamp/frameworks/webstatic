<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 11.04.2017
 * File: Page.php
 * Encoding: UTF-8
 * Project: WebStatic
 **/

namespace WebStatic\Admin;

use AppStatic\Core\PropertyBase;
use WebStatic\Core\MenuItem;
use WebStatic\Core\Template;

class Page extends PropertyBase
{
    /**
     * @var Menu
     */
    public $Menu;

    /**
     * @var Template
     */
    public $Template;

    /**
     * @var string
     */
    public $Name;

    /**
     * @var null|string
     */
    public $Title;

    /**
     * @var bool
     */
    public $AllowRobots;

    /**
     * Page constructor.
     *
     * @param Template $template
     * @param string   $title
     * @param bool     $allowRobots
     */
    public function __construct( Template $template, $title = null, $allowRobots = true )
    {
        $this->Template = $template;
        $this->Title = $title;
        $this->AllowRobots = $allowRobots;
    }

    public function Generate()
    {
        // Create the page and invoke content script
        $menuItem = $this->Menu->MenuItem;
        $page = $menuItem->SetPage( $this->Template,
                                    $this->Menu->MenuItem->getLanguage(),
                                    $this->Name ? $this->Name : $this->Menu->Name,
                                    $this->Title ? $this->Title : ($this->Menu->Title . ' - ' . SITE_NAME),
                                    $this->AllowRobots );

        // Generate uri segments beginning from top menu item until page parent menu item.
        $segments = [];
        if ($this->Menu->MenuItem->getParent()) {
            $generateSegments = function ( $generateSegments, MenuItem $menuItem ) use ( &$segments, $page ) {
                if ($menuItem->getParent()) {
                    $generateSegments( $generateSegments, $menuItem->getParent() );

                    return;
                }
                $segments [] = $menuItem->getName();
            };
            $generateSegments( $generateSegments, $this->Menu->MenuItem );
        }
        $segments [] = $page->getName();
        $pageName = \WebStatic\CONTENT_PATH . implode( '/', $segments );

        // Load page content script.
        // Fall back to neutral language if localized page content script is not present.
        foreach ([
                     $pageName . ".{$menuItem->getLanguage()}.php",
                     $pageName . ".php" ] as $contentScriptPath) {
            if (is_file( $contentScriptPath )) {
                /** @noinspection PhpIncludeInspection */
                require_once $contentScriptPath;
                break;
            }
        }
    }
}
