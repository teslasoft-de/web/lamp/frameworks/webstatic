<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: config.inc.php
 * Encoding: UTF-8
 * Project: WebStatic 
 **/

/* --------------------------
 * Contents:
 * --------------------------
 *  - APPLICATION ENVIRONMENT
 *  - ERROR REPORTING
 *
 *  - APP SETTINGS
 *  - MYSQLI CONNECT
 * --------------------------*/

namespace WebStatic\Configuration;

use AppStatic\Data\MySql\MySqliConnection;

define( 'WebStatic\VERSION', '1.1.1' );

// Verify validity of the document root
define( 'DOCUMENT_ROOT', filter_input( INPUT_SERVER, 'DOCUMENT_ROOT' ) );
if (!is_dir( DOCUMENT_ROOT )) {
    header( 'HTTP/1.1 500 Internal Server Error' );
    die('Internal Server Error. Server document root does not exist. Please contact system administrator.');
}

// Load local configuration if present
define( 'WebStatic\CONFIG_PATH', DOCUMENT_ROOT . '/../config/' );

// Load AppStatic Framework (Autoload, Error Handling, Trace)
require_once DOCUMENT_ROOT . '/AppStatic/Include.inc.php';

/*
 * ---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 * ---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     staging
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 */
define( 'ENVIRONMENT_DEVELOPMENT', 'development' );
define( 'ENVIRONMENT_STAGING', 'staging' );
define( 'ENVIRONMENT_PRODUCTION', 'production' );

/*
 * Inject Environment Configuration
 *
 *  The environment is specified by a dynamically loaded app_config.*.php file, where * is wildcard for the target app
 *  environment. The APPLICATION_ENV environment variable should be provided in order to control the loading of the
 *  configuration, but can be omitted. If no environment variable is set, the config uses the config file environment.
 *  So webservers which don't provide environment variables will be still able to enforce an specific environment.
 *
 *  NOTE: All settings you're providing through your configuration file will overwrite all defaults inside this file.
 *        There is a rule in .gitignore present for ignoring all specific environment configuration files :)
 *        Please follow the instructions below.
 *
 *  Setup:
 *      Development:
 *          1. Create a copy of 'config.inc.php' and change the environment in the file name.
 *          2. Apply your settings
 *          3. Request your settings file through the webserver environment variable.
 *          Example:
 *              $ cp config.inc.php config.development.inc.php
 *      After Deployment - Staging/Production:
 *          1. Rename 'config.inc.php' by changing the targeted environment name.
 *          2. Apply your Settings
 *          Example:
 *              $ mv config.inc.php config.production.inc.php
 *
 *  Environment Variable Setup
 *      Apache:
 *          Provide the application environment with the APPLICATION_ENV variable through your vhost configuration
 *          or by .htaccess.
 *          Example:
 *              <Directory /www>
 *                  SetEnv APPLICATION_ENV environment
 *              <Directory>
 *      ngix:
 *          // TODO: Add environment variable setup example for ngix
 */
// Lookup for environment configurations
foreach (glob( realpath( \WebStatic\CONFIG_PATH ) . '/config.*.inc.php' ) as $configPath) {
    // If there is no application environment predefined
    if (!isset($_SERVER['APPLICATION_ENV'])) {
        // Use filename segment as environment indicator.
        if (preg_match( '~\.(\w*)\.inc\.php$~', $configPath, $match )) {
            $_SERVER['APPLICATION_ENV'] = $match[1];
            break;
        }
        // Otherwise lookup for the predefined application configuration.
    }
    elseif (preg_match( '~\.(' . $_SERVER['APPLICATION_ENV'] . '*)\.inc\.php$~', $configPath, $match )) {
        require_once $configPath;
        break;
    }
}

define( 'ENVIRONMENT', isset($_SERVER['APPLICATION_ENV'])
    ? $_SERVER['APPLICATION_ENV']
    /* NOTE: It's common practice and a security guideline that the application should not know anything about
     * it's final environment. The configuration must be done on the target system for security reasons. This
     * also prevents configuration failures and ensures having the correct configuration in production enabled.
     * On servers which does not provide environment variables we are injecting the application environment
     * through a dynamically loaded app_config.*.php file, where * is wildcard for the target app environment. */
    : 'unknown' );

/*
 * ---------------------------------------------------------------
 * ERROR REPORTING
 * ---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */
switch (ENVIRONMENT) {
    case ENVIRONMENT_DEVELOPMENT:
        define( '_APP_ERROR_REPORTING_', E_ALL );
        define( '_APP_DISPLAY_ERRORS_', 1 );

        ini_set( 'display_errors', _APP_DISPLAY_ERRORS_ );
        error_reporting( _APP_ERROR_REPORTING_ );
        break;
    case ENVIRONMENT_STAGING:
    case ENVIRONMENT_PRODUCTION:
        ini_set( 'display_errors', 0 );
        error_reporting(
            version_compare( PHP_VERSION, '5.3', '>=' )
                ? E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED
                : E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE );
        break;
    default:
        #header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        #exit(0); // EXIT_ERROR
        trigger_error( 'Service Unavailable: The application environment is not set correctly.', E_USER_ERROR );
}

/*
 * ---------------------------------------------------------------
 * APP SETTINGS
 * ---------------------------------------------------------------
 *
 */

require_once __DIR__ . '/config.tpl.inc.php';

/*
 * ---------------------------------------------------------------
 * MYSQLI CONNECT
 * ---------------------------------------------------------------
 *
 */

/**
 * @param $path
 */
function mysqli_connnect_( $path )
{
    /** @noinspection PhpIncludeInspection */
    include realpath( $path );
    define( 'WebStatic\DATABASE_NAME', MySqliConnection::GetInstance()->getDBName() );
}

function mysqli_connect_www()
{
    mysqli_connnect_( \WebStatic\CONFIG_PATH . '.mysqli.www.inc.php' );
}

function mysqli_connect_www_admin()
{
    mysqli_connnect_( \WebStatic\CONFIG_PATH . '.mysqli.www.admin.inc.php' );
}

function mysqli_connect_www_siteadmin()
{
    mysqli_connnect_( \WebStatic\CONFIG_PATH . '.mysqli.www-site-admin.inc.php' );
}

/**
 * @param $key
 * @param $value
 */
function define( $key, $value )
{
    if (!defined( $key ))
        \define( $key, $value );
}