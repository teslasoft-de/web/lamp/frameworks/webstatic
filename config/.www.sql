/*
Navicat MySQL Data Transfer

Source Server         : iis02.dev.nw.teslasoft.de
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : smart-repairs

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2017-01-04 17:41:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins` (
  `Admin_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Site_ID` int(10) unsigned NOT NULL,
  `User_ID` int(10) unsigned NOT NULL,
  `Admin_SiteAdmin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Admin_Release` datetime DEFAULT NULL,
  `Admin_Expires` datetime DEFAULT NULL,
  `Admin_Created` datetime NOT NULL,
  `Admin_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Admin_ID`),
  UNIQUE KEY `Unique` (`User_ID`,`Site_ID`) USING BTREE,
  CONSTRAINT `admins_ibfk_users` FOREIGN KEY (`User_ID`) REFERENCES `users` (`User_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `Content_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Page_ID` int(10) unsigned NOT NULL,
  `Parent_ID` int(10) unsigned DEFAULT NULL,
  `TemplateItem_ID` int(10) unsigned NOT NULL,
  `Content_XPath` varchar(4096) DEFAULT NULL,
  `Content_Index` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Content_Name` char(255) NOT NULL,
  `Content_Value` text,
  `Content_Class` varchar(255) DEFAULT NULL,
  `Content_Release` datetime DEFAULT NULL,
  `Content_Expire` datetime DEFAULT NULL,
  `Content_Created` datetime NOT NULL,
  `Content_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Content_ID`),
  UNIQUE KEY `Name` (`Page_ID`,`Content_Name`) USING BTREE,
  KEY `contents_ibkf_templateitems` (`TemplateItem_ID`) USING BTREE,
  KEY `contents_ibkf_contents` (`Parent_ID`),
  KEY `contents_ibfk_page` (`Page_ID`) USING BTREE,
  CONSTRAINT `contents_ibfk_page` FOREIGN KEY (`Page_ID`) REFERENCES `pages` (`Page_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contents_ibkf_contents` FOREIGN KEY (`Parent_ID`) REFERENCES `contents` (`Content_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `contents_ibkf_templateitems` FOREIGN KEY (`TemplateItem_ID`) REFERENCES `templateitems` (`TemplateItem_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for domains
-- ----------------------------
DROP TABLE IF EXISTS `domains`;
CREATE TABLE `domains` (
  `Domain_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Domain_Name` char(255) NOT NULL,
  `Domain_Release` datetime DEFAULT NULL,
  `Domain_Expire` datetime DEFAULT NULL,
  `Domain_Created` datetime NOT NULL,
  `Domain_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Domain_ID`),
  UNIQUE KEY `Domain` (`Domain_Name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `Menu_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Site_ID` int(10) unsigned NOT NULL,
  `Parent_ID` int(10) unsigned DEFAULT NULL,
  `TemplateItem_ID` int(10) unsigned NOT NULL,
  `Menu_Language` char(2) NOT NULL DEFAULT 'en' COMMENT 'ISO3166_ALPHA2',
  `Menu_QueryPath` varchar(2000) DEFAULT NULL,
  `Menu_Name` char(255) NOT NULL DEFAULT 'Home',
  `Menu_Title` char(255) DEFAULT NULL,
  `Menu_Index` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Menu_Icon` varchar(255) DEFAULT NULL,
  `Menu_Release` datetime DEFAULT NULL,
  `Menu_Expire` datetime DEFAULT NULL,
  `Menu_Created` datetime NOT NULL,
  `Menu_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Menu_ID`),
  UNIQUE KEY `Unique` (`Menu_ID`) USING BTREE,
  UNIQUE KEY `Name` (`Site_ID`,`Menu_Language`,`Menu_Name`) USING BTREE,
  KEY `menus_ibfk_templateitems` (`TemplateItem_ID`) USING BTREE,
  KEY `Language` (`Menu_Language`) USING BTREE,
  KEY `menus_ibfk_sites` (`Site_ID`) USING BTREE,
  KEY `menus_ibfk_menus` (`Parent_ID`) USING BTREE,
  CONSTRAINT `menus_ibfk_menus` FOREIGN KEY (`Parent_ID`) REFERENCES `menus` (`Menu_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_ibfk_sites` FOREIGN KEY (`Site_ID`) REFERENCES `sites` (`Site_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menus_ibfk_templateitems` FOREIGN KEY (`TemplateItem_ID`) REFERENCES `templateitems` (`TemplateItem_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `Page_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Menu_ID` int(10) unsigned DEFAULT NULL,
  `Template_ID` int(10) unsigned NOT NULL,
  `Page_Language` char(2) NOT NULL DEFAULT 'en' COMMENT 'ISO3166_ALPHA2',
  `Page_QueryPath` varchar(2000) DEFAULT '/',
  `Page_Name` char(255) NOT NULL,
  `Page_Title` char(255) NOT NULL,
  `Page_Active` tinyint(1) unsigned DEFAULT '1',
  `Page_UnderConstruction` tinyint(1) unsigned DEFAULT '1',
  `Page_Robots` enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive') DEFAULT NULL,
  `Page_Googlebot` enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','nosnippet','noodp') DEFAULT NULL,
  `Page_Slurp` enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp','noydir') DEFAULT NULL,
  `Page_MSNBot` enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp') DEFAULT NULL,
  `Page_Teoma` enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive') DEFAULT NULL,
  `Page_Keywords` text,
  `Page_Description` text,
  `Page_RSS` varchar(256) DEFAULT NULL,
  `Page_Release` datetime DEFAULT NULL,
  `Page_Expire` datetime DEFAULT NULL,
  `Page_Created` datetime NOT NULL,
  `Page_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Page_ID`),
  UNIQUE KEY `Unique` (`Page_ID`) USING BTREE,
  UNIQUE KEY `Name` (`Menu_ID`,`Page_Language`,`Page_Name`) USING BTREE,
  KEY `pages_ibfk_menus` (`Menu_ID`) USING BTREE,
  KEY `Language` (`Page_Language`),
  KEY `pages_ibfk_templates` (`Template_ID`),
  CONSTRAINT `pages_ibfk_menus` FOREIGN KEY (`Menu_ID`) REFERENCES `menus` (`Menu_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_ibfk_templates` FOREIGN KEY (`Template_ID`) REFERENCES `templates` (`Template_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for sites
-- ----------------------------
DROP TABLE IF EXISTS `sites`;
CREATE TABLE `sites` (
  `Site_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Domain_ID` int(10) unsigned NOT NULL,
  `Template_ID` int(10) unsigned NOT NULL,
  `Site_Subdomain` char(250) DEFAULT NULL,
  `Site_Name` char(255) NOT NULL,
  `Site_DocumentRoot` char(255) NOT NULL,
  `Site_TrackingCode` char(255) DEFAULT NULL,
  `Site_Active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Site_UnderConstruction` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Site_Release` datetime DEFAULT NULL,
  `Site_Expire` datetime DEFAULT NULL,
  `Site_Created` datetime NOT NULL,
  `Site_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Site_ID`),
  UNIQUE KEY `Unique` (`Domain_ID`,`Site_ID`) USING BTREE,
  UNIQUE KEY `Name` (`Domain_ID`,`Site_Name`) USING BTREE,
  UNIQUE KEY `Subdomain` (`Domain_ID`,`Site_Subdomain`),
  KEY `sites_ibfk_templates` (`Template_ID`),
  CONSTRAINT `sites_ibfk_domains` FOREIGN KEY (`Domain_ID`) REFERENCES `domains` (`Domain_ID`) ON UPDATE CASCADE,
  CONSTRAINT `sites_ibfk_templates` FOREIGN KEY (`Template_ID`) REFERENCES `templates` (`Template_ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for templateitems
-- ----------------------------
DROP TABLE IF EXISTS `templateitems`;
CREATE TABLE `templateitems` (
  `TemplateItem_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Template_ID` int(10) unsigned NOT NULL,
  `Parent_ID` int(10) unsigned DEFAULT NULL,
  `TemplateItem_Name` char(255) NOT NULL,
  `TemplateItem_XPath` varchar(4096) NOT NULL,
  `TemplateItem_Created` datetime NOT NULL,
  `TemplateItem_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TemplateItem_ID`),
  UNIQUE KEY `Unique` (`Template_ID`,`TemplateItem_ID`) USING BTREE,
  UNIQUE KEY `Name` (`Template_ID`,`TemplateItem_Name`) USING BTREE,
  KEY `templateitems_ibfk_templateitems` (`Parent_ID`),
  CONSTRAINT `templateitems_ibfk_templateitems` FOREIGN KEY (`Parent_ID`) REFERENCES `templateitems` (`TemplateItem_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `templateitems_ibfk_templates` FOREIGN KEY (`Template_ID`) REFERENCES `templates` (`Template_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for templates
-- ----------------------------
DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `Template_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Template_Name` char(255) NOT NULL,
  `Template_Path` char(255) NOT NULL,
  `Template_Created` datetime NOT NULL,
  `Template_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Template_ID`),
  UNIQUE KEY `Unique` (`Template_Name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `User_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Site_ID` int(10) unsigned NOT NULL,
  `User_Name` char(35) NOT NULL,
  `User_PasswordHash` char(255) NOT NULL,
  `User_PasswordSalt` char(255) NOT NULL,
  `User_Language` char(2) NOT NULL COMMENT 'ISO3166_ALPHA2',
  `User_Email` char(253) NOT NULL,
  `User_FirstName` char(35) NOT NULL,
  `User_LastName` char(35) NOT NULL,
  `User_Street` char(35) NOT NULL,
  `User_City` char(35) NOT NULL,
  `User_ZipCode` char(35) NOT NULL,
  `User_Country` char(2) NOT NULL COMMENT 'ISO3166_ALPHA2',
  `User_Release` datetime DEFAULT NULL,
  `User_Expires` datetime DEFAULT NULL,
  `User_Created` datetime NOT NULL,
  `User_Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`User_ID`),
  UNIQUE KEY `Unique` (`Site_ID`,`User_ID`) USING BTREE,
  UNIQUE KEY `Name` (`Site_ID`,`User_Name`) USING BTREE,
  UNIQUE KEY `Email` (`Site_ID`,`User_Email`) USING BTREE,
  CONSTRAINT `users_ibfk_sites` FOREIGN KEY (`Site_ID`) REFERENCES `sites` (`Site_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Procedure structure for Admin_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Admin_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Admin_Insert`(siteID int(10) unsigned, userID int(10) unsigned, `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO admins(
	admins.Site_ID,
	admins.User_ID,
	admins.Admin_Release,
	admins.Admin_Expires,
	admins.Admin_Created)
VALUES(
	siteID,
	userID,
	`release`,
	expire,
	CURRENT_TIMESTAMP);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Admin_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Admin_Query`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Admin_Query`(siteID int(10) unsigned, userID int(10) unsigned)
    READS SQL DATA
BEGIN

SELECT admins.*, users.*  FROM users
INNER JOIN admins USING(Site_ID, User_ID)
WHERE Site_ID = siteID
AND User_ID = userID;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Content_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Content_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Content_Insert`(pageID int(10) unsigned, parentID int(10) unsigned, templateItemID int(10) unsigned, xPath varchar(4096), `name` char(255), `value` text, class char(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

DECLARE contentCount INT(10) UNSIGNED;
DECLARE contentIndex TINYINT(3) UNSIGNED DEFAULT 0;

SELECT COUNT(Content_ID), Content_Index INTO contentCount, contentIndex
FROM contents
WHERE	Page_ID = pageID
	AND (parentID IS NULL AND Parent_ID IS NULL OR Parent_ID = parentID )
GROUP BY Content_Index
ORDER BY Content_Index DESC
LIMIT 1;

IF contentCount > 0 THEN
	SET contentIndex = contentIndex + 1;
END IF;

INSERT INTO contents(
	contents.Page_ID,
	contents.Parent_ID,
	contents.TemplateItem_ID,
	contents.Content_XPath,
	contents.Content_Index,
	contents.Content_Name,
	contents.Content_Value,
	contents.Content_Class,
	contents.Content_Release,
	contents.Content_Expire,
	contents.Content_Created)
VALUES(
	pageID,
	parentID,
	templateItemID,
	xPath,
	contentIndex,
	`name`,
	`value`,
	class,
	`release`,
	expire,
	CURRENT_TIMESTAMP);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Content_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Content_Query`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Content_Query`(pageID int(10) unsigned, parentID int(10) unsigned, `name` char(255))
    READS SQL DATA
    DETERMINISTIC
BEGIN

SELECT * FROM contents
WHERE Page_ID = pageID
AND (parentID IS NULL AND Parent_ID IS NULL OR Parent_ID = parentID )
AND (`name` IS NULL OR Content_Name = `name`)
ORDER BY Content_Index;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Content_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Content_Update`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Content_Update`(id int(10) unsigned, parentID int(10) unsigned, templateItemID int(10) unsigned, xPath varchar(4096), `index` tinyint(3) unsigned, `name` char(255), `value` text, class char(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE contents
SET
	Parent_ID = parentID,
	TemplateItem_ID = templateItemID,
	Content_XPath = xPath,
	Content_Index = `index`,
	Content_Name = `name`,
	Content_Value = `value`,
	Content_Class = class,
	Content_Release = `release`,
	Content_Expire = expire
WHERE
	Content_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Domain_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Domain_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Domain_Insert`(`name` char(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO domains(
	domains.Domain_Name,
	domains.Domain_Release,
	domains.Domain_Expire,
	domains.Domain_Created)
VALUES(
	`name`,
	`release`,
	expire,
	CURRENT_TIMESTAMP);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Domain_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Domain_Query`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Domain_Query`(`name` char(255))
    READS SQL DATA
    DETERMINISTIC
BEGIN

SELECT * FROM domains WHERE Domain_Name = `name`;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Domain_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Domain_Update`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Domain_Update`(id int(10) unsigned, `name` char(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE domains
SET 
	Domain_Name = `name`,
	Domain_Release = `release`,
	Domain_Expire = expire
WHERE
	Domain_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Menu_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Menu_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Menu_Insert`(siteID int(10) unsigned, parentID int(10) unsigned, templateItemID int(10) unsigned, `language` char(2), queryPath char(255), `name` char(255), title char(255), icon varchar(255), `release` datetime, expire datetime)
    SQL SECURITY INVOKER
BEGIN

DECLARE menuCount INT(10) UNSIGNED;
DECLARE menuIndex TINYINT(3) UNSIGNED DEFAULT 0;

SELECT COUNT(Menu_ID)
INTO menuCount
FROM menus
WHERE	Menu_Language = `language`
AND		Menu_Name = `name`
AND		(Menu_QueryPath = queryPath OR (queryPath IS NULL AND Menu_QueryPath IS NULL));

IF menuCount > 0 THEN
	SIGNAL SQLSTATE '23000' SET MESSAGE_TEXT = 'Duplicate entry for colums Menu_Language, Menu_QueryPath', MYSQL_ERRNO = 1063;
END IF;

SELECT COUNT(Menu_ID), Menu_Index INTO menuCount, menuIndex
FROM menus
WHERE	Site_ID = siteID
	AND (parentID IS NULL AND Parent_ID IS NULL OR Parent_ID = parentID )
	AND Menu_Language = `language`
GROUP BY Menu_Index
ORDER BY Menu_Index DESC
LIMIT 1;

IF menuCount > 0 THEN
	SET menuIndex = menuIndex + 1;
END IF;

INSERT INTO menus(
	menus.Site_ID,
	menus.Parent_ID,
	menus.TemplateItem_ID,
	menus.Menu_Language,
	menus.Menu_QueryPath,
	menus.Menu_Name,
	menus.Menu_Title,
	menus.Menu_Index,
	menus.Menu_Icon,
	menus.Menu_Release,
	menus.Menu_Expire,
	menus.Menu_Created)
VALUES(
	siteID,
	parentID,
	templateItemID,
	`language`,
	queryPath,
	`name`,
	title,
	menuIndex,
	icon,
	`release`,
	expire,
	CURRENT_TIMESTAMP);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Menu_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Menu_Query`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Menu_Query`(siteID int(10) unsigned, parentID int(10) unsigned, `language` char(2), `name` char(255))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Querys Site Menus. Language, Name are optional.'
BEGIN

SELECT * FROM menus
WHERE Site_ID = siteID
AND (parentID IS NULL AND Parent_ID IS NULL OR Parent_ID = parentID )
AND (`language` IS NULL OR Menu_Language = `language`)
AND (`name` IS NULL OR Menu_Name = `name`)
ORDER BY Menu_Index;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Menu_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Menu_Update`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Menu_Update`(id int(10) unsigned, parentID int(10) unsigned, templateItemID int(10) unsigned, `language` char(2), queryPath char(255), `name` char(255), title char(255), menuIndex tinyint(3) unsigned, icon varchar(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE menus
SET
	Parent_ID = parentID,
	TemplateItem_ID = templateItemID,
	Menu_Language = `language`,
	Menu_QueryPath = queryPath,
	Menu_Name = `name`,
	Menu_Title = title,
	Menu_Index = menuIndex,
	Menu_Icon = icon,
	Menu_Release = `release`,
	Menu_Expire = expire
WHERE
	Menu_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Page_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Page_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Page_Insert`(menuID int(10) unsigned, templateID int(10) unsigned, `language` char(2), queryPath VARCHAR(2000), `name` char(255), title char(255), active TINYINT(1) UNSIGNED, underConstruction TINYINT(1) UNSIGNED, robots enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive'), googlebot enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','nosnippet','noodp'), slurp enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp','noydir'), msnBot enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp'), teoma enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive'), keywords TEXT, description TEXT, rss VARCHAR(256), `release` datetime, expire datetime)
    SQL SECURITY INVOKER
BEGIN

DECLARE pageCount INT(10) UNSIGNED;

SELECT COUNT(Page_ID)
INTO pageCount
FROM pages
WHERE Menu_ID = menuID
AND Page_Language = `language`
AND Page_Name = `name`
AND	(Page_QueryPath = queryPath OR (queryPath IS NULL AND Page_QueryPath IS NULL));

IF pageCount > 0 THEN
	SIGNAL SQLSTATE '23000' SET MESSAGE_TEXT = 'Duplicate entry for colum Page_QueryPath', MYSQL_ERRNO = 1063;
END IF;

INSERT INTO pages(
	pages.Menu_ID,
	pages.Template_ID,
	pages.Page_Language,
	pages.Page_QueryPath,
	pages.Page_Name,
	pages.Page_Title,
	pages.Page_Active,
	pages.Page_UnderConstruction,
	pages.Page_Robots,
	pages.Page_Googlebot,
	pages.Page_Slurp,
	pages.Page_MSNBot,
	pages.Page_Teoma,
	pages.Page_Keywords,
	pages.Page_Description,
	pages.Page_RSS,
	pages.Page_Release,
	pages.Page_Expire,
	pages.Page_Created)
VALUES(
	menuID,
	templateID,
	`language`,
	queryPath,
	`name`,
	title,
	active,
	underConstruction,
	robots,
	googlebot,
	slurp,
	msnBot,
	teoma,
	keywords,
	description,
	rss,
	`release`,
	expire,
	CURRENT_TIMESTAMP
);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Page_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Page_Query`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Page_Query`(menuID int(10) unsigned, `language` char(2), `name` char(255))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Querys Menu Pages. Language, Name are optional.'
BEGIN

SELECT * FROM pages
WHERE Menu_ID = menuID
AND (`language` IS NULL OR Page_Language = `language`)
AND (`name` IS NULL OR Page_Name = `name`);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Page_QueryLanguages
-- ----------------------------
DROP PROCEDURE IF EXISTS `Page_QueryLanguages`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Page_QueryLanguages`(pageName char(255))
    READS SQL DATA
    DETERMINISTIC
BEGIN

SELECT
	Page_Language,
	Menu_QueryPath
FROM pages
INNER JOIN menus USING(Menu_ID)
WHERE
	Page_Name = pageName;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Page_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Page_Update`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Page_Update`(id INT(10) unsigned, menuID INT(10) unsigned, templateID INT(10) unsigned, `language` CHAR(2), queryPath VARCHAR(2000), `name` CHAR(255), title CHAR(255), active TINYINT(1) UNSIGNED, underConstruction TINYINT(1) UNSIGNED, robots enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive'), googlebot enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','nosnippet','noodp'), slurp enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp','noydir'), msnBot enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive','noodp'), teoma enum('index, follow','noindex','nofollow','noindex, nofollow','noarchive'), keywords TEXT, description TEXT, rss VARCHAR(256), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE pages
SET
	Menu_ID = menuID,
	Template_ID = templateID,
	Page_Language = `language`,
	Page_Name = `name`,
	Page_Title = title,
	Page_Active = active,
	Page_UnderConstruction = underConstruction,
	Page_Robots = robots,
	Page_Googlebot = googlebot,
	Page_Slurp = slurp,
	Page_MSNBot = msnBot,
	Page_Teoma = teoma,
	Page_Keywords = keywords,
	Page_Description = description,
	Page_RSS = rss,
	Page_Release = `release`,
	Page_Expire = expire
WHERE
	Page_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Query_LastModified
-- ----------------------------
DROP PROCEDURE IF EXISTS `Query_LastModified`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Query_LastModified`(domainID int(10) unsigned, subDomain char(250), `language` char(2), queryPath varchar(2000))
    DETERMINISTIC
BEGIN

DECLARE modified, lastModified TIMESTAMP;
DECLARE siteID, menuID, pageID INT(10) UNSIGNED;

# Site
SELECT			Site_ID,
						Site_Modified
INTO				siteID,
						lastModified
FROM				domains
INNER JOIN	sites USING(Domain_ID)
WHERE				Site_Subdomain = subDomain;

# Site Template
SELECT			Template_Modified
INTO				modified
FROM				sites
INNER JOIN	templates USING(Template_ID)
WHERE				Site_ID = siteID;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Site Template Item
SELECT			TemplateItem_Modified
INTO				modified
FROM				sites
INNER JOIN	templates USING(Template_ID)
INNER JOIN	templateitems USING(Template_ID)
WHERE				Site_ID = siteID
ORDER BY		TemplateItem_Modified DESC
LIMIT 1;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Menu
SELECT			Menu_ID,
						Menu_Modified
INTO				menuID,
						modified
FROM				sites
INNER JOIN	menus USING(Site_ID)
WHERE				Site_ID = siteID
AND					Menu_Language = `language`
AND					Menu_QueryPath = queryPath;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Page
SELECT			Page_ID,
						Page_Modified
INTO				pageID,
						modified
FROM				menus
INNER JOIN	pages USING(Menu_ID)
WHERE				Menu_ID = menuID
AND					Page_Language = `language`
ORDER BY		Menu_Modified DESC;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Page Template
SELECT			Template_Modified
INTO				modified
FROM				pages
INNER JOIN	templates USING(Template_ID)
WHERE				Page_ID = pageID;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Page Template Item
SELECT			TemplateItem_Modified
INTO				modified
FROM				pages
INNER JOIN	templates USING(Template_ID)
INNER JOIN	templateitems USING(Template_ID)
WHERE				Page_ID = pageID
ORDER BY		TemplateItem_Modified DESC
LIMIT 1;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

# Page Contents
SELECT			Content_Modified
INTO				modified
FROM				pages
INNER JOIN	contents USING(Page_ID)
WHERE				Page_ID = pageID
ORDER BY		Content_Modified DESC
LIMIT 1;

IF modified > lastModified THEN
	SET lastModified = modified;
END IF;

SELECT lastModified;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ShowGrants
-- ----------------------------
DROP PROCEDURE IF EXISTS `ShowGrants`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ShowGrants`()
    COMMENT 'Show GRANT statements for users'
BEGIN
    DECLARE v TEXT CHARACTER SET utf8;
    DECLARE c CURSOR FOR
    SELECT DISTINCT CONCAT(
        'SHOW GRANTS FOR ', user, '@', host, ';'
    ) AS `query` FROM mysql.user WHERE user NOT IN ('root','phpmyadmin','debian-sys-maint');
    DECLARE EXIT HANDLER FOR NOT FOUND BEGIN END;  
    OPEN c;
    WHILE TRUE DO
        FETCH c INTO v;
        SET @v = v;
        PREPARE stmt FROM @v;
        EXECUTE stmt;
    END WHILE;
    CLOSE c;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ShowGrants_copy
-- ----------------------------
DROP PROCEDURE IF EXISTS `ShowGrants_copy`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ShowGrants_copy`()
    COMMENT 'Show GRANT statements for users'
BEGIN

DECLARE v TEXT CHARACTER SET utf8;

DECLARE done BOOL DEFAULT FALSE;
DECLARE cur CURSOR FOR
SELECT DISTINCT CONCAT(
		'SHOW GRANTS FOR ', user, '@', host, ';'
) FROM mysql.user WHERE user NOT IN ('root','phpmyadmin','debian-sys-maint');
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = TRUE;
OPEN cur;
myLoop: LOOP
	FETCH cur INTO v;
	IF done THEN
		CLOSE cur;
		LEAVE myLoop;
	END IF;
	SET @v = v;
	PREPARE stmt FROM @v;
	EXECUTE stmt;
END LOOP;
CLOSE cur;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Site_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Site_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Site_Insert`(domainID int(10), templateID int(10), subdomain char(250), `name` char(255), documentRoot char(255), trackingCode char(255), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO sites(
	sites.Domain_ID,
	sites.Template_ID,
	sites.Site_Subdomain,
	sites.Site_Name,
	sites.Site_DocumentRoot,
	sites.Site_TrackingCode,	
	sites.Site_Release,
	sites.Site_Expire,
	sites.Site_Created)
VALUES(
	domainID,
	templateID,
	subdomain,
	`name`,
	documentRoot,
	trackingCode,
	`release`,
	expire,
	CURRENT_TIMESTAMP);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Site_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Site_Query`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Site_Query`(domainID int(10) unsigned, subdomain char(250), `name` char(255))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Querys Domain Sites. Name is optional.'
BEGIN

SELECT * FROM sites
WHERE Domain_ID = domainID
AND Site_Subdomain = subdomain
AND (`name` IS NULL OR Site_Name = `name`);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Site_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Site_Update`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Site_Update`(id int(10), templateID int(10), subdomain char(250), `name` char(255), documentRoot char(255), trackingCode char(255), active tinyint(1) unsigned, underConstruction tinyint(1) unsigned, `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE sites
SET
	Template_ID = templateID,
	Site_Subdomain = subdomain,
	Site_Name = `name`,
	Site_DocumentRoot = documentRoot,
	Site_TrackingCode = trackingCode,
	Site_Active = active,
	Site_UnderConstruction = underConstruction,
	Site_Release = `release`,
	Site_Expire = expire
WHERE sites.Site_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for TemplateItem_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `TemplateItem_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `TemplateItem_Insert`(templateID int(10) unsigned, parentID int(10) unsigned, `name` char(255), xPath varchar(4096))
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO templateitems(
	templateitems.Template_ID,
	templateitems.Parent_ID,
	templateitems.TemplateItem_Name,
	templateitems.TemplateItem_XPath,
	templateitems.TemplateItem_Created
)
VALUES(
	templateID,
	parentID,
	`name`,
	xPath,
	CURRENT_TIMESTAMP
);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for TemplateItem_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `TemplateItem_Query`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `TemplateItem_Query`(templateID int(10) unsigned, parentID int(10) unsigned, `name` char(255))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'templateID int(10) unsigned, parentID, `name`'
BEGIN

SELECT * FROM templateitems
WHERE (templateID IS NULL OR Template_ID = templateID)
# Query only nonparents.
AND (parentID IS NULL AND Parent_ID IS NULL OR Parent_ID = parentID )
AND (`name` IS NULL OR TemplateItem_Name = `name`);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for TemplateItem_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `TemplateItem_Update`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `TemplateItem_Update`(id int(10) unsigned, parentID int(10) unsigned, `name` char(255), xPath varchar(4096))
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE templateitems
SET
	Parent_ID = parentID,
	TemplateItem_Name = `name`,
	TemplateItem_XPath = xPath
WHERE	TemplateItem_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Template_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `Template_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Template_Insert`(`name` char(255), path char(255))
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO templates(
	templates.Template_Name,
	templates.Template_Path,
	templates.Template_Created)
VALUES(
	`name`,
	path,
	CURRENT_TIMESTAMP
);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Template_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `Template_Query`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Template_Query`(id int(10) unsigned, `name` char(255))
    READS SQL DATA
    DETERMINISTIC
    COMMENT 'Querys Templates. Name is optional.'
BEGIN

SELECT * FROM templates
WHERE (id IS NULL OR Template_ID = id)
AND (`name` IS NULL OR Template_Name = `name`);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Template_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `Template_Update`;
DELIMITER ;;
CREATE DEFINER=`www-admin`@`localhost` PROCEDURE `Template_Update`(id int(10) unsigned, `name` char(255), path char(255), modified timestamp)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

UPDATE templates
SET
	Template_Name = `name`,
	Template_Path = path,
	Template_Modified = modified
WHERE
	Template_ID = id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for Truncate
-- ----------------------------
DROP PROCEDURE IF EXISTS `Truncate`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `Truncate`()
    SQL SECURITY INVOKER
BEGIN

DELETE FROM `pages`;
DELETE FROM `menus`;
DELETE FROM `sites`;
DELETE FROM `templates`;
DELETE FROM `domains`;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for User_Insert
-- ----------------------------
DROP PROCEDURE IF EXISTS `User_Insert`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `User_Insert`(siteID int(10) unsigned, `name` char(255), passwordHash char(255), passwordSalt char(255), `language` char(2), email char(253), firstName char(255), lastName char(255), street char(255), city char(255), zipCode char(255), country char(2), `release` datetime, expire datetime)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO users(
	users.Site_ID,
	users.User_Name,
	users.User_PasswordHash,
	users.User_PasswordSalt,
	users.User_Language,
	users.User_Email,
	users.User_FirstName,
	users.User_LastName,
	users.User_Street,
	users.User_City,
	users.User_ZipCode,
	users.User_Country,
	users.User_Release,
	users.User_Expire,
	users.User_Created
)
VALUES(
	siteID,
	`name`,
	passwordHash,
	passwordSalt,
	`language`,
	email,
	firstName,
	lastName,
	street,
	city,
	zipCode,
	country,
	`release`,
	expire,
	CURRENT_TIMESTAMP
);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for User_Query
-- ----------------------------
DROP PROCEDURE IF EXISTS `User_Query`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `User_Query`(siteID int(10) unsigned, `name` char(255))
    READS SQL DATA
BEGIN

SELECT	users.* FROM users
LEFT OUTER JOIN admins USING(Site_ID, User_ID)
WHERE	Site_ID = siteID
AND	User_Name = `name`
AND	Admin_ID = NULL;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for User_Update
-- ----------------------------
DROP PROCEDURE IF EXISTS `User_Update`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `User_Update`(id int(10) unsigned, `name` char(255), passwordHash char(255), passwordSalt char(255), `language` char(2), email char(253), firstName char(255), lastName char(255), street char(255), city char(255), zipCode char(255), country char(2), `release` datetime, expire datetime)
    MODIFIES SQL DATA
BEGIN

UPDATE users
SET
	User_Name = `name`,
	User_PasswordHash = passwordHash,
	User_PasswordSalt = passwordSalt,
	User_Language = `language`,
	User_Email = email,
	User_FirstName = firstName,
	User_LastName = lastName,
	User_Street = street,
	User_City = city,
	User_ZipCode = zipCode,
	User_Country = country,
	User_Release = `release`,
	User_Expire = expire
WHERE
	User_ID = id;

END
;;
DELIMITER ;
