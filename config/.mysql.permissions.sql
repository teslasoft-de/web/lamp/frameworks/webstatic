-- Grants for www@localhost --
GRANT USAGE ON *.* TO 'www'@'localhost' IDENTIFIED BY PASSWORD '';
GRANT CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE ON `www`.* TO 'www'@'localhost';
-- AppStatic Access --
GRANT CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE ON `appstatic`.* TO 'www'@'localhost';

-- Grants for www-admin@localhost --
GRANT USAGE ON *.* TO 'www-admin'@'localhost' IDENTIFIED BY PASSWORD '';
GRANT SELECT, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, SHOW VIEW ON `www`.* TO 'www-admin'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW VIEW ON `www`.`menus` TO 'www-admin'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW VIEW ON `www`.`contents` TO 'www-admin'@'localhost';
GRANT SELECT, SHOW VIEW ON `www`.`users` TO 'www-admin'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW VIEW ON `www`.`templates` TO 'www-admin'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW VIEW ON `www`.`pages` TO 'www-admin'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW VIEW ON `www`.`templateitems` TO 'www-admin'@'localhost';
-- AppStatic Access --
GRANT SELECT, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, SHOW VIEW ON `appstatic`.* TO 'www-admin'@'localhost';

-- Grants for www-site-admin@localhost --
GRANT USAGE ON *.* TO 'www-site-admin'@'localhost' IDENTIFIED BY PASSWORD '';
GRANT ALL PRIVILEGES ON `www`.* TO 'www-site-admin'@'localhost' WITH GRANT OPTION;
-- AppStatic Access --
GRANT ALL PRIVILEGES ON `appstatic`.* TO 'www-site-admin'@'localhost' WITH GRANT OPTION;