<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 14.01.2014
 * File: mysqli.www.inc.php
 * Encoding: UTF-8
 * Project: WebStatic
 *
 * Specify the password and replace WEBSTATIC_DATABASE_NAME.
 **/
\AppStatic\Data\MySql\MySqliConnection::SetInstance( 'localhost', 'www', '', \WebStatic\DATABASE_NAME );
