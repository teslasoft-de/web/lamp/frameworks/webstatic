<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 12.01.2014
 * File: Content.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Core\ExceptionBase;
use AppStatic\Data\XmlUtility;
use DOMNode;
use WebStatic\Common\DatabaseObject;
use WebStatic\Common\Expirable;

/**
 * Represents the page content and provides values for the linked template items.
 * 
 * @package WebStatic
 * @name Content
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Content extends Expirable
{
    private $IsTwin;
    private $ParentDOMNode;
    protected $Page;
    protected $Parent;

    protected $TemplateItem;
    protected $XPath;
    protected $Index;
    protected $Name;
    protected $Value;
    protected $Class;

    /**
     * The underlying template DOMElement
     * @var DOMNode
     */
    protected $DOMNode;

    /**
     * Determines if the content element has been generated by script include.
     * @var bool
     */
    public $NodeHandled;

    /**
     * Determines if the content value has been set by script include.
     * @var bool
     */
    public $ScriptHandled;

    /**
     * Returns the page which contains this Content instance.
     * @return Page
     */
    public function getPage()
    {
        return $this->Page;
    }

    /**
     * Return the Content parent.
     * @return Content
     */
    public function getParent()
    {
        return $this->Parent;
    }

    /**
     * Returns the
     * @return TemplateItem
     */
    public function getTemplateItem()
    {
        if (is_int( $this->TemplateItem )){
            $templateItem = $this->Page->getTemplate()->FindItem( $this->TemplateItem );
            if(!$templateItem)
                $templateItem = $this->Page->getMenu()->getSite()->getTemplate()->FindItem( $this->TemplateItem );
            $this->TemplateItem = $templateItem;
            $this->TemplateItem->Load();
        }
        return $this->TemplateItem;
    }

    public function getXPath()
    {
        return $this->XPath;
    }

    public function getIndex()
    {
        return $this->Index;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getValue()
    {
        return $this->Value;
    }

    public function getClass()
    {
        return $this->Class;
    }

    /**
     * 
     * @return DOMNode
     */
    public function getDOMNode()
    {
        return $this->DOMNode;
    }

    public function setTemplateItem( $TemplateItem )
    {
        $this->TemplateItem = $TemplateItem;
    }

    public function setName( $Name )
    {
        $this->Name = $Name;
    }

    public function setXPath( $XPath )
    {
        $this->XPath = $XPath;
    }

    public function setClass( $Class )
    {
        $this->Class = $Class;
    }

    public function setValue( $Value )
    {
        $this->Value = $Value;
    }

    public function setIndex( $Index )
    {
        $this->Index = $Index;
    }

    function __construct( Page $page, TemplateItem $templateItem = null, $name = null, Content $parent = null )
    {
        $this->Page = $page;
        $this->TemplateItem = $templateItem;
        $this->Name = $name;
        $this->Parent = $parent;
    }
    
    public function Load()
    {
        parent::Load();
        $this->loadChildren();
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        if (empty( $procedureName ))
            $procedureName = 'Content_Query';
        if (!count( $paramsArray ))
            $paramsArray += array(
                $this->Page,
                $this->Parent,
                $this->Name );
        if (!count( $memberRefArray ))
            $memberRefArray += array(
                $page = null,
                $parent = null,
                &$this->TemplateItem,
                &$this->XPath,
                &$this->Index,
                &$this->Name,
                &$this->Value,
                &$this->Class );

        parent::_Load( $procedureName, $paramsArray, $memberRefArray );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'Content_Insert';
            $paramsArray [] = $this->Page;
        } else {
            $procedureName = 'Content_Update';
            $paramsArray [] = $this;
        }

        $paramsArray [] = $this->Parent;
        $paramsArray [] = $this->TemplateItem;
        $paramsArray [] = $this->XPath;
        if ($procedureName == 'Content_Update')
            $paramsArray [] = $this->Index;
        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->Value;
        $paramsArray [] = $this->Class;

        parent::_Save( $procedureName, $paramsArray );
    }

    /**
     * @param Page $page
     * @return Content[]
     * @throws \WebStatic\Common\DataBaseObjectException
     */
    public static function FromPage( Page $page )
    {
        return self::QueryList(
                'Content_Query',
                array( $page, $parent = null, $name = null ),
                function( Content &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray)
                use ( $page ) {
            $obj = new Content( $page );
            $objectKeyProperty = 'Name';
            $procedureName = null;
            $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }

    /**
     * @param Page $page
     * @param Content $parentItem
     * @return Content[]
     * @throws \WebStatic\Common\DataBaseObjectException
     */
    public static function FromParent( Page $page, Content $parentItem )
    {
        return self::QueryList(
                'Content_Query',
                array( $page, $parentItem, $name = null ),
                function( Content &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray )
                use ( $page, $parentItem ) {
            $obj = new Content( $page, null, null, $parentItem );
            $objectKeyProperty = 'Name';
            $procedureName = null;
            $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }

    public function AddChild( $templateItem, $name, $xPath = null, $value = null, $class = null )
    {
        if ($this->ID == null)
            throw new ContentException( $this, "Content '$' is not initialized." );
        
        $child = new Content( $this->Page, $this->TemplateItem[ $templateItem ], $name, $this );
        $child->XPath = $xPath;
        $child->Value = $value;
        $child->Class = $class;
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }
    
    public function SetChild( $templateItem, $name, $xPath = null, $value = null, $class = null )
    {
        if ($this->ID == null)
            throw new ContentException( $this, "Content '$' is not initialized." );
        
        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if(!$child){
            $child = new Content( $this->Page, null, $name, $this );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID){
                if(parent::offsetExists( $child->getName() ))
                    unset( $this[ $child->getName() ] );
            }
            $child->setName( $name );
        }
        $child->setTemplateItem( $this->TemplateItem[ $templateItem ] );
        $child->XPath = $xPath;
        $child->Value = $value;
        $child->Class = $class;
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }

    /**
     * 
     * @return Content
     */
    private function loadChildren()
    {
        if(count($this))
            parent::exchangeArray (array());

        /* @var $value DatabaseObject */
        foreach (self::FromParent( $this->Page, $this ) as $key => $value){
            $value->Load();
            parent::offsetSet($key, $value);
        }
    }
    
    /**
     * Generates the contents into the specified $parentNode.
     * 
     * @param DOMNode $parentNode
     * @param \WebStatic\Common\DatabaseObject $contents
     */
    public static function GenerateNode( DOMNode $parentNode, DatabaseObject $contents )
    {
        // First loop through all contents
        // and collect their template present items for removal.
        $removeNodes = array();
        $removeAll = false;
        
        /* @var $content Content */
        foreach ($contents as $content) {
            
            // Query and collect all present items for removal.
            $item = $content->getTemplateItem();
            $nodes = $item->getTemplate()->getDOMXPath()->query( $item->getXPath(), $parentNode );
            
            // Determine the occurrence of the content template item.
            $isUnique = true;
            foreach( $removeNodes as $rNodes ){
                // If the content template item occurs more than once, remove all contents.
                if($rNodes[0]->TemplateItem->getID() == $content->TemplateItem->getID())
                    $isUnique = false;
                // Skip removal on double defined template items for setting multiple values in a single template item.
                else if($rNodes[0]->TemplateItem->getXPath() == $content->TemplateItem->getXPath()){
                    // Get existing DOMNode from content twin.
                    if(!$content->DOMNode){
                        $content->DOMNode = $rNodes[0]->DOMNode;
                        $content->IsTwin = true;
                    }
                    continue(2);
                }
            }
            
            // If the content template item occurs only once,
            // it will be directly used and not re-generated.
            if($nodes->length == 1 && $isUnique)
                $content->DOMNode = $nodes->item( 0 );
            else
                $removeAll = true;
            
            $removeNodes []= array( $content, $nodes );
        }
        
        // Collect all template items without content for removal.
        if($contents instanceof $content){
            foreach($contents->getTemplateItem() as $templateItem){
                foreach($removeNodes as $nodes)
                    if($nodes[0]->TemplateItem->getXPath() == $templateItem->getXPath())
                        continue(2);
                $nodes = $templateItem->getTemplate()->getDOMXPath()->query( $templateItem->getXPath(), $parentNode );
                if($nodes->length > 0)
                    $removeNodes []= array( false, $nodes );
            }
        }
        
        // Remove all present items except lonely ones.
        foreach ($removeNodes as $nodes) {
            
            // If template items occurring many times also single existent items
            // must be regenerated to preserve the content order.
            if($nodes[1]->length == 1 && $nodes[0] && !$removeAll)
                continue;
            foreach ($nodes[1] as $removeNode) {
                // Skip if already removed
                if($removeNode->parentNode){
                    if($nodes[0] && !$nodes[0]->ParentDOMNode)
                        $nodes[0]->ParentDOMNode = $removeNode->parentNode;
                    $removeNode->parentNode->removeChild( $removeNode );
                }
            }
        }
        
        // Generate the contents after the $node has been cleaned up.
        foreach ($contents as $content) {
            $content->Generate();
            if($content->NodeHandled){
                if($content->DOMNode->parentNode && !$content->IsTwin)
                    $content->DOMNode->parentNode->removeChild( $content->DOMNode );
                continue;
            }
                
            $domNode = $content->DOMNode;
            // Append content node if it is a fresh template item copy
            // or generated through an action script.
            if(!$domNode->parentNode){
                $parent = $content->ParentDOMNode ? $content->ParentDOMNode : $parentNode;
                $parent->appendChild( $domNode );
            }
            
            if(!count($content))
                continue;
            
            self::GenerateNode( $domNode, $content );
        }
    }
    
    public function Generate()
    {
        // Get a copy of the template item if not already set.
        if(!$this->DOMNode)
            $this->DOMNode = $this->getTemplateItem()->getDOMNode()->cloneNode( true );
        
        // Get menu structure
        $getMenuPath = function( $f, MenuItem $menuItem, array &$menuPath ){
            if($menuItem->getParent())
                $f( $f, $menuItem->getParent(), $menuPath );
            $menuPath[]= $menuItem->getName();
        };
        $menuPath = array();
        $getMenuPath( $getMenuPath, $this->getPage()->getMenu(), $menuPath );
        
        $paths = array();
        // Load content scripts beginning from top of the script directory tree.
        for ($depth = -1; $depth < count( $menuPath ); $depth++) {
            if($depth > -1)
                $paths = array_slice( $menuPath, 0, $depth +1 );
            
            $paths []= $this->getTemplateItem()->getName() . '.php';
            $contentScriptPath = \WebStatic\SCRIPT_PATH . implode( '/', $paths );
            if(is_file( $contentScriptPath ))
                /** @noinspection PhpIncludeInspection */
                require $contentScriptPath;
        }
        
        if($this->XPath){
            $node = $this->Page->getTemplate()->getDOMXPath()->query( $this->XPath, $this->DOMNode )->item( 0 );
            if(!$node)
                throw new ContentException( $this, <<<EOT
Content XPath '$this->XPath' is invalid.
Content: $
Template: {$this->Page->getTemplate()->getName()}
File: {$this->Page->getTemplate()->getPath()}
EOT
                );                     
        }
        else
            $node = $this->DOMNode;
            
        // Set class
        if($this->Class)
            XmlUtility::SetClassAttribute( $node, $this->Class, true );
        
        if($this->ScriptHandled)
            return;
        
        // Set value
        if($this->Value){
            // Insert value and ensure containing html elements won't be escaped.
            XmlUtility::SetHtmlContent( $node, $this->Value );
        }
    }
}

final class ContentException extends ExceptionBase
{

    /**
     * @var Content
     */
    private $content;

    /**
     * @return Content
     */
    public function getContent()
    {
        return $this->content;
    }

    function __construct( Content $content, $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( preg_replace( '~\$~', $content->getName(), $_message ), $_code, $_innerException );
        $this->content = $content;
    }

}