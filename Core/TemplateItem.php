<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 10.01.2014
 * File: TemplateItem.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Core\ExceptionBase;
use AppStatic\IO\Path;
use DOMNode;
use DOMNodeList;
use Exception;
use WebStatic\Common\Modifiable;

/**
 * Description of TemplateItem
 * 
 * @package WebStatic
 * @name TemplateItem
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class TemplateItem extends Modifiable
{
    protected $Template;
    protected $Parent;
    protected $Children;
    protected $Name;
    protected $XPath;
    protected $DOMNode;

    /**
     * 
     * @return Template
     */
    public function getTemplate()
    {
        return $this->Template;
    }

    /**
     * Gets the parent template item.
     * 
     * @return TemplateItem
     */
    public function getParent()
    {
        return $this->Parent;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getXPath()
    {
        return $this->XPath;
    }

    public function getFullXPath()
    {
        if (!$this->Parent)
            return $this->XPath;

        return "{$this->Parent->getFullXPath()}/$this->XPath";
    }

    public function setName( $name )
    {
        $this->Name = $name;
    }

    public function setTargetXPath( $xPath )
    {
        $this->XPath = $xPath;
    }

    /**
     * 
     * @return DOMNode
     */
    public function getDOMNode()
    {
        return $this->DOMNode;
    }
    
    public function setDOMNode( $xmlNode )
    {
        $this->DOMNode = $xmlNode;
    }

    
    function __construct( Template $template, $name = null, $targetXPath = null, TemplateItem $parent = null )
    {
        $this->Template = $template;
        $this->Name = $name;
        $this->XPath = $targetXPath;
        $this->Parent = $parent;
    }

    public function Load( $skipIfNotFound = true )
    {
        if (!$this->ID){
            parent::Load();
            if($skipIfNotFound && !$this->ID)
                return;
        }

        if ($this->DOMNode)
            return;

        /* @var $evalResult DOMNodeList */
        $evalResult = $this->Template->getDOMXPath()->evaluate(
                $this->XPath, $this->Parent ? $this->Parent->DOMNode : null  );
        if (!$evalResult || $evalResult->length == 0) {
            $templatePath = Path::Combine( DOCUMENT_ROOT, $this->Template->getPath() );
            $xpath = $this->XPath;
            $parent = $this->Parent ? "\nParent: {$this->Parent->getName()} - {$this->Parent->getFullXPath()}" : null;
            throw new TemplateItemException( <<<EOT
XPath for template item '$this->Name' does not target any elements.
Please verify that the XPath targets an existing element.
XPath: $xpath$parent - $templatePath

Fix: Correct the XPath
Workaround: Remove the TemplateItem from {$this->Template->getName()} configuration. - $templatePath
EOT
            );
        }
        $this->DOMNode = $this->QueryNodes()->item( 0 );
        if($this->ID)
            $this->loadChildren();
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        if (empty( $procedureName ))
            $procedureName = 'TemplateItem_Query';
        if (!count( $paramsArray ))
            $paramsArray += array(
                $this->Template,
                $this->Parent,
                $this->Name );
        if (!count( $memberRefArray ))
            $memberRefArray += array(
                $template = null,
                $parent = null,
                &$this->Name,
                &$this->XPath );

        parent::_Load( $procedureName, $paramsArray, $memberRefArray );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'TemplateItem_Insert';
            $paramsArray [] = $this->Template;
        } else {
            $procedureName = 'TemplateItem_Update';
            $paramsArray [] = $this;
        }

        $paramsArray [] = $this->Parent;
        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->XPath;
    }

    /**
     * 
     * @param Template $template
     * @return array
     */
    public static function FromTemplate( Template $template )
    {
        return self::QueryList(
                'TemplateItem_Query',
                array( $template, $parent = null, $name = null ),
                function( TemplateItem &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray )
                use ( $template ) {
            $obj = new TemplateItem( $template );
            $objectKeyProperty = 'Name';
            $procedureName = null;
            $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }

    /**
     * 
     * @param Template $template
     * @param TemplateItem $parentItem
     * @return array(TemplateItem)
     */
    public static function FromParent( Template $template, TemplateItem $parentItem )
    {
        return self::QueryList(
                'TemplateItem_Query',
                array( $template, $parentItem, $name = null ),
                function( TemplateItem &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray )
                use ( $template, $parentItem ) {
            $obj = new TemplateItem( $template, null, null, $parentItem );
            $objectKeyProperty = 'Name';
            $procedureName = null;
            $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }
    
    protected function OnPropertyOffsetGet( $offset )
    {
        $item = $this->Template->FindItem( $offset, $this );
        return $item ? $item : parent::OnPropertyOffsetGet( $offset );
    }
    
    protected function OnPropertyOffsetSet( $offset, $value )
    {
        parent::OnPropertyOffsetSet( $offset, $value );
        
        if(!Template::$TemplateMode)
            return parent::OnPropertyOffsetSet( $offset, $value );
        
        if(!is_string( $value ))
            throw new TemplateException( 'Offset property value is invalid. String expected. \''. gettype( $value ) .'\' given.' );
        
        $this->SetChild( $offset, $value );
        return true;
    }

    public function AddChild( $name, $targetXPath )
    {
        if (!$this->ID)
            throw new TemplateItemException( "Can't add child until TemplateItem has not been loaded." );
        
        $child = new TemplateItem( $this->Template, $name, $targetXPath, $this );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }

    public function SetChild( $name, $targetXPath )
    {
        if (!$this->ID)
            throw new TemplateItemException( "Can't add child until TemplateItem has not been loaded." );
        
        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if(!$child){
            $child = new TemplateItem( $this->Template, $name, null, $this );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID){
                if(parent::offsetExists( $child->getName() ))
                    unset( $this[ $child->getName() ] );
            }
            $child->setName( $name );
        }
        $child->setTargetXPath( $targetXPath );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }

    /**
     * 
     * @return TemplateItem
     */
    private function loadChildren()
    {
        if(count($this))
            parent::exchangeArray (array());


        /** @var TemplateItem $value */
        foreach (self::FromParent( $this->Template, $this ) as $key => $value){
            $value->Load();
            parent::offsetSet( $key, $value );
        }
    }

    public function QueryNodes()
    {
        $parentXMLNode = $this->Parent ? $this->Parent->DOMNode : null;
        return $this->Template->getDOMXPath()->query( $this->XPath, $parentXMLNode );
    }
    
    /**
     * 
     * @param DOMNode $parentNode
     * @param string $item
     * @return DOMNode
     *
     * TODO: Flip parameters and make parent node parameter optional
     */
    public function QueryChildNode( DOMNode $parentNode, $item )
    {
        /** @var string $item */
        return $this->getTemplate()->getDOMXPath()->query( $this[ $item ]->getXPath(), $parentNode )->item( 0 );
    }
}

class TemplateItemException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}

final class InvalidTemplateItemNodeTypeException extends TemplateItemException
{
    /**
     * @var TemplateItem
     */
    private $templateItem;
    /**
     * @var Exception
     */
    private $nodeType;
    /**
     * @var
     */
    private $expectedNodeType;

    function __construct( TemplateItem $templateItem, $nodeType, $expectedNodeType, $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( <<<EOT
Invalid template item node type!
$_message
Template Item: {$templateItem->getName()}
Current Node Type: $nodeType
Expected Node Type: $expectedNodeType
EOT
            , $_code, $_innerException );
        $this->templateItem = $templateItem;
        $this->nodeType = $nodeType;
        $this->expectedNodeType = $expectedNodeType;
    }
}