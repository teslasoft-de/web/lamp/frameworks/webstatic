<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 08.01.2014
 * File: Page.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Core\String;
use AppStatic\Data\Xml\XPath;
use AppStatic\Core\ExceptionBase;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\Web\HtmlUtility;
use DOMNode;
use WebStatic\Common\Expirable;

/**
 * Description of Page
 *
 * @package   WebStatic
 * @name Page
 * @version   0.8.9
 * @author    H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Page extends Expirable
{
    protected $Menu;
    protected $Template;
    protected $Language;
    protected $QueryPath;
    protected $Name;
    protected $Title;
    protected $Active;
    protected $UnderConstruction;
    protected $Robots;
    protected $Googlebot;
    protected $Slurp;
    protected $MSNBot;
    protected $Teoma;
    protected $Keywords;
    protected $Description;
    protected $RSS;

    /**
     *
     * @return MenuItem
     */
    public function getMenu()
    {
        return $this->Menu;
    }

    /**
     *
     * @return Template
     */
    public function getTemplate()
    {
        if (is_int( $this->Template )) {
            $this->Template = Template::QueryByID( $this->Template );
            $this->Template->Load();
        }

        return $this->Template;
    }

    public function getLanguage()
    {
        return $this->Language;
    }

    public function getQueryPath()
    {
        return $this->QueryPath;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getTitle()
    {
        return $this->Title;
    }

    public function getActive()
    {
        return $this->Active;
    }

    public function getUnderConstruction()
    {
        return $this->UnderConstruction;
    }

    public function getRobots()
    {
        return $this->Robots;
    }

    public function getGooglebot()
    {
        return $this->Googlebot;
    }

    public function getSlurp()
    {
        return $this->Slurp;
    }

    public function getMSNBot()
    {
        return $this->MSNBot;
    }

    public function getTeoma()
    {
        return $this->Teoma;
    }

    public function getKeywords()
    {
        return $this->Keywords;
    }

    public function getDescription()
    {
        return $this->Description;
    }

    public function getRSS()
    {
        return $this->RSS;
    }

    public function setTemplate( $Template )
    {
        $this->Template = $Template;
    }

    public function setLanguage( $Language )
    {
        $this->Language = $Language;
    }

    public function setQueryPath( $QueryPath )
    {
        $this->QueryPath = $QueryPath;
    }

    public function setName( $Name )
    {
        $this->Name = $Name;
    }

    public function setTitle( $Title )
    {
        $this->Title = $Title;
    }

    public function setActive( $Active )
    {
        $this->Active = $Active;
    }

    public function setUnderConstruction( $UnderConstruction )
    {
        $this->UnderConstruction = $UnderConstruction;
    }

    public function setRobots( $Robots )
    {
        $this->Robots = $Robots;
    }

    public function setGooglebot( $Googlebot )
    {
        $this->Googlebot = $Googlebot;
    }

    public function setSlurp( $Slurp )
    {
        $this->Slurp = $Slurp;
    }

    public function setMSNBot( $MSNBot )
    {
        $this->MSNBot = $MSNBot;
    }

    public function setTeoma( $Teoma )
    {
        $this->Teoma = $Teoma;
    }

    public function setKeywords( $Keywords )
    {
        $this->Keywords = $Keywords;
    }

    public function setDescription( $Description )
    {
        $this->Description = $Description;
    }

    public function setRSS( $RSS )
    {
        $this->RSS = $RSS;
    }

    public function setContents( $Contents )
    {
        $this->Contents = $Contents;
    }

    function __construct( MenuItem $menu, Template $template = null, $language = null, $queryPath = null, $name = null, $title = null, $active = null, $underConstruction = null )
    {
        $this->Menu = $menu;
        $this->Template = $template;
        $this->Language = $language;
        $this->QueryPath = $queryPath;
        $this->Name = $name;
        $this->Title = $title;
        $this->Active = $active;
        $this->UnderConstruction = $underConstruction;
    }

    public function Load( $initialize = true )
    {
        parent::Load();
        if ($initialize)
            $this->loadContents();
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        parent::_Load(
            $procedureName = 'Page_Query',
            $paramsArray += [
                $this->Menu,
                $this->Language,
                $this->Name],
            $memberRefArray += [
                $menu = null,
                &$this->Template,
                &$this->Language,
                &$this->QueryPath,
                &$this->Name,
                &$this->Title,
                &$this->Active,
                &$this->UnderConstruction,
                &$this->Robots,
                &$this->Googlebot,
                &$this->Slurp,
                &$this->MSNBot,
                &$this->Teoma,
                &$this->Keywords,
                &$this->Description,
                &$this->RSS] );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'Page_Insert';
        } else {
            $procedureName = 'Page_Update';
            $paramsArray [] = $this->getID();
        }
        $paramsArray [] = $this->Menu;
        $paramsArray [] = $this->Template;
        $paramsArray [] = $this->Language;
        $paramsArray [] = $this->QueryPath;
        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->Title;
        $paramsArray [] = $this->Active;
        $paramsArray [] = $this->UnderConstruction;
        $paramsArray [] = $this->Robots;
        $paramsArray [] = $this->Googlebot;
        $paramsArray [] = $this->Slurp;
        $paramsArray [] = $this->MSNBot;
        $paramsArray [] = $this->Teoma;
        $paramsArray [] = $this->Keywords;
        $paramsArray [] = $this->Description;
        $paramsArray [] = $this->RSS;

        parent::_Save( $procedureName, $paramsArray );
    }

    public function AddContent( TemplateItem $templateItem, $name, $xPath = null, $value = null, $class = null )
    {
        if ($this->ID == null)
            throw new PageException( "Can't add child until Page '$this->Name' has not been loaded." );

        $child = new Content( $this, $templateItem, $name );
        $child->setXPath( $xPath );
        $child->setValue( $value );
        $child->setClass( $class );
        $child->Save();
        parent::offsetSet( $name, $child );

        return $child;
    }

    public function SetContent( TemplateItem $templateItem, $name, $xPath = null, $value = null, $class = null )
    {
        if ($this->ID == null)
            throw new PageException( "Can't add child until Page has not been loaded." );

        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if (!$child) {
            $child = new Content( $this, null, $name );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID) {
                if (parent::offsetExists( $child->getName() ))
                    unset($this[ $child->getName() ]);
            }
            $child->setName( $name );
        }
        $child->setTemplateItem( $templateItem );
        $child->setXPath( $xPath );
        $child->setValue( $value );
        $child->setClass( $class );

        $child->Save();
        parent::offsetSet( $name, $child );

        return $child;
    }

    /**
     *
     * @return Content
     */
    private function loadContents()
    {
        if (count( $this ))
            parent::exchangeArray( [] );

        foreach (Content::FromPage( $this ) as $key => $value) {
            $value->Load();
            parent::offsetSet( $key, $value );
        }
    }

    /**
     * Generates this instance.
     */
    public function Generate()
    {
        /*if (!count( $this ))
            return;*/

        $siteXPath = $this->Menu->getSite()->getTemplate()->getDOMXPath();
        $siteBody = $siteXPath->query( '/html/body' )->item( 0 );
        $firstScript = $siteXPath->query( 'script', $siteBody )->item( 0 );

        $pageXPath = $this->getTemplate()->getDOMXPath();
        $pageBody = $pageXPath->query( '/html/body' )->item( 0 );

        if (count( $this ))
            Content::GenerateNode( $pageBody, $this );
        $pageActive = $this->Menu == $this->Menu->getSite()->getMenu()->getActiveMenuItem();

        if ($pageActive) {
            // Generate the the page content
            // Remove all site template body nodes after the site menu nav.
            // Preserve Elements: script, php, \WebStatic\TEMPLATE_DIALOG_IDENTIFIER
            $children = [];
            $navbarFound = false;
            foreach ($siteBody->childNodes as $node) {
                if (!$navbarFound) {
                    if (!$node->attributes)
                        continue;
                    $class = $node->attributes->getNamedItem( 'class' );
                    if ($class && strpos( $class->nodeValue, \WebStatic\NAVBAR_IDENTIFIER ) !== false)
                        $navbarFound = true;
                    continue;
                }
                // Remove all but script, php tags and dialogs
                switch ($node->nodeName) {
                    case 'script':
                    case 'php':
                        break;
                    default:
                        // Don't remove dialogs
                        if (HtmlUtility::HasAttributeIdentifier( $node, \WebStatic\DIALOG_IDENTIFIER ))
                            continue;
                        $children [] = $node;
                        break;
                }
            }
            foreach ($children as $child)
                $siteBody->removeChild( $child );
        } else {
            // If WebStatic::Page::Generate was called on a non active page the page content
            // will be appended to the site content. (i.e. Bootstrap dialogs)
            $pageContent = XPath::SelectNodesContainsAttributeIdentifier( $pageBody,
                \WebStatic\CONTENT_ELEMENT_IDENTIFIER,
                \WebStatic\CONTENT_ELEMENT_TAG )->item( 0 );
            $siteContent = XPath::SelectNodesContainsAttributeIdentifier( $siteBody,
                \WebStatic\CONTENT_ELEMENT_IDENTIFIER,
                \WebStatic\CONTENT_ELEMENT_TAG )->item( 0 );
            foreach ($pageContent->childNodes as $child) {
                $node = $siteBody->ownerDocument->importNode( $child, true );
                // Dialogs are appended to the body to ensure visibility during responsive view.
                if ($this->Menu->getQueryPath()[0] == '#')
                    $siteBody->insertBefore( $node, $firstScript );
                else
                    $siteContent->appendChild( $node );
            }
        }

        // Append page content and copy new script tags to the site.
        /* @var $child DOMNode */
        foreach ($pageBody->childNodes as $child) {
            if ($child->nodeName == 'script') {
                // Omit script tag if it is testing script or id is already defined.
                $idAttribute = $child->attributes->getNamedItem( 'id' );
                if ($idAttribute && (String::EndsWidth( $idAttribute->nodeValue, '-test' ) || $siteXPath->query( "script[@id=\"$idAttribute->nodeValue\"]" )->length > 0))
                    continue;
                // Omit script tag if content already defined
                $srcAttribute = $child->attributes->getNamedItem( 'src' );
                if ($srcAttribute) {
                    if ($siteXPath->query( "//script[@src=\"$srcAttribute->nodeValue\"]" )->length > 0)
                        continue;
                } else {
                    $text = XPath::SaveXPathString( trim( $child->textContent ) );
                    if ($siteXPath->query( "//script[text()[contains(.,$text)]]" )->length > 0)
                        continue;
                }
                $node = $siteBody->ownerDocument->importNode( $child, true );
                $siteBody->appendChild( $node );
                continue;
            }
            // Append only active page content to the site.
            // Non active page content will be appended later.
            if ($pageActive) {
                $node = $siteBody->ownerDocument->importNode( $child, true );
                $siteBody->insertBefore( $node, $firstScript );
            }
        }
    }

    /**
     * @return \stdClass[]
     * @throws \AppStatic\Data\MySql\StoredProcedureEmptyResultException
     * @throws \AppStatic\Data\MySql\StoredProcedureException
     */
    public function getLanguages()
    {
        // Get page languages
        $procedure = new StoredProcedure( 'Page_QueryLanguages' );
        $procedure->Prepare( $this->Name );
        $procedure->Execute();
        $languages = [];

        $languageStrings = [
            'en' => 'English',
            'de' => 'Deutsch',
            'pl' => 'Polskie'];
        do {
            $langCode = null;
            $queryPath = null;
            $procedure->BindResult( [&$langCode, &$queryPath] );
            if ($procedure->Fetch( true )) {

                // Prepend lang code to the url if language does not equal the domain language or user language.
                $url = '';
                $site = $this->Menu->getSite();
                $domainFirstLevel = $site->getDomain()->getDomainName()->getFirstLevel();
                if ($domainFirstLevel != $langCode || $domainFirstLevel != $site->getUserLanguage())
                    $url .= '/' . $langCode;

                $languages [] = (object)[
                    'LangCode'  => $langCode,
                    'Name'      => isset($languageStrings[ $langCode ]) ? $languageStrings[ $langCode ] : $langCode,
                    'QueryPath' => $url . $queryPath];
            } else
                unset($langCode);
        } while (isset($langCode));
        unset($procedure);

        return $languages;
    }

}

final class PageException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}