<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 19.07.2014
 * File: Sitemap.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

/**
 * Description of Sitemap
 * 
 * @package WebStatic
 * @name Sitemap
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Sitemap
{
    // TODO: Sitemap xml generator
    
    // Sitemap formats and guidelines
    // https://support.google.com/webmasters/answer/183668?hl=en
    
    // Use a sitemap to indicate alternate language pages
    // https://support.google.com/webmasters/answer/2620865?hl=en
}
