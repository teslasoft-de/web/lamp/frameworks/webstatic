<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 12.01.2014
 * File: Domains.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;
use AppStatic\Web\DomainName;
use AppStatic\Core\ExceptionBase;
use AppStatic\Data\MySql\StoredProcedureEmptyResultException;
use WebStatic\Common\DataBaseObjectException;
use WebStatic\Common\Expirable;

/**
 * Description of Domains
 * 
 * @package WebStatic
 * @name Domains
 * @version 0.8.9 (12.01.2014 00:20:43)
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Domain extends Expirable
{
    protected $Name;
    protected $DomainName;

    /**
     * Returns the value of the domain name database field.
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Returns the domain name as DomainName object instance.
     * @return DomainName
     */
    public function getDomainName()
    {
        return $this->DomainName;
    }

    function __construct( DomainName $domainName )
    {
        $this->DomainName = $domainName;
        $this->Name = $domainName->getSecondLevel();
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        parent::_Load(
                $procedureName = 'Domain_Query', $paramsArray += array( $this->Name ), $memberRefArray += array( &$this->Name ) );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID())
            $procedureName = 'Domain_Insert';
        else {
            $procedureName = 'Domain_Update';
            $paramsArray [] = $this->ID;
        }

        $paramsArray [] = $this->Name;
        parent::_Save( $procedureName, $paramsArray );
    }

    function SetSite( DomainName $domainName, Template $template, $name, $documentRoot, $language )
    {
        if ($this->ID == null)
            throw new DomainException( "Can't add site until domain has not been loaded." );

        // Create a 'www' Site for the Domain.
        $site = new Site( $this, $domainName, $template );
        $site->setUserDefinedLanguage( $language );
        $site->setTemplate( $template );
        $site->Load();
        $site->setTemplate( $template );
        $site->Load();
        $site->setName( $name );
        $site->setDocumentRoot( $documentRoot );
        $site->Save();

        return $site;
    }

    /**
     *
     * @param bool $loadSite
     * @return Site
     * @throws DomainException
     */
    public function getSite( $loadSite = true )
    {
        if (!$this->ID)
            throw new DomainException( "Domain '$this->DomainName' has not been loaded or is unknown." );

        $site = new Site( $this, $this->DomainName );
        if (!$loadSite)
            return $site;
        try{
            $site->Load();
        }
        catch(DataBaseObjectException $ex){
            if($ex->GetInnerException() instanceof StoredProcedureEmptyResultException)
                \WebStatic\handle_file_not_found();
        }
        if (!$site->getID())
            throw new DomainException( "No site for domain '$this->DomainName' found.", E_USER_ERROR, isset($ex) ? $ex : null  );
        return $site;
    }

}

final class DomainException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}
