<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 13.01.2014
 * File: MenuItem.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Collections\ArrayObjectPropertyBase;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\String;
use AppStatic\Data\XmlUtility;
use DOMNode;
use Exception;
use WebStatic\Common\Expirable;

/**
 * Description of Menu
 * 
 * @package WebStatic
 * @name MenuItem
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class MenuItem extends Expirable
{
    protected $Site;
    protected $Parent;
    protected $TemplateItem;
    protected $Language;
    protected $QueryPath;
    protected $Name;
    protected $Title;
    protected $Index;
    protected $Icon;
    /** @var DOMNode $domNode */
    protected $DOMNode;
    protected $Active;

    /**
     * 
     * @return Site
     */
    public function getSite()
    {
        return $this->Site;
    }

    /**
     * 
     * @return MenuItem
     */
    public function getParent()
    {
        return $this->Parent;
    }

    /**
     * 
     * @return TemplateItem
     */
    public function getTemplateItem()
    {
        if (is_int( $this->TemplateItem ))
            $this->TemplateItem = $this->Site->getTemplate()->FindItem( $this->TemplateItem );
        return $this->TemplateItem;
    }

    public function getLanguage()
    {
        return $this->Language;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getTitle()
    {
        return $this->Title;
    }

    public function getQueryPath()
    {
        $path = $this->QueryPath;
        if($this->QueryPath && $this->QueryPath{0} == '/' && ($this->Site->getUserDefinedLanguage() || $this->getLanguage() != $this->Site->getUserLanguage()))
            $path = '/' . $this->Site->getUserDefinedLanguage() . $path;
        return $path;
    }

    public function getIndex()
    {
        return $this->Index;
    }
    
    public function getIcon()
    {
        return $this->Icon;
    }
    
    /**
     * 
     * @return DOMNode
     */
    public function getDOMNode()
    {
        return $this->DOMNode;
    }

    public function getActive()
    {
        return $this->Active;
    }

    public function setTemplateItem( TemplateItem $TemplateItem )
    {
        $this->TemplateItem = $TemplateItem;
    }

    public function setLanguage( $Language )
    {
        $this->Language = $Language;
    }

    public function setName( $Name )
    {
        $this->Name = $Name;
    }
    
    public function setTitle( $Title )
    {
        $this->Title = $Title;
    }
    
    public function setQueryPath( $QueryPath )
    {
        $this->QueryPath = $QueryPath;
    }

    public function setIndex( $Index )
    {
        $this->Index = $Index;
    }
    
    public function setIcon( $Icon )
    {
        $this->Icon = $Icon;
    }

    function __construct( Site $site, TemplateItem $templateItem = null, $language = null, $queryPath = null, $name = null, $title = null, $icon = null, MenuItem $parent = null )
    {
        $this->Site = $site;
        $this->Parent = $parent;
        $this->TemplateItem = $templateItem;
        $this->Language = $language;
        $this->QueryPath = $queryPath;
        $this->Name = $name;
        $this->Title = $title;
        $this->Icon = $icon;
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        if (empty( $procedureName ))
            $procedureName = 'Menu_Query';

        if (!count( $paramsArray ))
            $paramsArray += array(
                $this->Site,
                $this->Parent,
                $this->Language,
                $this->Name );
        if (!count( $memberRefArray ))
            $memberRefArray += array(
                $site = null,
                $parent = null,
                &$this->TemplateItem,
                &$this->Language,
                &$this->QueryPath,
                &$this->Name,
                &$this->Title,
                &$this->Index,
                &$this->Icon );

        parent::_Load( $procedureName, $paramsArray, $memberRefArray );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'Menu_Insert';
            $paramsArray [] = $this->Site;
        } else {
            $procedureName = 'Menu_Update';
            $paramsArray [] = $this;
        }

        $paramsArray [] = $this->Parent;
        $paramsArray [] = $this->TemplateItem;
        $paramsArray [] = $this->Language;
        $paramsArray [] = $this->QueryPath;
        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->Title;
        if ($procedureName == 'Menu_Update')
            $paramsArray [] = $this->Index;
        $paramsArray [] = $this->Icon;

        parent::_Save( $procedureName, $paramsArray );
    }

    public function Load()
    {
        parent::Load();

        // Determine current url path
        $path = urldecode( parse_url( filter_input( INPUT_SERVER, 'REQUEST_URI' ), PHP_URL_PATH ) );
        if($this->QueryPath){
            $queryPath = $this->QueryPath;
            // Append path separator if missing
            if(String::EndsWidth( $path, '/') && !String::EndsWidth($queryPath, '/'))
                  $queryPath .= '/';
            // Compare default and localized query path.
            $this->Active = $path == $queryPath || $this->getPage( false ) && $path == '/' . $this->getPage( false )->getLanguage() . $queryPath;
        }
        $this->loadChildren();
    }

    /**
     * 
     * @param Site $site
     * @return MenuItem[]
     */
    public static function FromSite( Site $site )
    {
        return self::QueryList(
                'Menu_Query',
                array(
                    $site,
                    $parent = null,
                    $site->getUserDefinedLanguage()
                        ? $site->getUserDefinedLanguage()
                        : $site->getUserLanguage(),
                    $name = null ),
                function( MenuItem &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray) use ( $site ) {
                    $obj = new MenuItem( $site );
                    $objectKeyProperty = 'Name';
                    $procedureName = null;
                    $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }
    
    /**
     * 
     * @param Site $site
     * @param MenuItem $parentItem
     * @return MenuItem[]
     */
    public static function FromParent( Site $site, MenuItem $parentItem )
    {
        return self::QueryList(
                'Menu_Query',
                array(
                    $site,
                    $parentItem,
                    $parentItem->getLanguage(),
                    $name = null ),
                function( MenuItem &$obj = null, &$objectKeyProperty, array $params, array &$memberRefArray )
                use ( $site, $parentItem ) {
            $obj = new MenuItem( $site, null, null, null, null, null, null, $parentItem );
            $objectKeyProperty = 'Name';
            $procedureName = null;
            $obj->_Load( $procedureName, $params, $memberRefArray );
        } );
    }
    
    public function AddChild( TemplateItem $templateItem, $language, $queryPath, $name, $title, $icon = null )
    {
        if ($this->ID == null)
            throw new MenuItemException( "Can't add child until TemplateItem has not been loaded." );

        $child = new MenuItem( $this->Site, $templateItem, $language, $queryPath, $name, $title, $icon, $this );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }
    
    public function SetChild( TemplateItem $templateItem, $language, $queryPath, $name, $title, $icon = null )
    {
        if ($this->ID == null)
            throw new MenuItemException( "Can't add child until TemplateItem has not been loaded." );
        
        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if(!$child){
            $child = new MenuItem( $this->Site, null, $language, null, $name, null, null, $this );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID){
                if(parent::offsetExists( $child->getName() ))
                    unset( $this[ $child->getName() ] );
            }
            $child->setName( $name );
        }
        $child->setTitle( $title );
        $child->setTemplateItem( $templateItem );
        $child->setLanguage( $language );
        $child->setQueryPath( $queryPath );
        $child->setIcon( $icon );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }
    
    /**
     * 
     * @return TemplateItem
     */
    private function loadChildren()
    {
        if(count($this))
            parent::exchangeArray (array());
        
        foreach (self::FromParent( $this->Site, $this ) as $key => $value){
            $value->Load();
            parent::offsetSet( $key, $value );
        }
    }
    
    public function SetPage( Template $pageTemplate, $language, $name, $title, $allowRobots = true )
    {
        // Create the page
        $page = new Page( $this, null, $language, null, $name, null, true, true );
        $page->Load();
        $page->setName( $name );
        $page->setTemplate( $pageTemplate );
        $page->setLanguage( $language );
        $page->setTitle( $title );
        if($allowRobots){
            $page->setRobots( 'index, follow' );
            $page->setGooglebot( 'index, follow' );
            $page->setSlurp( 'index, follow' );
            $page->setMSNBot( 'index, follow' );
            $page->setTeoma( 'index, follow' );
        }

        $page->Save();
        $this->page = $page;
        return $page;
    }
    
    private $SubMenuDropDownNode;
    
    public static function GenerateNode( \DOMNode $parentNode, ArrayObjectPropertyBase $menuItems )
    {
        $template = null;
        // Clear existing template items
        foreach ($menuItems as $menuItem){
            $template = $menuItem->getSite()->getTemplate();
            
            // If sub menu get template items.
            if(!isset($menuItem->SubMenuTitleNode) && count($menuItem) ){
                $menuItem->SubMenuDropDownNode = $menuItem->getTemplateItem()->SubMenu_DropDown->getDOMNode()->cloneNode( true );
                $menuItem->SubMenuDropDownNode->nodeValue = null;
            }
            
            // Query and remove all present items.
            $nodes = $template->getDOMXPath()->query( $menuItem->getTemplateItem()->getXPath(), $parentNode );
            // Remove the first element
            foreach($nodes as $domNode)
                $parentNode->removeChild( $domNode );
        }
        
        // Fix whitespaces.
        $parentNode->nodeValue = preg_replace('~\s*\n\s*~', "\n", $parentNode->nodeValue );
            
        /* @var $menuItem MenuItem*/
        foreach ($menuItems as $menuItem) {
            $menuItem->Generate();
            $domNode = $menuItem->DOMNode;
            $parentNode->appendChild( $domNode );

            if(!count($menuItem))
                continue;

            // TODO: Detect missing template items and display warnings. Figure out how to prevent missing items.
            if($dropDown = $template->getDOMXPath()->query( $template->FindItem( 'SubMenu_DropDown' )->getXPath(), $domNode )->item( 0 ))
                $domNode->removeChild($dropDown);
                
            $domNode->appendChild( $menuItem->SubMenuDropDownNode );
            self::GenerateNode( $menuItem->SubMenuDropDownNode, $menuItem );
        }
    }

    public function Generate()
    {
        $this->DOMNode = $this->TemplateItem->getDOMNode()->cloneNode( true );

        // If menu item is single menu item
        if(!count( $this )){
            
            // If menu item is a page link
            if($this->QueryPath){

                XmlUtility::SetClassAttribute( $this->DOMNode, 'active', $this->Active );
                
                $menuItem = $this->Parent ? 'SubMenuItem' : 'MenuItem';
                
                $this->GenerateLink( $menuItem );
            }
            // Set node values for layout sub menu items.
            else switch($this->TemplateItem->getName()){
                case 'SubMenuItem_Header': 
                    $this->GenerateLink( 'SubMenuItem_Header' );
                    break;
                case 'SubMenuItem_Divider':
                    $this->DOMNode->nodeValue = null;
                    break;
                default:
                    break;
            }
        }
        // Menu item is sub menu.
        else {
            // Set active if any menu child is active
            XmlUtility::SetClassAttribute( $this->DOMNode, 'active', $this->Site->getMenu()->getActiveMenuItem( $this ) != null );
            $this->GenerateLink( 'SubMenu' );
        }
    }

    /**
     * Generates the menu link by searching for content elements inside a possible *_Link element or inside the current MenuItem->TemplateItem.
     *
     * @param $menuItemName
     *
     * @throws InvalidTemplateItemNodeTypeException
     */
    function GenerateLink( $menuItemName )
    {
        if ($this->TemplateItem->offsetExists( $menuItemName . '_Link' )) {
            $templateItem = $this->TemplateItem[ $menuItemName . '_Link' ];
            $node = $this->TemplateItem->QueryChildNode( $this->DOMNode, $templateItem->getName() );
        } else {
            $templateItem = $this->TemplateItem;
            $node = $this->DOMNode;
        }

        // Set link URL
        $queryPath = $this->getQueryPath();
        if($queryPath){
            if($node->nodeName != 'a')
                throw new InvalidTemplateItemNodeTypeException($templateItem, $node->nodeName, 'a',  <<<EOT
The template item node type cannot be used for setting the query path value of the menu item.
Menu Item: $this->Name
Query Path: $queryPath
EOT
                );
            XmlUtility::SetAttribute( $node, 'href', $queryPath );
        }

        // Set link icon
        if($this->Icon) {
            $target = $templateItem->offsetExists( $menuItemName . '_Icon' )
                ? $templateItem->QueryChildNode( $node, $menuItemName . '_Icon' )
                : $node;
            XmlUtility::SetClassAttribute( $target, 'glyphicon*', false );
            XmlUtility::SetClassAttribute( $target, '(^|\s+)((fa)|(fa-*))', false );
            XmlUtility::SetClassAttribute( $target, $this->Icon, $this->Icon );
        }

        // Set link title
        if($this->Title) {
            $target = $templateItem->offsetExists( $menuItemName . '_Title' )
                ? $templateItem->QueryChildNode( $node, $menuItemName . '_Title' )
                : $node;
            XmlUtility::SetHtmlContent( $target, $this->Title );
        }
    }
    
    private $page;

    /**
     *
     * @param bool $initialize
     * @return Page
     */
    public function getPage( $initialize = true )
    {
        if($this->QueryPath && !$this->page){
            
            $page = new Page( $this, null,
                $this->Site->getUserDefinedLanguage()
                    ? $this->Site->getUserDefinedLanguage()
                    : $this->Site->getUserLanguage() );
            $page->Load();
            if(!$page->ID && !$this->Site->getUserDefinedLanguage()){
                $page = new Page( $this );
                $page->Load( $initialize );
            }
            if($page->ID)
                $this->page = $page;
        }
        
        if($initialize && $this->page->count() == 0)
            $this->page->Load();
        return $this->page;
    }
}

class MenuItemException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}