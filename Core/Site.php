<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 09.01.2014
 * File: Site.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Collections\ArrayObjectPropertyBase;
use AppStatic\Core\ExceptionBase;
use AppStatic\Data\Xml\XPath;
use AppStatic\Data\XmlUtility;
use AppStatic\Web\HttpUtility;
use AppStatic\Web\DomainName;
use DOMNode;
use Exception;
use WebStatic\Common\Expirable;

/**
 * Description of Site
 *
 * @package WebStatic
 * @name Site
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Site extends Expirable
{
    protected $Domain;
    protected $Subdomain;
    protected $Template;
    protected $Name;
    protected $DocumentRoot;
    protected $TrackingCode;
    protected $Active;
    protected $UnderConstruction;

    /**
     * @var Menu
     */
    protected $Menu;

    /**
     *
     * @return Domain
     */
    public function getDomain()
    {
        return $this->Domain;
    }

    public function getSubdomain()
    {
        return $this->Subdomain;
    }

    /**
     *
     * @return Template
     */
    public function getTemplate()
    {
        if (is_int( $this->Template )) {
            $this->Template = Template::QueryByID( $this->Template );
            $this->Template->Load();
        }
        return $this->Template;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getDocumentRoot()
    {
        return $this->DocumentRoot;
    }

    public function getTrackingCode()
    {
        return $this->TrackingCode;
    }

    public function getActive()
    {
        return $this->Active;
    }

    public function getUnderConstruction()
    {
        return $this->UnderConstruction;
    }

    public function setTemplate( Template $Template )
    {
        $this->Template = $Template;
    }

    public function setSubdomain( $Subdomain )
    {
        $this->Subdomain = $Subdomain;
    }

    public function setName( $Name )
    {
        $this->Name = $Name;
    }

    public function setDocumentRoot( $DocumentRoot )
    {
        $this->DocumentRoot = $DocumentRoot;
    }

    public function setTrackingCode( $TrackingCode )
    {
        $this->TrackingCode = $TrackingCode;
    }

    public function setActive( $Active )
    {
        $this->Active = $Active;
    }

    /**
     *
     * @return Menu
     */
    public function getMenu()
    {
        return $this->Menu;
    }

    public function setUnderConstruction( $UnderConstruction )
    {
        $this->UnderConstruction = $UnderConstruction;
    }

    private $userLanguage;

    public function getUserLanguage()
    {
        if(!$this->userLanguage)
            foreach(HttpUtility::GetHttpAcceptLanguages() as $lang ) {
                $this->userLanguage = $lang->Country;
                break;
            }

        return $this->userLanguage;
    }

    private $userDefinedLanguage;

    public function getUserDefinedLanguage()
    {
        if(!$this->userDefinedLanguage){
            $path = parse_url( filter_input( INPUT_SERVER, 'REQUEST_URI' ), PHP_URL_PATH );
            $match = array();
            if(preg_match( '~^/([a-z]{2})/~i', $path, $match ))
                $this->userDefinedLanguage = $match[1];
        }
        return $this->userDefinedLanguage;
    }

    public function setUserDefinedLanguage( $userDefinedLanguage )
    {
        $this->userDefinedLanguage = $userDefinedLanguage;
    }


    function __construct( Domain $domain, DomainName $domainName, Template $template = null, $name = null, $documentRoot = null )
    {
        $this->Domain = $domain;
        $this->Subdomain = $domainName->getSubdomain();
        $this->Template = $template;
        $this->Name = $name;
        $this->DocumentRoot = $documentRoot;
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        parent::_Load(
            $procedureName = 'Site_Query',
            $paramsArray += array(
                $this->Domain->getID(),
                $this->Subdomain,
                $this->Name ),
            $memberRefArray += array(
                $domain = null,
                &$this->Template,
                &$this->Subdomain,
                &$this->Name,
                &$this->DocumentRoot,
                &$this->TrackingCode,
                &$this->Active,
                &$this->UnderConstruction ) );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'Site_Insert';
            $paramsArray [] = $this->Domain->getID();
        } else {
            $procedureName = 'Site_Update';
            $paramsArray [] = $this->getID();
        }

        $paramsArray [] = $this->Template;
        $paramsArray [] = $this->Subdomain;
        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->DocumentRoot;
        $paramsArray [] = $this->TrackingCode;

        if (parent::getID()) {
            $paramsArray [] = $this->Active;
            $paramsArray [] = $this->UnderConstruction;
        }
        parent::_Save( $procedureName, $paramsArray );
    }

    public function Load()
    {
        parent::Load();
        if($this->ID)
            $this->Menu = new Menu( $this );
    }

    function GenerateContentLanguage()
    {
        $activeMenuItem = $this->Menu->getActiveMenuItem();
        $lang = $activeMenuItem->getPage()->getLanguage();
        $html = $this->Template->getDOMXPath()->query( '/html' )->item( 0 );

        // Set html content language.
        XmlUtility::SetAttribute( $html, 'xml:lang', $lang );
        XmlUtility::SetAttribute( $html, 'lang', $lang );
        foreach ($this->Template->getDOMXPath()->query( '/comment()' ) as $comment )
            $comment->nodeValue = preg_replace( '~(lang\=")[a-z]{2}-[A-Z]{2}(")~', "$1$lang$2", $comment->nodeValue );

        // Prevent google from translating already translated content.
        if($lang != 'en')
            $this->AddMetaTag( 'google', 'notranslate' );

        // Set document audience language.
        header( "Content-Language: $lang", true );

        // Set language redirect landing page. (google)
        $this->AddAlternateLink( array( 'hreflang' => 'x-default' ), '/' );

        // Tell search engines alternate page languages.
        foreach($activeMenuItem->getPage()->getLanguages() as $language){
            if($language->LangCode != $lang)
                $this->AddAlternateLink( array( 'hreflang' => $language->LangCode ), $language->QueryPath );
        }

        // TODO: Add alternate lang for downloads through http header:
        // Link: <http://es.example.com/>; rel="alternate"; hreflang="es"
    }

    function GenerateLoginForm()
    {
        $form = $this->Template->FindItem( 'LoginForm' );
        if(!$form)
            return;

        if(!isset($_SERVER['HTTPS'])){
            $form->getDOMNode()->parentNode->removeChild( $form->getDOMNode() );
            $email = $form->LoginForm_Email->getDOMNode();
            $email->parentNode->removeChild( $email );
            /* @var $password DOMNode */
            $password = $form->LoginForm_Password->getDOMNode();
            $password->parentNode->removeChild( $password );
        }
        $loginStrings = array(
            'en' => 'Sign In',
            'de' => 'Login',
            'pl' => 'Zaloguj' );
        $form->LoginForm_SubmitButton->getDOMNode()->nodeValue =
            $loginStrings[ $this->getUserDefinedLanguage()
                ? $this->getUserDefinedLanguage()
                : ($this->getUserLanguage() ? $this->getUserLanguage() : 'en') ];
    }

    function GeneratePageTitle()
    {
        $pageTitle = $this->Template->FindItem( 'PageTitle' );
        if(!$pageTitle)
            return;

        XmlUtility::SetHtmlContent( $pageTitle->getDOMNode(), $this->Menu->getActiveMenuItem()->getPage()->getTitle() );
    }

    function GenerateBreadcrumbs()
    {
        $breadcrumbs = $this->Template->FindItem( 'Breadcrumbs' );
        if(!$breadcrumbs)
            return;

        // Collect menu items
        $menuItems = [];
        $getMenuParents = function ( $f, MenuItem $item, array &$menuItems ) {
            if ($item->getParent())
                $f( $f, $item->getParent() );

            $menuItems[] = $item;
        };
        $getMenuParents( $getMenuParents, $this->Menu->getActiveMenuItem(), $menuItems );

        // Get breadcrumbs container and empty it
        $breadcrumbsNode = $breadcrumbs->getDOMNode();
        $breadcrumbsNode->nodeValue = null;

        /** @var TemplateItem $breadcrumbItem */
        $breadcrumbItem = $breadcrumbs->BreadcrumbItem;

        /** @var MenuItem $menuItem */
        foreach($menuItems as $menuItem){

            $breadcrumbNode = $breadcrumbItem->getDOMNode()->cloneNode( true );

            /** @var TemplateItem $linkItem */
            $linkItem = $breadcrumbItem->BreadcrumbItem_Link;
            $linkNode = $breadcrumbItem->QueryChildNode( $breadcrumbNode, $linkItem->getName() );

            $queryPath = $menuItem->getQueryPath();
            if($linkNode->nodeName != 'a')
                throw new InvalidTemplateItemNodeTypeException( $linkItem, $linkNode->nodeName, 'a', <<<EOT
"The template item node type cannot be used for setting the query path value of the breadcrumb menu item.
Breadcrumb Item: {$linkItem->getName()}
Query Path: $queryPath
EOT
                );

            XmlUtility::SetAttribute( $linkNode, 'href', $queryPath );

            $linkNode = $linkItem->offsetExists( 'BreadcrumbItem_Title' )
                ? $linkItem->QueryChildNode( $linkNode, 'BreadcrumbItem_Title' )
                : $linkNode;

            XmlUtility::SetHtmlContent( $linkNode, $menuItem->getTitle() );

            $breadcrumbsNode->appendChild( $breadcrumbNode );
        }
    }

    function GenerateLanguageSelect()
    {
        // TODO: Make languageSelect dynamicly insertable (not only within Menu)
        $menu = $this->Template->FindItem( 'Menu' );
        /** @var TemplateItem $languageSelect */
        $languageSelect = $menu->LanguageSelect;
        if(!$languageSelect)
            return;
        $languageSelectNode = $menu->QueryChildNode($menu->getDOMNode(), $languageSelect->getName());

        // Get template item
        /** @var TemplateItem $dropDown */
        $dropDown = $languageSelect->LanguageSelect_DropDown;
        $dropDownNode = $languageSelect->QueryChildNode( $languageSelectNode, $dropDown->getName() );
        $dropDownNode->nodeValue = null;

        /** @var TemplateItem $langItem */
        $langItem = $dropDown->LanguageSelectItem;

        // Create language selector menu entries.
        foreach ($this->getMenu()->getActiveMenuItem()->getPage()->getLanguages() as $language ) {

            $langNode = $langItem->getDOMNode()->cloneNode( true );

            XmlUtility::SetClassAttribute( $langNode, 'active',
                ($this->getUserDefinedLanguage()
                    ? $this->getUserDefinedLanguage()
                    : $this->getUserLanguage()) == $language->LangCode );

            // TODO: Use implementation from MenuItem::GenerateLink

            /** @var TemplateItem $linkItem */
            $linkItem = $langItem->LanguageSelectItem_Link;
            $linkNode = $langItem->QueryChildNode( $langNode, $linkItem->getName() );

            if($linkNode->nodeName != 'a')
                throw new InvalidTemplateItemNodeTypeException( $linkItem, $linkNode->nodeName, 'a', <<<EOT
"The template item node type cannot be used for setting the query path value of the language menu item.
Language Menu Item: $language->Name
Query Path: $language->QueryPath
EOT
                );

            XmlUtility::SetAttribute( $linkNode, 'href', $language->QueryPath );

            $linkNode = $linkItem->offsetExists( 'LanguageSelectItem_Title' )
                ? $linkItem->QueryChildNode( $linkNode, 'LanguageSelectItem_Title' )
                : $linkNode;

            XmlUtility::SetHtmlContent( $linkNode, $language->Name );

            $dropDownNode->appendChild( $langNode );
        }
    }

    function GenerateJSWarning()
    {
        $jsWarning = $this->Template->getDOMXPath()->query( '//div[@id="js-warning"]/span[2]' )->item( 0 );

        switch($this->Menu->getActiveMenuItem()->getPage()->getLanguage()){
            case 'de':
                XmlUtility::SetHtmlContent( $jsWarning, "Die Inhalte dieser Website werden nur mit eingeschaltetem JavaScript optimal angezeigt. <strong>Bitte aktivieren Sie JavaScript</strong> und laden Sie die Seite erneut." );
                break;
            default:
                XmlUtility::SetHtmlContent( $jsWarning, "This website works best with JavaScript enabled. <strong>Please enable JavaScript</strong>, then refresh this page." );
                break;
        }
    }

    function GenerateScrollSpy()
    {
        $targets = $this->Template->getDOMXPath()->query( '//*' . XPath::ContainsAttributeValue( 'class', 'scrollspy-target' ) );
        $scrollSpy = $this->Template->getDOMXPath()->query( '//div[@id="scrollspy"]/ul')->item( 0 );
        if($targets->length == 0 || !$scrollSpy)
            return;
        /* @var $target DOMNode */
        foreach ($targets as $target){
            $li = $scrollSpy->appendChild( $this->Template->getDOMDocument()->createElement('li') );
            $a = $li->appendChild( $this->Template->getDOMDocument()->createElement('a') );
            XmlUtility::SetAttribute( $a, 'href', '#'.$target->attributes->getNamedItem('id')->nodeValue );
            XmlUtility::SetAttribute( $a, 'class', 'scrollspy-Link' );
            $title = $target->attributes->getNamedItem('title');
            XmlUtility::SetHtmlContent( $a, $title->nodeValue );
            $title->nodeValue = null;
        }
    }

    function GenerateCookieHint()
    {
        $cookieHint = $this->Template->getDOMXPath()->query( '//div[@id="cookie-hint"]' )->item( 0 );

        if(!$cookieHint)
            return;

        $cookie = filter_input( INPUT_COOKIE, 'cookie-hint' );
        if($cookie){
            $cookieHint->parentNode->removeChild( $cookieHint );
            return;
        }
        $cookieHintMessage = $this->Template->getDOMXPath()->query( 'span[2]', $cookieHint )->item( 0 );
        $cookieHintLink = $this->Template->getDOMXPath()->query( 'a', $cookieHint )->item( 0 );

        switch($this->Menu->getActiveMenuItem()->getPage()->getLanguage()){
            case 'de';
                XmlUtility::SetHtmlContent( $cookieHintMessage, "Cookies helfen uns Ihnen eine optimale Nutzererfahrung zu bieten. Durch die Nutzung unser Websites erklären Sie sich damit einverstanden, dass wir Cookies setzen." );
                XmlUtility::SetHtmlContent( $cookieHintLink, "Mehr erfahren" );
                break;
            case 'default';
                XmlUtility::SetHtmlContent( $cookieHintMessage, "Cookies help us to offer an optimal user experience. By using our websites, you agree to our use of cookies." );
                XmlUtility::SetHtmlContent( $cookieHintLink, "Learn more" );
                break;
        }
    }

    public static function GenerateEmailBotInvisibleLinks( $content ){
        return preg_replace( "~([a-zA-Z0-9\._\+-]+)\@((\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,7}|[0-9]{1,3})(\]?))~", <<<EOT
<a class="link" href="mailto:">
    <i class="fa fa-envelope-o notranslate"></i><span class="bot-invisible" prepend="$1" append="$2">@</span>
</a>
EOT
        , $content );
    }

    /**
     * Updates the link parameters from the corresponding menu item determined by the link title.
     *
     * @param \DOMNode $a
     * @param bool $setLinkName Optionally skips the link name itself.
     * @throws SiteException
     */
    function GenerateSiteLink( \DOMNode $a, $setLinkName )
    {
        $title = $a->attributes->getNamedItem( 'title' );
        if(!is_string($title->nodeValue)){
            throw new SiteException(<<<EOT
Site link title missing.
XPath: {$a->getNodePath()}
EOT
            );
        }

        $menuItem = $this->Menu->FindItem( $title->nodeValue );

        if(!$menuItem){
            throw new SiteException(<<<EOT
Site link menu item '$title->nodeValue' not found.
XPath: {$a->getNodePath()}
EOT
            );
        }

        XmlUtility::SetAttribute( $a, 'title', $menuItem->getTitle() );

        $hrefTarget = preg_split( "~#~", $a->attributes->getNamedItem( 'href' )->nodeValue );
        $href = $menuItem->getQueryPath();
        if (count( $hrefTarget ) > 1)
            $href .= "#$hrefTarget[1]";
        XmlUtility::SetAttribute( $a, 'href', $href );

        if ($setLinkName)
            XmlUtility::SetHtmlContent( $a, $menuItem->getTitle() );

        XmlUtility::SetClassAttribute( $a, 'site-link*', false );
    }

    /**
     * Updates all site link parameters from the corresponding menu item determined by the link title. (Link class name: site-link)
     */
    function GenerateSiteLinks()
    {
        foreach ($this->Template->getDOMXPath()->query( '//a' . XPath::ContainsAttributeValue( 'class', 'site-link' ) ) as $a)
            $this->GenerateSiteLink( $a, true );
    }

    /**
     * Updates the link parameters except the link name from the corresponding menu item determined by the link title. (Link class name: site-link, site-link-target)
     */
    function GenerateSiteLinkTargets()
    {
        foreach ($this->Template->getDOMXPath()->query( '//a' . XPath::ContainsAttributeValue( 'class', 'site-link-target' ) ) as $a)
            $this->GenerateSiteLink( $a, false );
    }

    function GenerateScrollToTop()
    {
        $scrollToTop = $this->Template->FindItem( 'ScrollToTop' );
        if(!$scrollToTop)
            return;
        $xpath = $this->Template->getDOMXPath();
        $body = $xpath->query( '/html/body' )->item( 0 );
        $firstScript = $xpath->query( 'script', $body )->item( 0 );
        $body->insertBefore( $scrollToTop->getDOMNode(), $firstScript );
    }

    function GeneratePage()
    {
        // Generate page
        $page = $this->Menu->getActiveMenuItem()->getPage();
        $page->Generate();

        $appendDialogs = function( $appendDialogs, Template $template, ArrayObjectPropertyBase $menuItems )
        {
            /* @var $menuItems MenuItem */
            foreach ($menuItems as $menuItem){
                if($menuItem->QueryPath{0} == '#'){
                    $dialog = new Page( $menuItem );
                    $dialog->Load();
                    $dialog->Generate();
                }
                $appendDialogs( $appendDialogs, $template, $menuItem );
            }
        };
        $appendDialogs( $appendDialogs, $this->Template, $this->Menu );

        $html = $this->Template->getDOMXPath()->query( '/html' )->item( 0 );

        $title = $this->Template->getDOMXPath()->query( 'head/title', $html )->item( 0 );
        XmlUtility::SetHtmlContent( $title, strip_tags( $page->getTitle() ) );

        $this->AddMetaTag( 'application-name', $this->Menu->getActiveMenuItem()->getTitle() );
        $this->AddMetaTag( 'apple-mobile-web-app-title', $this->Menu->getActiveMenuItem()->getTitle() );

        $this->AddMetaTag( 'robots', $page->getRobots() );
        $this->AddMetaTag( 'googlebot', $page->getGooglebot() );
        $this->AddMetaTag( 'slurp', $page->getSlurp() );
        $this->AddMetaTag( 'msnbot', $page->getMSNBot() );
        $this->AddMetaTag( 'teoma', $page->getTeoma() );
        $this->AddMetaTag( 'title', $page->getTitle() );
        $this->AddMetaTag( 'description', strip_tags( $page->getDescription() ) );
        $this->AddMetaTag( 'keywords', $page->getKeywords() );
        // TODO: Set editor name as page author (user management)
        $this->AddMetaTag( 'author', \WebStatic\DEFAULT_SITE_AUTHOR );
        $this->AddMetaTag( 'generator', 'WebStatic ' . \WebStatic\VERSION . ' - Teslasoft' );

        if ($page->getRSS())
            $this->AddAlternateLink(
                array(
                    'type' => 'application/rss+xml',
                    'title' => 'News-Feed' ),
                $page->getRSS(),
                '[@rel="alternate" and @title="News-Feed"]' );
    }

    public function AddMetaTag( $name, $content )
    {
        $head = $this->Template->getDOMXPath()->query( "/html/head" )->item( 0 );
        $title = $this->Template->getDOMXPath()->query( "title", $head )->item( 0 );
        $list = $this->Template->getDOMXPath()->query( "meta[@name=\"$name\"]", $head );
        $meta = $list->length == 0
            ? $head->insertBefore( $head->ownerDocument->createElement( 'meta' ), $title )
            : $list->item( 0 );
        XmlUtility::SetAttribute( $meta, 'name', $name );
        XmlUtility::SetAttribute( $meta, 'content', $content, true );
    }

    public function AddAlternateLink( array $attrubutes, $href, $attributeXpath = null )
    {
        $head = $this->Template->getDOMXPath()->query( "/html/head" )->item( 0 );
        $first = $this->Template->getDOMXPath()->query( 'link', $head )->item( 0 );
        if (!$first)
            $first = $this->Template->getDOMXPath()->query( 'script', $head )->item( 0 );
        $list = $attributeXpath
            ? $this->Template->getDOMXPath()->query( "link$attributeXpath", $head )
            : null;
        $link = !$list || $list->length == 0
            ? $head->insertBefore( $head->ownerDocument->createElement( 'link' ), $first )
            : $list->item( 0 );

        XmlUtility::SetAttribute( $link, 'rel', 'alternate' );
        foreach ($attrubutes as $key => $value)
            XmlUtility::SetAttribute( $link, $key, $value );
        XmlUtility::SetAttribute( $link, 'href', $href );
    }

    public function GenerateHTML()
    {
        $menuItem = $this->Menu->getActiveMenuItem();
        if (!$menuItem)
            \WebStatic\handle_file_not_found();

        $this->GenerateLoginForm();

        $this->Menu->Generate();

        $this->GeneratePageTitle();

        $this->GenerateBreadcrumbs();

        $this->GeneratePage();

        $this->GenerateLanguageSelect();

        $this->GenerateContentLanguage();

        $this->GenerateJSWarning();

        $this->GenerateScrollSpy();

        $this->GenerateCookieHint();

        $this->GenerateSiteLinks();
        $this->GenerateSiteLinkTargets();

        $this->GenerateScrollToTop();

        return $this->Template->getHtml();
    }
}

final class SiteException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}
