<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 23.05.2014
 * File: Menu.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Collections\ArrayObjectPropertyBase;

/**
 * Description of Menu
 * 
 * @package WebStatic
 * @name Menu
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Menu extends ArrayObjectPropertyBase
{
    protected $Site;
    protected $TemplateItem;

    /**
     * 
     * @return Site
     */
    public function getSite()
    {
        return $this->Site;
    }

    /**
     * 
     * @return TemplateItem
     */
    public function getTemplateItem()
    {
        return $this->TemplateItem;
    }

    /**
     *
     * @param ArrayObjectPropertyBase $items
     * @return MenuItem
     */
    public function getActiveMenuItem( ArrayObjectPropertyBase $items = null )
    {
        if ($items === null)
            $items = $this;

        foreach ($items as $item) {
            if ($item->getActive())
                return $item;
            $item = $this->getActiveMenuItem( $item );
            if (!$item)
                continue;
            return $item;
        }
        return null;
    }

    function __construct( Site $site )
    {
        $this->Site = $site;
        /* @var $menu TemplateItem */
        $this->TemplateItem = $site->getTemplate()[ 'Menu' ];
        $this->TemplateItem->Load();
        $this->loadMenuItems();
    }

    /**
     * 
     * @return MenuItem
     */
    private function loadMenuItems()
    {
        if(count($this))
            parent::exchangeArray (array());
        
        foreach (MenuItem::FromSite( $this->Site ) as $key => $value){
            $value->Load();
            parent::offsetSet( $key, $value );
        }
    }
    
    public function AddItem( TemplateItem $templateItem, $language, $queryPath, $name, $title, $icon = null )
    {
        $child = new MenuItem( $this->Site, $templateItem, $language, $queryPath, $name, $title, $icon );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }
    
    public function SetItem( TemplateItem $templateItem, $language, $queryPath, $name, $title, $icon = null )
    {
        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if(!$child){
            $child = new MenuItem( $this->Site, null, $language, null, $name );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID){
                if(parent::offsetExists( $child->getName() ))
                    unset( $this[ $child->getName() ] );
            }
            $child->setName( $name );
        }
        $child->setTitle( $title );
        $child->setTemplateItem( $templateItem );
        $child->setLanguage( $language );
        $child->setQueryPath( $queryPath );
        $child->setIcon( $icon );
        
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }
    
    /**
     * 
     * @param string $name
     * @param \AppStatic\Collections\ArrayObjectPropertyBase $items
     * @return MenuItem
     */
    public function FindItem( $name, ArrayObjectPropertyBase $items = null )
    {
        if ($items == null)
            return $this->FindItem( $name, $this );

        if($items->offsetExists( $name ))
            return $items[ $name ];
        
        foreach ($items as $item) {
            $item = $this->FindItem( $name, $item );
            if ($item)
                return $item;
        }
        return null;
    }

    public function Generate()
    {
        MenuItem::GenerateNode( $this->getTemplateItem()->getDOMNode(), $this );
    }
}