<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 30.06.2014
 * File: Admin.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

/**
 * Description of Admin
 * 
 * @package WebStatic
 * @name Admin
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Admin extends User
{
    protected $SiteAdmin;

}
