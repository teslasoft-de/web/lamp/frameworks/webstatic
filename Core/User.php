<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 12.01.2014
 * File: User.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;
use WebStatic\Common\Expirable;

/**
 * Description of User
 * 
 * @package WebStatic
 * @name User
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class User extends Expirable
{
    
    protected $Site;
    protected $Name;
    protected $PasswordHash;
    protected $PasswordSalt;
    protected $Language;
    protected $Email;
    protected $FirstName;
    protected $LastName;
    protected $Street;
    protected $City;
    protected $ZipCode;
    protected $Country;
    
    private $LoginPasswordHash;

    function getName()
    {
        return $this->Name;
    }

    function __construct( $site, $name, $passwordHash )
    {
        $this->Site = $site;
        $this->Name = $name;
        $this->LoginPasswordHash = $passwordHash;
    }

        
    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        if (empty( $procedureName ))
            $procedureName = 'User_Query';
        if (!count( $paramsArray ))
            $paramsArray += array(
                $this->Site,
                $this->Name );
        if (!count( $memberRefArray ))
            $memberRefArray += array(
                $site = null,
                &$this->Name,
                &$this->PasswordHash,
                &$this->PasswordSalt,
                &$this->Language,
                &$this->Email,
                &$this->FirstName,
                &$this->LastName,
                &$this->Street,
                &$this->City,
                &$this->ZipCode,
                &$this->Country );

        parent::_Load( $procedureName, $paramsArray, $memberRefArray );
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if (!parent::getID()) {
            $procedureName = 'User_Insert';
            $paramsArray [] = $this->Site;
        } else {
            $procedureName = 'User_Update';
            $paramsArray [] = $this;
        }

        $paramsArray [] = $this->Name;
        $paramsArray [] = $this->PasswordHash;
        $paramsArray [] = $this->PasswordSalt;
        $paramsArray [] = $this->Language;
        $paramsArray [] = $this->Email;
        $paramsArray [] = $this->FirstName;
        $paramsArray [] = $this->LastName;
        $paramsArray [] = $this->Street;
        $paramsArray [] = $this->City;
        $paramsArray [] = $this->ZipCode;
        $paramsArray [] = $this->Country;

        parent::_Save( $procedureName, $paramsArray );
    }

}
