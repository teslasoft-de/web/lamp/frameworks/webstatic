<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 12.01.2014
 * File: Template.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic\Core;

use AppStatic\Core\ErrorHandler;
use AppStatic\Core\ExceptionBase;
use AppStatic\Data\XmlUtility;
use AppStatic\IO\Path;
use AppStatic\Web\HtmlUtility;
use DOMDocument;
use DOMXPath;
use Exception;
use InvalidArgumentException;
use LibXMLError;
use WebStatic\Common\DatabaseObject;
use WebStatic\Common\Modifiable;

/**
 * Description of Template
 *
 * @package WebStatic
 * @name Template
 * @version 0.8.9
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Template extends Modifiable
{
    protected $Name;
    protected $Path;

    /**
     *
     * @var DOMDocument
     */
    protected $DOMDocument;

    /**
     *
     * @var DOMXPath
     */
    protected $DOMXPath;

    public function getName()
    {
        return $this->Name;
    }

    public function getPath()
    {
        return $this->Path;
    }

    /**
     *
     * @return DOMDocument
     */
    public function getDOMDocument()
    {
        return $this->DOMDocument;
    }

    /**
     *
     * @return DOMXPath
     */
    public function getDOMXPath()
    {
        return $this->DOMXPath;
    }

    public function setName( $Name )
    {
        $this->Name = $Name;
    }

    public function setPath( $Path )
    {
        $this->Path = $Path;
    }

    function __construct( $name = null, $path = null )
    {
        $this->Name = $name;
        $this->Path = $path;
    }

    static $HTML5Elements = array(
        'aside',
        'canvas',
        'header',
        'fecomposite',
        'fegaussianblur',
        'feflood',
        'femerge',
        'femergenode',
        'feoffset',
        'filter',
        'footer',
        'g',
        'image',
        'nav',
        'svg');
    static $DOMUnknownElementPattern;
    static function getDOMUnknownElementPattern(){
        if(empty(self::$DOMUnknownElementPattern)){
            $pattern = implode( '|',self::$HTML5Elements );
            self::$DOMUnknownElementPattern = "~Tag ($pattern) invalid~i";
        }
        return self::$DOMUnknownElementPattern;
    }

    /**
     * Loads the template into a xml DOMDocument.
     * Skip's validation of user defined unknown html elements specified in the Template::$DOMUnknownElements array.
     * @param boolean $html5
     * @throws Exception
     * @throws TemplateException
     */
    public function Load( $html5 = true )
    {
        if(!$this->ID)
            parent::Load();

        $path = Path::IsPathRooted($this->Path)
            ? $this->Path
            : Path::Combine(DOCUMENT_ROOT, $this->Path );

        // create a DOM document and load the HTML data
        $htmlDoc = new DOMDocument();
        $htmlDoc->formatOutput = false;
        $htmlDoc->preserveWhiteSpace = true;
        $this->DOMDocument = $htmlDoc;
        if($html5)
            libxml_use_internal_errors( true );
        $loadHTMLFile = new ErrorHandler( array( $htmlDoc, 'loadHTMLFile' ));
        // this doesn't dump out any warnings
        $loadHTMLFile->Invoke($path);
        $messages = '';
        try {

            // Check for hard load errors.
            if ($loadHTMLFile->getHasErrors()) {

                foreach ($loadHTMLFile->getErrors() as $error){
                    if(preg_match( $this->getDOMUnknownElementPattern(), $error->getMessage() ))
                        continue;

                    $messages .= <<<EOT

{$error->getMessage()}
EOT;
                }
            }
            // Check for parse errors.
            else{
                $lines = array();
                /* @var $error LibXMLError */
                foreach( libxml_get_errors() as $key => $error){

                    // Skip DOM unknown elements.
                    if(preg_match( $this->getDOMUnknownElementPattern(), $error->message ))
                        continue;
                    $key++;
                    switch($error->level){
                        case LIBXML_ERR_WARNING:
                            $level = "Warning";
                            break;
                        case LIBXML_ERR_ERROR:
                            $level = "Error";
                            break;
                        case LIBXML_ERR_FATAL:
                            $level = "Fatal";
                            break;
                        default:
                            $level = "Unknown";
                            break;
                    }
                    $message = trim($error->message);
                    $lines []= "$key. $level: $message (Line $error->line)";
                }
                if(count($lines)){
                    $messages = <<<EOT
Template Path: $path

EOT;
                    $messages .= implode(DIRECTORY_SEPARATOR, $lines);
                }
            }

            if(!empty($messages)){
                $messages = <<<EOT
Template '$this->Name' has errors.
$messages
EOT;
                throw new TemplateException( $messages );
            }
            $this->DOMXPath = new DOMXPath( $htmlDoc );
            $this->Modified = date( 'Y-m-d H:i:s', filemtime( $path ) );
        } catch (Exception $ex) {}

        if($html5)
            libxml_use_internal_errors(false);
        if(isset($ex))
            throw $ex;

        if($this->ID)
            $this->loadTemplateItems();
    }

    protected function _Load( &$procedureName, array &$paramsArray, array &$memberRefArray )
    {
        parent::_Load(
            $procedureName = 'Template_Query',
            $paramsArray += array( $id = null, $this->Name ),
            $memberRefArray += array( &$this->Name, &$this->Path ));
    }

    protected function _Save( &$procedureName, array &$paramsArray )
    {
        if(!parent::getID()){
            $procedureName = 'Template_Insert';
        }
        else{
            $procedureName = 'Template_Update';
            $paramsArray []= $this->getID();
        }

        $paramsArray []= $this->Name;
        $paramsArray []= $this->Path;
        if($procedureName == 'Template_Update')
            $paramsArray []= $this->Modified;
    }

    protected function OnPropertyOffsetGet( $offset )
    {
        $item = $this->FindItem( $offset );
        return $item ? $item : parent::OnPropertyOffsetGet( $offset );
    }

    static $TemplateMode = false;

    protected function OnPropertyOffsetSet( $offset, $value )
    {
        if(!self::$TemplateMode)
            return parent::OnPropertyOffsetSet( $offset, $value );

        if(!is_string( $value ))
            throw new TemplateException( 'Offset property value is invalid. String expected. \''. gettype( $value ) .'\' given.' );

        $this->SetItem( $offset, $value );
        return true;
    }

    /**
     *
     * @param string $name
     * @param string $targetXPath
     * @return TemplateItem
     * @throws TemplateItemException
     */
    public function AddItem( $name, $targetXPath )
    {
        if (!$this->ID)
            throw new TemplateItemException( "Can't add TemplateItem until Template has not been loaded." );

        $child = new TemplateItem( $this, $name, $targetXPath );
        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }

    public function SetItem( $name, $targetXPath )
    {
        if (!$this->ID)
            throw new TemplateItemException( "Can't set TemplateItem until Template has not been loaded." );

        $child = parent::offsetExists( $name ) ? $this[ $name ] : null;
        if(!$child){
            $child = new TemplateItem( $this, $name );
            $child->Load();
            // Ensure name case changes will be also overwritten.
            if ($child->ID){
                if(parent::offsetExists( $child->getName() ))
                    unset( $this[ $child->getName() ] );
            }
            $child->setName( $name );
        }
        $child->setTargetXPath( $targetXPath );

        $child->Save();
        parent::offsetSet( $name, $child );
        return $child;
    }

    /**
     *
     * @return TemplateItem
     */
    private function loadTemplateItems()
    {
        if(count($this))
            parent::exchangeArray (array());

        /* @var $value DatabaseObject */
        foreach (TemplateItem::FromTemplate( $this ) as $key => $value){
            $value->Load();
            parent::offsetSet($key, $value);
        }
    }

    /**
     *
     * @param integer|string $id
     * @param TemplateItem $item
     * @return TemplateItem
     * @throws InvalidArgumentException
     */
    public function FindItem( $id, TemplateItem $item = null )
    {
        if(!is_int( $id ) && !is_string( $id ))
            throw new InvalidArgumentException( "\$id parameter value is invalid. Value: '$id'", E_USER_ERROR );

        /* @var $item TemplateItem */
        foreach ((!$item ? $this : $item) as $item){
            if(is_int( $id ) && $item->getID() == $id || $item->getName() == $id)
                return $item;

            $temp = $this->FindItem( $id, $item );
            if($temp)
                return $temp;
        }

        return null;
    }

    public function getHtml( \DOMNode $node = null )
    {
        XmlUtility::IndentChildNodes( $this->DOMXPath, $node ? $node : $this->DOMDocument->documentElement );
        $html = $this->DOMDocument->saveHTML( $node );
        // Convert non-self-closing HTML tags to self-closing XHTML tags.
        // TODO: Export non-self-closing to self-closing HTML Tag conversion to AppStatic
        return HtmlUtility::ConvertHtmlToXHtml($html);
    }
}


final class TemplateException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}