<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 26.08.2016, 12:56
 * File: cache_control.php
 * Encoding: UTF-8
 * Project: WebStatic
 * */

namespace WebStatic;

use AppStatic\Collections\ArrayObjectPropertyBase;
use AppStatic\Core\ExceptionHandler;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\Web\Http\HttpCacheControl;
use WebStatic\Core\Domain;
use WebStatic\Core\Site;

/**
 * Enables the Cache Control depending on debugging.
 *
 * @param Domain $domain
 */
function CacheControl(Domain $domain)
{
    $query = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'), PHP_URL_QUERY);
    // Disable Cache Control if debugging is enabled.
    if (filter_input(INPUT_COOKIE, 'XDEBUG_SESSION') && !($query && strpos($query, 'XDEBUG'))) {
        ExceptionHandler::SetSaveTrace(false);

        return;
    }

    $lastModified = queryLastModified($domain);
    // Enable Cache Control if last modified timestamp could be determined, otherwise the request url is invalid.
    if ($lastModified) {
        HttpCacheControl::Init(strtotime($lastModified), 60);
        // Die immediately if not modified.
        if (http_response_code() == 304)
            exit();
    }
}

/**
 * Returns the last modified timestamp of the current site.
 *
 * @param Domain $domain
 *
 * @return int 'null' if no site for the given request parameters was found.
 */
function queryLastModified(Domain $domain)
{
    $site = $domain->getSite(false);
    // Get user defined language or browser language.
    $language = $site->getUserDefinedLanguage() ? $site->getUserDefinedLanguage() : $site->getUserLanguage();
    // Strip user defined language and trailing slash from query path.
    $path = preg_replace('~^/[a-z]{2}(/)~', '$1', parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'), PHP_URL_PATH));
    $path = preg_replace('~^(/.*)/$~', '$1', $path);
    $lastModified = null;

    $procedure = new StoredProcedure('Query_LastModified');
    $procedure->Prepare($domain->getID(), $site->getSubdomain(), $language, $path);
    $procedure->Execute();
    $procedure->BindResult([&$lastModified]);
    $procedure->Fetch(true);
    unset($procedure);

    return $lastModified;
}

/**
 * Returns the last modified site object.
 *
 * @param Site $site
 *
 * @return int
 */
function getLastModified(Site $site)
{
    $modified = $site->getCreated();

    $findLastModified = function ($findYoungestContent, ArrayObjectPropertyBase $contents, &$modified) {
        if ($contents instanceof Modifiable && $contents->getModified() > $modified)
            $modified = $contents->getModified();
        foreach ($contents as $content) {
            if ($content->getModified() > $modified)
                $modified = $content->getModified();

            $findYoungestContent($findYoungestContent, $content, $modified);
        }
    };
    $findLastModified($findLastModified, $site->getTemplate(), $modified);
    $findLastModified($findLastModified, $site->getMenu()->getActiveMenuItem(), $modified);
    $findLastModified($findLastModified, $site->getMenu()->getActiveMenuItem()->getPage(), $modified);
    $findLastModified($findLastModified, $site->getMenu()->getActiveMenuItem()->getPage()->getTemplate(), $modified);

    return $modified;
}