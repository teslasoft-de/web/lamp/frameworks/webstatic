<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 08.01.2014
 * File: index.php
 * Encoding: UTF-8
 * Project: WebStatic 
 * */

namespace WebStatic;

use AppStatic\IO\Path;
use AppStatic\Web\DomainName;
use AppStatic\Web\HttpUtility;
use WebStatic\Core\Domain;
use WebStatic\Core\MenuItem;
use WebStatic\Core\Site;

require_once __DIR__ . '/config/config.inc.php';
require_once __DIR__ . '/cache_control.php';

\WebStatic\Configuration\mysqli_connect_www();

// Load and require domain
global $domain;
$domain = new Domain(DomainName::getCurrent());
$domain->Load();
if (!$domain->getID())
    trigger_error('Unknown domain ' . $domain->getName(), E_USER_ERROR);

// Perform login
Login();

// Load domain site
$site = $domain->getSite();

// Do language redirect
LanguageRedirect($site);

// Generate site
eval('?>' . $site->GenerateHTML() . '<?php ');

// Finally, activate cache control when processing finished successfully without exceptions.
CacheControl($domain);

function Login()
{
    $loginRequest = filter_input(INPUT_POST, 'login');
    if (!$loginRequest)
        return;
}

function LanguageRedirect(Site $site)
{
    // Redirect to user language or fallback to english if not available.
    if (parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'), PHP_URL_PATH) == '/') {
        if (!$site->getMenu()->getActiveMenuItem() ||
            $site->getUserLanguage() != $site->getDomain()->getDomainName()->getFirstLevel()
        ) {
            $redirect = function ($lang) use ($site) {
                $menuItem = new MenuItem($site, null, $lang, null, 'Home');
                $menuItem->Load();
                if ($menuItem->getID()) {
                    HttpUtility::MovePage(302, "/$lang/");
                    exit();
                }
            };
            foreach (HttpUtility::GetHttpAcceptLanguages() as $lang)
                $redirect($lang->Country);
            $redirect('en');
        }
    }
}

/**
 * Throws 404 if requested file does not exist.
 */
function handle_file_not_found()
{
    /** @noinspection PhpIncludeInspection */
    require DOCUMENT_ROOT . '/redirect.inc.php';
    $path = urldecode(parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'), PHP_URL_PATH));
    if (isset($permanent[ $path ]))
        HttpUtility::MovePage(301, $permanent[ $path ]);
    else {
        http_response_code(404);
        /** @noinspection PhpIncludeInspection */
        include Path::Combine(DOCUMENT_ROOT, $_SERVER['ERROR_DOCUMENT']);
    }
    die();
}