<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 08.01.2014
 * File: GoogleAnalytics.php
 * Encoding: UTF-8
 * Project: WebStatic 
 **/

namespace WebStatic;
use AppStatic\Core\ErrorHandler;
use AppStatic\Web\DomainName;
use WebStatic\Core\Domain;

if(!isset($site) && function_exists( '\WebStatic\Configuration\mysqli_connect_www' )){
    (new ErrorHandler( function () {
        \WebStatic\Configuration\mysqli_connect_www();
        $domain = new Domain( new DomainName( DomainName::getCurrent()->getThirdLevel() ) );
        $domain->Load();
        if($domain->getID()) {
            global $site;
            (new ErrorHandler( function () use ( $domain ) {
                global $site;
                $site = $domain->getSite();
            } ))->Invoke();
        }
    } ))->TryCatch();
}
if(!isset($site) || !$site->getTrackingCode())
    return;
?>
<script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create','<?php echo $site->getTrackingCode(); ?>','auto');
            ga('set','anonymizeIp',true);
            ga('set','forceSSL',true);
            ga('require','linkid','linkid.js');
            ga('send','pageview');
            var gaProperty='<?php echo $site->getTrackingCode(); ?>';
            var disableStr='ga-disable-'+gaProperty;
            if (document.cookie.indexOf(disableStr+'=true')>-1)
                window[disableStr] = true;
            function gaEnabled(){
                return !window[disableStr];
            }
            function gaOptOut(){
                if(!gaEnabled())return;
                document.cookie=disableStr+'=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableStr]=true;
            }
            function gaOptIn(){
                if(gaEnabled())return;
                document.cookie=disableStr+'=; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                try{delete window[disableStr];}
                catch(e){window[disableStr]=undefined;}
            }
        </script>
