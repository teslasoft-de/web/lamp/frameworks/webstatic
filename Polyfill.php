<?php

/** @noinspection PhpIncludeInspection */
require_once "{$_SERVER['DOCUMENT_ROOT']}/AppStatic/Include.inc.php";

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 27.10.2014
 * File: Polyfill.php
 * Encoding: UTF-8
 * Project: WebStatic
 * */

// Define output helper functions
function echoStylesheet($media, $path){
    echo <<<HTML
<link rel="stylesheet" media="$media" type="text/css" href="$path">

HTML;
}

/**
 * Prints default Polyfill includes.
 */
function echoPolyfill()
{
    echo <<<HTML
        <script src="/WebStatic/js/dist/WebStatic.polyfill.min.js"></script>
        <!--[if lt IE 9]>
        <script src="/WebStatic/js/vendor/html5shiv-printshiv.min.js"></script>
        <![endif]-->

HTML;
}

/**
 * Prints enhance includes.
 */
function echoScript( array $configItems ){

    $tests = array();
    foreach($configItems as $testName => $mediaTypes){
        $mediaTypes = implode('\',\'',$mediaTypes);
        $tests[]=<<<JS
polyfill.configItem('$testName', ['$mediaTypes'])
JS;
    }
    $tests = implode(',',$tests);
    $script = <<<HTML
        <script>
            var polyfill = new Core.Polyfill('polyfill');
            polyfill.enhance([$tests]);
        </script>

HTML;
    echo $script;
}

/**
 * Prints a error message before the body content.
 * @param $message
 */
function onError($message){
    http_response_code(500);
    echo <<<HTML
<!-- PHP-Error: $message -->
<style>
body:before {
  white-space: pre;
  font-family: monospace;
  content: "PHP-Error: $message\A"; }
</style>

HTML;
    exit();
}

function echoStylesheets($cookie, $defaultMediaTypes){
    // If no cookie is set generate default.
    if (!$cookie)
        $cookie = implode(',', $defaultMediaTypes) . '|' . "/css/{$defaultMediaTypes[0]}.css";

    // Validate media types and path.
    $params = explode('|', $cookie);
    if(count($params) < 2)
        return "Polyfill failed! Invalid cookie value. '$cookie'";

    $mediaTypes = array('all', 'braille', 'embossed', 'handheld', 'print', 'projection', 'screen', 'speech', 'tty', 'tv');
    $media = empty($params[0]) ? $mediaTypes[0] : $params[0];
    $path = $params[1];

    foreach (explode(',',$media) as $mediaType)
        if (!in_array($mediaType, $mediaTypes))
            return "Polyfill failed! Unknown media type '$media'.";

    $relativePath = AppStatic\IO\Path::IsPathRooted($path)
        ? substr($path, 1)
        : $path;

    if (!is_file($relativePath))
        return "Polyfill failed! Path is invalid. '$path'.";

    echoStylesheet($media, $path);
    echoPolyfill();
    return true;
}

/** @noinspection PhpUndefinedVariableInspection */
if(empty($polyfill) || !is_array($polyfill))
    onError('Polyfill failed! $polyfill must be type of array.');

if(!empty($enhance) && !is_array($enhance))
    onError('Polyfill failed! $enhance must be type of array.');

foreach($polyfill as $cookieName => $defaultMediaTypes) {

    if(!is_string($cookieName))
        onError('Polyfill enhance failed! Cookie name must be type of string.');
    if(!is_array($defaultMediaTypes) || count($defaultMediaTypes) < 1)
        onError("Polyfill failed! Media types must be type of array and should contain at least one element.");

    $cookie = null;
    $cookies = array();
    foreach($_COOKIE as $name => $value) {
        if (strpos($name, $cookieName) === 0) {
            // Read and validate cookie value
            $cookies [$name]=filter_input(INPUT_COOKIE, $name);
        }
    }

    if(count($cookies) == 0)
        $cookies []= null;

    $errors = array();
    foreach ($cookies as $name => $cookie) {
        $cookie = echoStylesheets($cookie, $defaultMediaTypes);
        if ($cookie !== true) {
            $errors []= $cookie;
            setcookie(str_replace('_','.',$name), null, -1, '/');
        }
    }

    if(count($errors))
        onError(implode("\r\n", $errors));
}

if(!empty($enhance) && is_array($enhance)) {
    foreach ($enhance as $testName => $mediaTypes) {
        if(!is_string($testName))
            onError('Polyfill enhance failed! Test name must be type of string.');
        if (!is_array($mediaTypes) || count($mediaTypes) < 1)
            onError("Polyfill enhance failed! Media types for test '$testName' must be type of array and should contain at least one element.");
    }
    echoScript($enhance);
}
