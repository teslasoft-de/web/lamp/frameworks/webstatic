<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 25.08.2016, 00:43
 * File: error.php
 * Encoding: UTF-8
 * Project: WebStatic
 * */

use WebStatic\Common\Language;

// Load AppStatic Framework (Autoload, Error Handling, Trace)
/** @noinspection PhpIncludeInspection */
require_once __DIR__ . '/config/config.inc.php';

// Get redirect or response code.
$status = filter_input(INPUT_SERVER, 'REDIRECT_STATUS');
if ($status != http_response_code())
    $status = http_response_code();

$isKnownStatusCode = Language::$Error->offsetExists( $status );

// Get title from locales or use default.
$title = $isKnownStatusCode
    ? Language::$Error[ $status ][0]
    : Language::$Error->Title;
$sub_title = Language::$Error->SubTitle;
$alert = Language::$Error->Alert;

if (!isset($message))
    $message = ($isKnownStatusCode)
        ? Language::$Error[ $status ][1]
        : 'Please supply a valid HTTP status code.';

// Load error document for current status or fall back to 500 error document
$file = \WebStatic\TEMPLATE_PATH . "$status.phtml";
if (!is_file($file))
    $file = \WebStatic\TEMPLATE_PATH . "500.phtml";

/** @noinspection PhpIncludeInspection */
require_once $file;