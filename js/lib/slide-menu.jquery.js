/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.10.2014
 * File: slide-menu.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

 // Adds slide from left (YouTube style) effect for site menu.
$(document).ready(function() {
    // Disable bootstrap menu collapse animation.
    $('.navbar-collapse').on('hide.bs.collapse', function() {
        return false;
    });

    var slideMenu = '#slidemenu';
    var slideNav = '#slide-nav';
    var toggler = '.navbar-toggle';
    var pageWrapper = '#page-contents';
    var navigationWrapper = '.navbar-header';
    var scrollspyContainer = '#scrollspy-container';
    var heightCol = '#navbar-height-col';
    var menuWidth = '100%';
    var slideWidth = '80%';
    var menuNeg = '-100%';
    var slideNeg = '-80%';
    var slideElements = 'body, .navbar, '+navigationWrapper+', '+pageWrapper;
    
    //stick in the fixed 100% height behind the navbar but don't wrap it
    $(slideNav+'.navbar-inverse').after($("<div id=\""+heightCol.substr(1)+"\" class=\"inverse\"></div>"));
    $(slideNav+'.navbar-default').after($("<div id=\""+heightCol.substr(1)+"\"></div>"));
    /*$("<div id=\""+heightCol.substr(1)+"\" class=\"inverse\"></div>").appendTo(slideNav+'.navbar-inverse').after();
    $("<div id=\""+heightCol.substr(1)+"\"></div>").appendTo(slideNav+'.navbar-default');*/

    var $window = $(window);
    
    var toggleScroll = function(active){
        var state = active ? 'auto' : "hidden";
        $('body').css("overflow-x", state);
        $('body').css("overflow-y", state);

        if (active)
            $window.disableScroll('undo');
        else
            $window.disableScroll({ scrollCallback: function(e){
                // Prevent scrolling or swipig on entire window except the slidenav and dialogs.
                if($(e.target).closest(slideMenu).length > 0 || $(e.target).closest('.modal').length > 0)
                    return true;
            } });
    };
    function SlideToggle(eventSource, active)
    {
        //$('.navbar-collapse').css('min-height', Response.band(768) ? '' : jQuery(window).height());
        $('.navbar-collapse').css('max-height', jQuery(window).height());

        $(slideMenu).stop().animate({
            left: active ? menuNeg : '0px'
        });
        $(heightCol).stop().animate({
            left: active ? slideNeg : '0px'
        });
        $(pageWrapper+','+scrollspyContainer).stop().animate({
            left: active ? '0px' : slideWidth
        });
        $(navigationWrapper).stop().animate(
                {left: active ? '0px' : slideWidth},
        {complete: function() {
                // Toggle blur after slide in.
                if(!active)
                    blur(pageWrapper, !active);
                // Turn scrollbars on after collapsing.
                toggleScroll(active);
                $(slideNav).trigger('toggled',[!active]);
            }});

        // Toggle slide active state
        eventSource.toggleClass('slide-active', !active);
        $(slideMenu).toggleClass('slide-active', !active);
        $(slideElements).toggleClass('slide-active', !active);

        // Prevent touch movements on page content while slide active.
        // Turn scrollbars off prior collapsing.
        if (!active)
            toggleScroll(active);
    }

    // Handle click on menu slide button
    $(slideNav).on("click", toggler, function(e) {
        
        var element = $(this);
        var slideActive = element.hasClass('slide-active');
        $(slideNav).trigger('toggle',[slideActive]);
        
        setTimeout(function(){
            if ($(slideNav).hasClass('slide-active')){
                // Toggle blur before slide out.
                blur(pageWrapper, false, function() {
                    SlideToggle(element, slideActive);
                    if(!$('body > ul.scroll-to-top').hasClass('hide'))
                        $('body > ul.scroll-to-top').fadeIn();
                });
            }
            else{
                $('body > ul.scroll-to-top').fadeOut();
                SlideToggle(element, slideActive);
            }
        },50);
    });

    $(window).on('throttledresize', function() {
        // Clip navbar to screen height.
        //$('.navbar-collapse').css('min-height', Response.band(768) ? '' : jQuery(window).height());
        $('.navbar-collapse').css('max-height', jQuery(window).height());
        
        // If not in responsive view and navbar is active but hidden enable page content.
        if (Response.band(768) &&
                $('.navbar-toggle').is(':hidden') &&
                $(slideNav).hasClass('slide-active')) {
            
            if (!$('.modal-backdrop').length)
                // If no dialog active unblur and enable page content scrollbars.
                blur(pageWrapper, false, function() {
                    toggleScroll(true);
                });
            else
                // Don't unblur if dialog active.
                // Enable page content scrollbars after dialog is closed.
                $('.modal').one('hidden.bs.modal', function() {
                    toggleScroll(true);
                });
        }
        // If navbar is visible blur page content.
        else if ($(slideNav).hasClass('slide-active'))
            blur(pageWrapper, true, function() {
                toggleScroll(false);
            });
    });

    $(slideNav+", "+heightCol).on('swipeleft', function() {
        if($(slideNav).hasClass('slide-active'))
            $(".navbar-toggle").click();
    });

    // Get swipe event start coordinates
    touchPos = 21;
    $('body').on('touchstart', function(e) {
        touchPos = e.originalEvent.touches[0].pageX;
    });
    $('body').on('mousedown', function(e) {
        touchPos = e.originalEvent.clientX;
    });

    // Close slide-nav on content click.
    $(pageWrapper).on('mousedown', function(e) {
        if (!Response.band(768) && $(slideNav).hasClass('slide-active'))
            $(".navbar-toggle").click();
    });

    // Open slide-nav on swipe events from left.
    $('body').on('swiperight', function() {
        if (touchPos <= 20 && !Response.band(768) && !$(slideNav).hasClass('slide-active')) {
            $(".navbar-toggle").click();
        }
    });
});