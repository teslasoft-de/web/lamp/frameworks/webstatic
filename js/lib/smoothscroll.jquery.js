/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 23.10.2014
 * File: smoothscroll.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */
 
(function($){
 
    smoothScroll = {
        topScrollOffset: -84,
        scrollTiming: 1500,
        pageLoadScrollDelay: 1000,
        easeIn: 'easeInQuint',
        easeOut: 'easeOutExpo',
        delayCallback: null,
        target: $($.browser.webkit ? 'body': 'html'),
        hashLinkClicked: function(e){
 
            // current path
            var temp    = window.location.pathname.split('#');
            var curPath = temp[0]/*smoothScroll.addTrailingSlash(temp[0])*/;
 
            // target path
            var link       = $(this).attr('href');
            var linkArray  = link.split('#');
            var navId      = (typeof linkArray[1] !== 'undefined') ? linkArray[1] : null;
            var targetPath = linkArray[0] ? linkArray[0]/*smoothScroll.addTrailingSlash()*/ : null;
 
            // ScrollTo the hash id if the target is on the same page
            if ((!targetPath || targetPath == curPath) && navId) {
                e.preventDefault();
                var delay = $(this).attr('scrollspy-delay');
                if(delay)
                    setTimeout( function(){
                        smoothScroll.scrollToElement('#'+navId);
                    }, delay);
                else
                    smoothScroll.scrollToElement('#'+navId);
 
            // otherwise add '_' to hash so the browser will ignore it.
            } else if (navId) {
                e.preventDefault();
                navId = smoothScroll.generateTempNavId(navId);
                window.location = targetPath+'#'+navId;
            }
        },
        
        addTrailingSlash: function(str){
            lastChar = str.substring(str.length-1, str.length);
            if (lastChar != '/')
                str = str+'/';
            return str;
        },
        scrollToElement: function(whereTo, delayed){
            if($(whereTo).length === 0)
                return;
            if(smoothScroll.delayCallback && (typeof smoothScroll.delayCallback == 'function')){
                if(typeof delayed === 'undefined')
                    delayed = true;
                var delay = smoothScroll.delayCallback(whereTo);
                if(delayed && delay){
                    setTimeout( function(){
                        smoothScroll.scrollToElement(whereTo, false);
                    }, delay);
                    return;
                }
            }
            var offset = $(whereTo).offset().top + (typeof smoothScroll.topScrollOffset ==
                'function' ? smoothScroll.topScrollOffset()
                : smoothScroll.topScrollOffset);
            if(offset < 0)
                offset = 0;

            $(window).smoothWheel({remove: true});
            // Animate half scroll distance with ease-in and remaining scroll distance with ease-out function.
            smoothScroll.target.stop().animate({scrollTop: offset-(offset-$(window).scrollTop()) / 2},
                smoothScroll.scrollTiming / 2,
                smoothScroll.easeIn,
                function(){
                    smoothScroll.target.animate({scrollTop: offset},
                        smoothScroll.scrollTiming / 2, 
                        smoothScroll.easeOut,
                        function(){
                            $(whereTo).trigger('smoothScrolled');
                            // When done, add hash to url (default click behaviour).
                            if(whereTo.substring(0,1) === '#')
                            window.location.hash = smoothScroll.generateTempNavId(whereTo.substring(1, whereTo.length));
                            $(window).smoothWheel();
                        });
                });
                
        },
        scrollBody: function(){
            target.mousewheel(function(event, delta) {
                
                if (delta < 0)
                    target.stop().animate({scrollTop:'+=100'}, 1500 );
                else
                    target.stop().animate({scrollTop:'+=100'}, 1500 );
                //e.preventDefault();
                return false;
            });
        },
        // jQuery scrollTo plugin
        /*scrollToElement: function(whereTo){
            $.scrollTo(whereTo, smoothScroll.scrollTiming, { offset: { top: smoothScroll.topScrollOffset }, easing: 'easeInOutQuart' });
        },*/
        generateTempNavId: function(navId){
            return '_'+navId;
        },
        getNavIdFromHash: function(){
            var hash = window.location.hash;
 
            if (smoothScroll.hashIsTempNavId()) {
                hash = hash.substring(2);
            }
 
            return hash;
        },
        hashIsTempNavId: function(){
            var hash = window.location.hash;
 
            return hash.substring(0,2) === '#_';
        },
 
        loaded: function(){
            if (smoothScroll.hashIsTempNavId()) {
                setTimeout(function(){
                    smoothScroll.scrollToElement('#'+smoothScroll.getNavIdFromHash());
                },
                smoothScroll.pageLoadScrollDelay);
            }
            $(window).smoothWheel();
        }
    };
    smoothScroll.target.bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(){
        smoothScroll.target.stop();
    });
})(jQuery);