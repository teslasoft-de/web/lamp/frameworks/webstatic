/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.10.2014
 * File: eventFreezer.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

 $.event.freezeEvents = function(elem) {

    if (typeof($._funcFreeze)=="undefined") {
        $._funcFreeze = [];
    }

    if (typeof($._funcNull)=="undefined") {
        $._funcNull = function() {};
    }

    // don't do events on text and comment nodes
    if ( elem.nodeType == 3 || elem.nodeType == 8 ) {
        return;
    }

    var events = $._data(elem, "events");

    if (events) {
        $.each(events, function(type, definition) {
            $.each(definition, function(index, event) {
                if (event.handler != $._funcNull){
                    $._funcFreeze["events_freeze_" + event.guid] = event.handler;
                    event.handler = $._funcNull;
                }
            });
        });
    }
};

$.event.unFreezeEvents = function(elem) {

    // don't do events on text and comment nodes
    if ( elem.nodeType == 3 || elem.nodeType == 8 )
        return;

    var events = $._data(elem, "events");

    if (events) {
        $.each(events, function(type, definition) {
            $.each(definition, function(index, event) {
                if (event.handler == $._funcNull){
                    event.handler = $._funcFreeze["events_freeze_" + event.guid];
                }
            });
        });
    }
};

$.fn.freezeEvents = function() {
    return this.each(function(){
        $.event.freezeEvents(this);
    });
};

$.fn.unFreezeEvents = function() {
    return this.each(function(){
        $.event.unFreezeEvents(this);
    });
};