/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 15.10.2014
 * File: blur.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

 /**
 * Appends the css3 blur effect to an element.
 *
 * @param string selector
 * @param boolean enabled
 * @param delegate callback
 */
function blur( selector, enabled, callback, duration )
{
    $(selector).each(function (i, o) {
        var element = $(o);
        if (!element.prop('vague')) {
            if (!enabled)
                return;
            element.prop('vague', element.Vague({
                intensity: enabled === true ? 3 : enabled,
                easing: 'easeInExpo'
            }));
        }
        var vague = element.prop('vague');
        var onFinish = function (destroy) {
            if (destroy && vague.destroy) {
                vague.destroy();
                element.prop('vague', null);
                element.enableSelection();
            }
            if (callback)
                callback();
        };
        if (enabled) {
            element.disableSelection();
            var value = enabled === true ? 3 : enabled;
            vague.animate(value, {duration: duration ? duration : 150}).done(function () {
                onFinish(!value);
            });
        }
        else {
            vague.animate(0, {duration: duration ? duration : 150}).done(function () {
                onFinish(true);
            });
        }
    });
}