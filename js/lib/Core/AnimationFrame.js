/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : AnimationFrame.ts
 Created    : 2015-04-15 12:44:03 +0200
 Updated    : 2015-04-15 12:44:03 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */
/// <reference path="../../typings/tsd.d.ts"/>
var Core;
(function (Core) {
    var AnimationFrame = (function () {
        function AnimationFrame(callback) {
            var _this = this;
            this.callback = callback;
            this.ticking = false;
            this.RequestTick = function () {
                if (!_this.ticking) {
                    AnimationFrame.proc.call(window, function () {
                        _this.callback();
                        _this.ticking = false;
                    });
                }
                _this.ticking = true;
            };
            this.running = false;
            this.RequestLoop = function (running) {
                if (typeof running != 'undefined')
                    _this.running = running;
                if (_this.running === false)
                    return;
                AnimationFrame.proc.call(window, function () {
                    _this.callback();
                    _this.RequestLoop();
                });
            };
        }
        AnimationFrame.proc = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
                return 0;
            };
        return AnimationFrame;
    })();
    Core.AnimationFrame = AnimationFrame;
})(Core || (Core = {}));
//# sourceMappingURL=AnimationFrame.js.map