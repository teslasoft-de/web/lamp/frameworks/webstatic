/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : Polyfill.ts
    Created    : 2016-09-14 21:19:22 +0200
    Updated    : 2016-09-14 21:19:22 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/

interface Window{
    polyfill: Core.Polyfill;
}

module Core{

    declare var conditionizr: {
        on: (testName:string, callback: Function) => void;
    };

    export interface TestParams{
        media: Array<string>;
        path: string;
    }
    export interface Tests{
        name: string;
        params: TestParams
    }
    export class Polyfill{

        constructor(private cookieDomain: string, private assetsPath: string = '/css/'){}

        configItem: Function = (test,mediaTypes):Tests => {
            return {
                name: test,
                params: {
                    media: mediaTypes,
                    path: (this.assetsPath + (mediaTypes[0] == test ? test + '.css' : mediaTypes[0] + '.' + test + '.css'))
                }
            };
        };

        /**
         * Load non-critical CSS async on first visit:
         * On first visit to the site, the critical CSS for each template should be inlined in the head, while the full CSS for the site should be requested async and cached for later use.
         * A meta tag with a name matching the fullCSSKey should have a content attribute referencing the path to the full CSS file for the site.
         * If no cookie is set to specify that the full CSS has already been fetched, load it asynchronously and set the cookie.
         * Once the cookie is set, the full CSS is assumed to be in cache, and the server-side templates should reference the full CSS directly from the head of the page with a link element, in place of inline critical styles.
         * @param {Array<Tests>} tests
         */
        enhance: Function = (tests: Array<Tests>) => {
            for( var key in tests) {
                var test = tests[key];
                conditionizr.on(test.name, () => {
                    if( !this.cookie( test.name ) ){
                        var params = test.params;
                        this.loadCSS( params.path, params.media );
                        // set cookie to mark this file fetched
                        this.cookie( test.name, params.media.join()+'|'+params.path, 7 );
                    }
                });
            }
        };

        /**
         * Loads a css file asynchronously.
         *
         * @param {string} href Is the URL for your CSS file.
         * @param {Array<string>} media The CSS media type(s). Default 'all'
         * @param {HTMLElement} before Optionally defines the element we'll use as a reference for injecting our <link>
         * By default, `before` uses the first <script> element in the page.
         * However, since the order in which stylesheets are referenced matters, you might need a more specific location in your document.
         * If so, pass a different reference element to the `before` argument and it'll insert before that instead
         * @returns {HTMLElement}
         * @private
         */
        loadCSS( href: string, media:Array<string>, before?:HTMLElement ): HTMLElement {

            var doc = window.document,
                ss = doc.createElement("link"),
                ref = before || doc.getElementsByTagName("script")[0],
                sheets = doc.styleSheets;

            ss.rel = "stylesheet";
            ss.href = href;
            // temporarily, set media to something non-matching to ensure it'll fetch without blocking render
            ss.media = "only x";

            if(media.length == 0)
                media[0] = 'all';

            // This function sets the link's media back to `all` so that the stylesheet applies once it loads
            // It is designed to poll until document.styleSheets includes the new sheet.
            var toggleMedia = function () {
                var defined,
                    old;
                for (var i = 0; i < sheets.length; i++) {
                    var sheet = sheets[i];
                    if (sheet.href && sheet.href.indexOf(href) > -1)
                        defined = true;
                    else if (sheet.href.indexOf(media[0] + '.css') > -1)
                        old = sheet.ownerNode;
                }
                if (!defined)
                    setTimeout(toggleMedia);
                else {
                    ss.media = media.join();
                    if (old)
                        ref.parentNode.removeChild(old);
                }
            };
            toggleMedia();

            // inject link
            ref.parentNode.insertBefore(ss, ref);

            return ss;
        }


        cookie( name:string, value?:any, days?:number ): string{
            name = this.cookieDomain+'.'+name;
            // if value is undefined, get the cookie value
            if( value === undefined ){
                var cookiestring = "; " + window.document.cookie;
                var cookies = cookiestring.split( "; " + name + "=" );
                if ( cookies.length == 2 )
                    return cookies.pop().split( ";" ).shift();
                return null;
            }
            else {
                // if value is a false boolean, we'll treat that as a delete
                if( value === false )
                    days = -1;

                if ( days ) {
                    var date = new Date();
                    date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
                    var expires = "; expires="+date.toUTCString();
                }
                else
                    var expires = "";

                window.document.cookie = name + "=" + value + expires + "; path=/";
            }
        }
    }
}