/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : AnimationFrame.ts
    Created    : 2016-09-17 05:10:07 +0200
    Updated    : 2016-09-14 21:19:22 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/

/// <reference path="../../typings/tsd.d.ts"/>

module Core {
    export class AnimationFrame {
        constructor(private callback: Function) { }

        static proc: (callback: FrameRequestCallback) => number =
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
                return 0;
            };

        ticking: boolean = false;
        RequestTick: Function = () => {
            if (!this.ticking) {
                AnimationFrame.proc.call(window, () => {
                    this.callback();
                    this.ticking = false;
                });
            }
            this.ticking = true;
        };

        running: boolean = false;
        RequestLoop: Function = (running?: boolean) => {

            if (typeof running != 'undefined')
                this.running = running;
            if (this.running === false)
                return;
            AnimationFrame.proc.call(window, () => {
                this.callback();
                this.RequestLoop();
            });
        }
    }
}