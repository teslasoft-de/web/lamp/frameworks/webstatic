/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 23.10.2014
 * File: smoothwheel.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

 (function ($) {
    var self = this,
        container,
        running=false,
        currentY = 0,
        targetY = 0,
        oldY = 0,
        maxScrollTop= 0,
        minScrollTop,
        direction,
        onRenderCallback=null,
        fricton = 0.95, // higher value for slower deceleration
        vy = 0,
        stepAmt = 1,
        minMovement= 0.1;

    var updateScrollTarget = function (amt) {
        targetY += amt;
        vy += (targetY - oldY) * stepAmt;
        oldY = targetY;
    };

    var render = new Core.AnimationFrame(function () {
        if (vy < -(minMovement) || vy > minMovement) {
            currentY = (currentY + vy);
            if (currentY > maxScrollTop) {
                currentY = vy = 0;
            } else if (currentY < minScrollTop) {
                vy = 0;
                currentY = minScrollTop;
            }
            container.scrollTop(-currentY);
            vy *= fricton;
            if(onRenderCallback){
                onRenderCallback();
            }
        }
    });

    var onWheel = function (e) {
        e.preventDefault();
        var evt = e.originalEvent;
        var delta = evt.detail ? evt.detail * -1 : evt.wheelDelta / 40;
        var dir = delta < 0 ? -1 : 1;
        if (dir != direction) {
            vy = 0;
            direction = dir;
        }
        //reset currentY in case non-wheel scroll has occurred (scrollbar drag, etc.)
        currentY = -container.scrollTop();
        updateScrollTarget(delta);
    };
    var arrowKeyScroll = function (event) {
        var ArrowKeys = {
            'up': '38',
            'down': '40'
        };
        if (event.keyCode == ArrowKeys.up || event.keyCode == ArrowKeys.down ) {
            if(document.activeElement){
                switch(document.activeElement.tagName){
                    case 'INPUT':
                    case 'TEXTAREA':
                    case 'SELECT':
                        return;
                    default:break;
                }
            }
            event.preventDefault();
            if (event.keyCode == ArrowKeys.up) {
                event.originalEvent.wheelDelta = 120;
            } else {
                event.originalEvent.wheelDelta = -120;
            }
            onWheel(event);
        }
    };
    /*
     * http://jsbin.com/iqafek/2/edit
     */
    var normalizeWheelDelta = function () {
        // Keep a distribution of observed values, and scale by the
        // 33rd percentile.
        var distribution = [],
            done = null,
            scale = 30;
        return function (n) {
            // Zeroes don't count.
            if (n === 0) return n;
            // After 500 samples, we stop sampling and keep current factor.
            if (done !== null) return n * done;
            var abs = Math.abs(n);
            // Insert value (sorted in ascending order).
            outer: do { // Just used for break goto
                for (var i = 0; i < distribution.length; ++i) {
                    if (abs <= distribution[i]) {
                        distribution.splice(i, 0, abs);
                        break outer;
                    }
                }
                distribution.push(abs);
            } while (false);
            // Factor is scale divided by 33rd percentile.
            var factor = scale / distribution[Math.floor(distribution.length / 3)];
            if (distribution.length == 500) done = factor;
            return n * factor;
        };
    }();
    /**
     * @param {Array} options Possible options are: onRender(func), remove(boolean)
     * @returns {*}
     */
    $.fn.smoothWheel = function () {
        var options = jQuery.extend({}, arguments[0]);
        return this.each(function (index, elm) {
            if(!('ontouchstart' in window)){
                container = $(this);
                container.bind("DOMMouseScroll mousewheel", onWheel);
                container.bind("keydown", arrowKeyScroll);
                //set target/old/current Y to match current scroll position to prevent jump to top of container
                targetY = oldY = container.scrollTop();
                currentY = -targetY;
                minScrollTop = container.get(0).clientHeight - container.get(0).scrollHeight;
                container.on('resize', function(){
                    minScrollTop = container.get(0).clientHeight - container.get(0).scrollHeight;
                });
                if(options.onRender)
                    onRenderCallback = options.onRender;
                if(options.remove){
                    render.RequestLoop(false);
                    container.unbind("DOMMouseScroll mousewheel", onWheel);
                    container.unbind("keydown", arrowKeyScroll);
                    vy = 0;
                }else if(!render.running)
                    render.RequestLoop(true);
            }
        });
    };
})(jQuery);