/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 21.10.2014
 * File: plugins.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

(function($) {

    $.fn.getSelector = function () {
        var selector = $(this).parents()
            .map(function () {
                return this.tagName;
            })
            .get().reverse().join(" > ");

        if (selector) {
            selector += " " + $(this)[0].nodeName;
        }

        var id = $(this).attr("id");
        if (id) {
            selector += "#" + id;
        }

        var classNames = $(this).attr("class");
        if (classNames) {
            selector += "." + $.trim(classNames).replace(/\s/gi, ".");
        }

        return selector;
    };

    $.fn.outerHTML = function () {
        return jQuery('<div />').append(this.eq(0).clone()).html();
    };

    $.fn.disableSelection = function () {
        this.hideSelection();
        return this
            .attr('unselectable', 'on')
            .css({
                'user-select': 'none',
                '-moz-user-select': 'none',
                '-o-user-select': 'none',
                '-khtml-user-select': 'none',
                '-webkit-user-select': 'none',
                '-ms-user-select': 'none'
            })
            .on('selectstart', false)
            .on('contextmenu', false)
            .on('keydown', false);
        //.on('mousedown', false);
    };

    $.fn.enableSelection = function () {
        return this
            .attr('unselectable', '')
            .css({
                'user-select': '',
                '-moz-user-select': '',
                '-o-user-select': '',
                '-khtml-user-select': '',
                '-webkit-user-select': '',
                '-ms-user-select': ''
            })
            .off('selectstart', false)
            .off('contextmenu', false)
            .off('keydown', false);
        //.off('mousedown', false);
    };

    $.fn.hideSelection = function () {
        if (window.getSelection) {
            if (window.getSelection().empty) {  // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {  // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {  // IE?
            document.selection.empty();
        }
    };

    var pxRegex = /px/, percentRegex = /%/, urlRegex = /url\(['"]*(.*?)['"]*\)/g;
    $.fn.getBackgroundSize = function (callback) {
        var img = new Image(), width, height, backgroundSize = this.css('background-size').split(' ');

        var getValue = function (value, parentValue) {
            if (pxRegex.test(value))
                return parseInt(value);
            if (percentRegex.test(value))
                return parentValue * (parseInt(value) / 100);
        };

        width = getValue(backgroundSize[0], this.parent().width());
        height = getValue(backgroundSize[1], this.parent().height());
        // Return cahced values, if width and height was set just call the callback and return
        if ((typeof width != 'undefined') && (typeof height != 'undefined')) {
            callback({width: width, height: height});
            return this;
        }

        var getAspectValue = function(value, otherValue, otherValueDafault){
            var aspectRatio = otherValue == otherValueDafault ? 100 : otherValue / otherValueDafault * 100;

            // Calc aspect by percentage and round to nearest .5 decimal
            return Number((Math.round(value * aspectRatio/ 100 * 2) / 2).toFixed(1));
        };

        img.onload = function () {
            if (typeof width == 'undefined')
                width = getAspectValue(this.width, height, this.height);
            if (typeof height == 'undefined')
                height = getAspectValue(this.height, width, this.width);
            callback({width: width, height: height});
        };
        img.src = this.css('background-image').replace(urlRegex, '$1');
        return this;
    };
})(jQuery);