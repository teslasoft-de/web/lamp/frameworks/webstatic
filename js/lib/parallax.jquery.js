/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 15.10.2014
 * File: parallax.jquery.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

(function($) {
 
    $.fn.parallax = function(options) {
 
        var windowHeight = $(window).height();
 
        // Set default settings
        var settings = $.extend({
            type            : 'header',
            speed           : 0.15,
            blurTarget      : '.container',
            blurTargets     : null,
            valueCallback   : null,
            parallaxCallback: null
        }, options);
 
        // Iterate over each object in collection
        return this.each( function() {
            // Save a reference to the element
            var $this = $(this);
            
            if($this.data('parallax')){
                $this.data('parallax')();
                return;
            }
            var $blurTarget = $this.getSelector(),
                $blurTargetTop;
            if(settings.blurTarget){
                $blurTarget = $blurTarget + ' ' + settings.blurTarget;
                $blurTargetTop = $($blurTarget).offset().top;
            }
            
            parallax = function(){

                var scrollTop = $(window).scrollTop();
                var offset = $this.offset().top;
                var height = $this.outerHeight();

                // Check if above or below viewport
                if (offset + height <= scrollTop || offset >= scrollTop + windowHeight)
                    return;
                var pos = scrollTop-height;
                
                var yBgPosition;
                switch (settings.type){
                    case 'header':
                        yBgPosition = Math.round(height/2+pos-pos/2);
                        break;
                    case 'content':
                        yBgPosition = Math.round((offset - scrollTop) * settings.speed);
                        break;
                }
                var yPos = yBgPosition  + 'px',
                    newPos;
                if(settings.valueCallback)
                    newPos = settings.valueCallback(yPos);
                
                // Apply the Y Background Position to Set the Parallax Effect
                $this.css('background-position', newPos ? newPos[0] + ' ' + newPos[1] : 'center ' + yPos);
                /*$this.stop().animate({
                    backgroundPositionX: (newPos ? newPos[1] : '50%'),
                    backgroundPositionY: yPos
                }, {duration: 200});*/
                
                if(settings.parallaxCallback)
                    settings.parallaxCallback($this, yBgPosition, height, scrollTop);

                var applyBlurEffect = function(progress, target){
                    var blurTarget = $(target),
                        blurState = null,
                        opacityState = null;
                    if(progress == -1 || progress > 100) {
                        //blurTarget.hide();
                        blurState = 0;
                        opacityState = 0;
                    }
                    // Get actual containing blur targets.
                    else if(progress <= 100){
                        blurTarget.show();
                        // Calc progress for Vague blur range of 0-30.
                        // Round values by two decimal places (.00)
                        blurState = Math.round( 30 / 100 * progress * 100) / 100;
                        opacityState = Math.round(((100 - progress) / 100) * 100) / 100;
                    }

                    blur(blurTarget, blurState);
                    // Apply opacity (rounding by two decimal places)
                    blurTarget.css({
                        opacity: opacityState/*,
                        marginTop: target.height() * (progress*-1/100)*/});

                    //console.log(target+': '+progress+' '+blurState+' '+opacityState);
                };

                var progress = 100 - 100*(pos*-1/height);
                /* Calculates accelerated expo easing */
                var accelerate = function(progress){
                    return progress * Math.pow(progress,progress/128);
                };

                // Blur targets out
                if(settings.blurTarget){
                    applyBlurEffect(accelerate(progress),$blurTarget);
                }

                if(settings.blurTargets && settings.blurTargets instanceof Array) {
                    var step = 100 / settings.blurTargets.length;
                    var storyBoards = [];
                    $.each(settings.blurTargets, function(i, o) {

                        // Prepare progress parameters.
                        var range = step * (i+1),
                            lastProcessed = i > 0 && storyBoards[i-1].progress >= 100,
                            currentProgress = progress;

                        // Switch to prevous storyboard if previous item is completed.
                        if(lastProcessed)
                            range -= step;

                        // Suspend progress until the accelerated progress range
                        // is greater than the real progress range.
                        else if(i > 0){
                            var acc = Math.pow(100,100/128),
                                // Minimum progress range for item.
                                minProgress = range - step;
                                // Max progress range before real progress starts.
                                maxProgress = 100 - acc;

                            // Adjust progress if in range of item.
                            if(currentProgress >= minProgress && currentProgress <= range) {
                                if (currentProgress < maxProgress)
                                    currentProgress = minProgress;
                                else
                                    currentProgress = minProgress + (currentProgress - maxProgress);
                            }
                        }
                        var innerProgress = range - currentProgress;

                        // Calc progress if item is in step range, otherwise hide;
                        innerProgress = ((range >= currentProgress) && (innerProgress <= step)) ?
                            // Calc percentage of inner progress.
                            100 - innerProgress / step * 100:
                            // Element is not in progress.
                            -1;

                        // Invert progress if previous item is completed. (fade in)
                        if(lastProcessed)
                            innerProgress = 100 - innerProgress;

                        storyBoards[i] = {
                            progress: innerProgress > -1 ? accelerate(innerProgress) : innerProgress,
                            selector: $this.getSelector() + ' ' + o
                        };
                    });
                    $.each(storyBoards, function(i,o){
                        var progress = o.progress <= 100 ? o.progress : 100;
                        applyBlurEffect(progress, o.selector);
                    });
                }
            };

            var animFrame = new Core.AnimationFrame(parallax);
            // Set up scroll handler
            $(document).scroll(animFrame.RequestTick);
            $(document).on('touchmove throttledresize', animFrame.RequestTick);
            $this.data('parallax', animFrame.RequestTick);
            animFrame.RequestTick();
        });
    };
}(jQuery));