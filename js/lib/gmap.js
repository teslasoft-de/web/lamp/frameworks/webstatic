/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 14.10.2014
 * File: gmap.js
 * Encoding: UTF-8
 * Project: WebStatic
 * */

(function($) {
 
    $.fn.gmap = function(options) {
        
        var that = $(this);
        var style = that.data('gmap-style');
        
        // Set default settings
        var settings = $.extend({
            zoom: parseInt(that.attr("data-zoom"),10) || 6,
            center: new google.maps.LatLng(that.attr("data-latitude"), that.attr("data-longitude")),
            html: that.attr("data-title"),
            mapTypeControl: style ? false : true,
            popup: true,
            scaleControl: true,
            draggable: true,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }, options);
        
        var infoContent = that.html();
        var map = new google.maps.Map(that[0], settings);
        
        if (style) {
            var styledMap = new google.maps.StyledMapType(style, {name: "Styled Map"});
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }

        var mLat = that.attr("data-marker-latitude"),
            mLng = that.attr("data-marker-longitude");
    
        if(!mLat || !mLng)
            return;
        
        var infowindow = new google.maps.InfoWindow({
            content: infoContent
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(mLat,mLng),
            map: map,
            title: that.attr("data-title")
        });
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
    };
}(jQuery));