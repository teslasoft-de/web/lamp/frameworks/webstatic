/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

import grunt = require("grunt");
import IProjectConfig = grunt.config.IProjectConfig;
import FilesConfig = grunt.file.IFilesConfig;
import FileModule = grunt.file.FileModule;
import ITaskOptions = grunt.task.ITaskOptions;

module Grunt
{
    import IProjectConfig = grunt.config.IProjectConfig;
    export interface Function<T extends string|number>
    {
        (s?: T|Array<T>): T
    }
    export interface StringFunction extends Function<string> {}
    export interface NumberFunction extends Function<number> {}

    export class GruntConfig implements IProjectConfig
    {
        /**
         * @see GruntConfig.Dir
         */
        public _sourceDir: StringFunction;

        /**
         * @see GruntConfig.Dir
         */
        public _outDir: StringFunction;

        get outFile(): string { return this._outFile; }

        protected _pkg: Object;
        get pkg() { return this._pkg; }

        protected _bwr: Object;
        get bwr() { return this._bwr; }

        /**
         * Task Store
         * @type {string[]}
         * @private
         */
        private __: string[] = ['time-grunt'];
        get _(): string[] { return this.__; }

        set _(value: string[]) { this.__ = this.__.concat(value); }

        /**
         * @param _grunt
         * @param sourceDir
         * @param outDir
         * @param _outFile
         * @param tasks
         * @constructor
         */
        constructor(
            protected _grunt: grunt,
            sourceDir: string,
            outDir: string,
            protected _outFile: string = 'main',
            ...tasks: string[])
        {
            this._sourceDir = this.Dir('sourceDir', sourceDir);
            this._outDir = this.Dir('outDir', outDir);

            grunt.loadNpmTasks('grunt-extend-config');

            this.__ = this.__.concat(tasks);

            this._pkg = grunt.file.readJSON('package.json');
            this._bwr = grunt.file.readJSON('bower.json');
        }

        /**
         * Returns a function which prepends the specified dir to the file path specified at the returned function
         * and crates a string get property for the specified dir.
         * @param name
         * @param dir
         * @returns {(path:any)=>any} The full path of the specified file.
         * Path null -> it appends the *.js pattern to the directory.
         * Path '' => it returns the directory only.
         */
        public Dir(name: string, dir: string): StringFunction
        {
            let map = (_path: any) => { return dir + _path },
                property = (path: any) => {
                    return (path instanceof Array) ? path.map(map) : map((path || path === '') ? path : '*.js');
                };

            // Define property on this GruntConfig instance to enable usage
            // of the raw directory name within grunt template processing.
            Object.defineProperty(this, name, {
                get         : function () {
                    return dir;
                },
                enumerable  : true,
                configurable: true
            });
            return property;
        }

        public init()
        {
            require('load-grunt-tasks')(grunt, {
                config           : './package.json',
                scope            : 'devDependencies',
                requireResolution: true,
                pattern          : ['grunt-contrib-*'].concat(this._)
            });

            grunt.initConfig(this);
        }

        /**
         * Extends the grunt configuration. Important: User only after GruntConfig.init()!
         * @param newConfig
         */
        public extend<T extends IProjectConfig>(newConfig: T): void
        {
            this._grunt.extendConfig(newConfig);
        }

        banner: IProjectConfig;

        /**
         * Add file banners.
         * @private
         */
        public _banner(
            //js: string|string[] = [this._sourceDir()],
            ts: string|string[] = [this._sourceDir('*.ts')]): void
        {
            this.banner = {
                //'php': ['<%= concat.dist.dest %>,<%= concat.dist.src %>']
                //'js': ['<%= concat.dist.src %>']
                //'js': ((js instanceof Array) ? js : [js]),
                'ts': ((ts instanceof Array) ? ts : [ts]),
                //'jade': ['./../*.jade'] // TODO: Implement JADE file header templates (comment tags)
                //'php': ['./../Common/DatabaseObject.php'] // TODO: Implement PHP file header templates (inside php tags)
            };

            require('./banner.js').banner(this);
        }

        bowercopy: IProjectConfig;

        /**
         * Copy bower components to distribution directory
         * @param options
         * @param targets
         * @private
         */
        public _bowercopy(options: Object, targets: IProjectConfig): void
        {
            this._ = ['grunt-bowercopy'];

            this.bowercopy = {options: options};
            for (let _target in targets)
                this.bowercopy[_target] = {files: targets[_target]};
        }

        replace: IProjectConfig;

        /**
         * Replace strings
         * @param taskName
         * @param src
         * @param dest
         * @param patterns
         * @private
         */
        public _replace(
            taskName: string, src: string|string[], dest: string,
            patterns: {[replace: string]: RegExp}): void
        {
            if (!this.replace) {
                this._ = ['grunt-replace'];
                this.replace = {};
            }

            this.replace[taskName] = {
                options: {
                    preserveOrder: true,
                    patterns     : (()=> {
                        let config: IProjectConfig[] = [];
                        for (let _replace in patterns)
                            config.push({
                                match      : patterns[_replace],
                                replacement: _replace
                            })
                        return config;
                    })()
                },
                files  : [
                    {
                        expand : true,
                        flatten: true,
                        src    : ((src instanceof Array) ? src : [src]),
                        dest   : dest
                    }
                ]
            };
        }

        wrap: IProjectConfig;

        /**
         * Wrap whole file contents
         * @param taskName
         * @param src
         * @param dest
         * @param prepend
         * @param append
         * @private
         */
        public _wrap(taskName: string, src: string|string[], dest: string, prepend: string, append: string): void
        {
            if (!this.wrap) {
                this._ = ['grunt-wrap'];
                this.wrap = {};
            }

            this.wrap[taskName] = {
                src    : ((src instanceof Array) ? src : [src]),
                dest   : dest,
                options: {
                    wrapper: [prepend, append]
                }
            };
        }

        /**
         * Convert commonJS files into amd modules.
         * @param taskName
         * @param src
         * @param dest
         * @param exports
         * @param imports
         * @private
         */
        public _wrap_amd(
            taskName: string,
            src: string|string[],
            dest: string,
            exports: string,
            imports: string = ''): void
        {
            this._wrap(taskName, src, dest,
                'define(function (require, exports, module) {' + imports + '\n',
                '\nreturn ' + exports + ';\n});'
            );
        }

        minify: IProjectConfig;

        /**
         * Compress modules and concatenated code
         * @private
         */
        public _minify(minify: IProjectConfig)
        {
            this.minify = minify;
            require('./minify.js').minify(this, {
                dest: this._outDir(''),
                map : true
            });
        }

        jshint: IProjectConfig;

        /**
         * Check for JavaScript errors.
         */
        public _jshint(files: string|string[] = [this._sourceDir()]): void
        {
            this.jshint = {
                files  : ((files instanceof Array) ? files : [files]),
                options: {
                    globals: {
                        jQuery : true,
                        console: true,
                        module : true
                    },
                    asi    : true // Ignore semicolon warnings
                }
            }
        }

        jscs: IProjectConfig;

        /**
         * Style checker definitions (optional)
         * @private
         */
        public _jscs(files: string|string[] = [this._sourceDir()]): void
        {
            this.jscs = {
                files  : ((files instanceof Array) ? files : [files]),
                options: {
                    preset: "jquery"
                }
            }
        }

        concat: IProjectConfig;

        /**
         * Concat all modules.
         * @private
         */
        public _concat(src: string, dest: string = this._outDir(this.outFile + '.js')): void
        {
            this.concat = {
                options: {
                    separator   : '\r\n\r\n',
                    stripBanners: {block: true},
                    sourceMap   : true
                },
                dist   : {
                    src   : src,
                    dest  : dest,
                    nonull: true
                }
            }
        }

        watch: IProjectConfig;

        /**
         * Observe source files for changes and regenerate
         * @private
         */
        public _watch(
            files: string|string[] = ['<%= jshint.files %>'],
            tasks: string|string[] = ['jshint', 'concat', 'minify']): void
        {
            this.watch = {
                files: ((files instanceof Array) ? files : [files]),
                tasks: ((tasks instanceof Array) ? tasks : [tasks])
            }
        }

        concurrent: IProjectConfig;

        /**
         *
         * @private
         */
        public _concurrent(
            checkTasks: string|string[] = ['jshint'],
            buildTasks: string|string[] = ['concat', 'minify', 'watch']): void
        {
            this._ = ['grunt-concurrent'];

            this.concurrent = {
                options: {
                    logConcurrentOutput: true
                },
                check  : {
                    tasks: ((checkTasks instanceof Array) ? checkTasks : [checkTasks])
                },
                build  : {
                    tasks: ((buildTasks instanceof Array) ? buildTasks : [buildTasks])
                }
            }
        }

        typescript_export: IProjectConfig;

        /**
         * Generates a single index.d.ts file for your NPM package implemented in TypeScript
         * by concatenating per-file d.ts files, wrapping them all into an implicit module
         * declaration and rewriting/moving some lines.
         * @param taskName
         * @param src
         * @param dest
         * @private
         */
        public _typescript_export(
            taskName: string = this.outFile,
            src: string|string[] = [this._sourceDir('*.d.ts')],
            dest = this._outDir(this.outFile + '.d.ts'))
        {
            if (!this.typescript_export) {
                this._ = ['grunt-typescript-export'];
                this.typescript_export = {};
            }

            this.typescript_export[taskName] = {
                src : ((src instanceof Array) ? src : [src]),
                dest: dest
            };
        }

        compass: IProjectConfig;

        /**
         *
         * @see https://github.com/gruntjs/grunt-contrib-compass
         * @param env
         * @param options
         * @private
         */
        public _compass(
            env: string,
            options: IProjectConfig = {})
        {
            const defaultOptions: IProjectConfig = {
                force      : true,
                config     : 'compass/config.rb',
                sassDir    : 'compass/sass/',
                httpPath   : '../',
                cssDir     : 'css',
                fontsDir   : 'fonts',
                imagesDir  : 'img',
                outputStyle: 'compact',
                environment: 'development'
            };

            if (!this.compass) this.compass = {};

            this.compass[env] = {
                options: (() => {
                    // extend options parameter with default values
                    for (let _target in defaultOptions) {
                        if (!options.hasOwnProperty(_target))
                            options[_target] = defaultOptions[_target];
                    }
                    if (options.hasOwnProperty('specify'))
                        options['specify'] = options['specify'].map((path: string) => {return options['sassDir'] + path});

                    options['environment'] = env;
                    return options;
                })()
            }
        }

        auto_install: IProjectConfig;

        public _auto_install(taskName: string, cwd: string, npm: string = '--development')
        {
            if (!this.auto_install) {
                this._ = ['grunt-auto-install'];
                this.auto_install = {};
                this.auto_install['local'] = {};
            }

            this.auto_install[taskName] = {
                options: {
                    cwd        : cwd,
                    stdout     : true,
                    stderr     : true,
                    failOnError: true,
                    npm        : npm
                }
            }
        }

        run_grunt: IProjectConfig;

        public _run_grunt(taskName: string, src: string|string[], tasks: string[])
        {
            if (!this.run_grunt) {
                this._ = ['grunt-run-grunt'];
                this.run_grunt = {};
                this.run_grunt['options'] = {
                    minimumFiles: 1
                };
            }

            this.run_grunt[taskName] = {
                src    : ((src instanceof Array) ? src : [src]),
                options: {
                    task   : tasks,
                    log    : false,
                    process: function (res: any) {
                        if (res.fail) {
                            res.output = 'new content';
                            grunt.log.writeln('Grunt compile failed!');
                        }
                    }
                },
            };
        }
    }
}
export = Grunt