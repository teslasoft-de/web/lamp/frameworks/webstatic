/// <reference path="../../typings/tsd.d.ts"/>
var Core;
(function (Core) {
    var AnimationFrame = (function () {
        function AnimationFrame(callback) {
            var _this = this;
            this.callback = callback;
            this.ticking = false;
            this.RequestTick = function () {
                if (!_this.ticking) {
                    AnimationFrame.proc.call(window, function () {
                        _this.callback();
                        _this.ticking = false;
                    });
                }
                _this.ticking = true;
            };
            this.running = false;
            this.RequestLoop = function (running) {
                if (typeof running != 'undefined')
                    _this.running = running;
                if (_this.running === false)
                    return;
                AnimationFrame.proc.call(window, function () {
                    _this.callback();
                    _this.RequestLoop();
                });
            };
        }
        AnimationFrame.proc = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
                return 0;
            };
        return AnimationFrame;
    })();
    Core.AnimationFrame = AnimationFrame;
})(Core || (Core = {}));

/**
 * Appends the css3 blur effect to an element.
 *
 * @param string selector
 * @param boolean enabled
 * @param delegate callback
 */
function blur( selector, enabled, callback, duration )
{
    $(selector).each(function (i, o) {
        var element = $(o);
        if (!element.prop('vague')) {
            if (!enabled)
                return;
            element.prop('vague', element.Vague({
                intensity: enabled === true ? 3 : enabled,
                easing: 'easeInExpo'
            }));
        }
        var vague = element.prop('vague');
        var onFinish = function (destroy) {
            if (destroy && vague.destroy) {
                vague.destroy();
                element.prop('vague', null);
                element.enableSelection();
            }
            if (callback)
                callback();
        };
        if (enabled) {
            element.disableSelection();
            var value = enabled === true ? 3 : enabled;
            vague.animate(value, {duration: duration ? duration : 150}).done(function () {
                onFinish(!value);
            });
        }
        else {
            vague.animate(0, {duration: duration ? duration : 150}).done(function () {
                onFinish(true);
            });
        }
    });
}

(function($) {

    $.fn.getSelector = function () {
        var selector = $(this).parents()
            .map(function () {
                return this.tagName;
            })
            .get().reverse().join(" > ");

        if (selector) {
            selector += " " + $(this)[0].nodeName;
        }

        var id = $(this).attr("id");
        if (id) {
            selector += "#" + id;
        }

        var classNames = $(this).attr("class");
        if (classNames) {
            selector += "." + $.trim(classNames).replace(/\s/gi, ".");
        }

        return selector;
    };

    $.fn.outerHTML = function () {
        return jQuery('<div />').append(this.eq(0).clone()).html();
    };

    $.fn.disableSelection = function () {
        this.hideSelection();
        return this
            .attr('unselectable', 'on')
            .css({
                'user-select': 'none',
                '-moz-user-select': 'none',
                '-o-user-select': 'none',
                '-khtml-user-select': 'none',
                '-webkit-user-select': 'none',
                '-ms-user-select': 'none'
            })
            .on('selectstart', false)
            .on('contextmenu', false)
            .on('keydown', false);
        //.on('mousedown', false);
    };

    $.fn.enableSelection = function () {
        return this
            .attr('unselectable', '')
            .css({
                'user-select': '',
                '-moz-user-select': '',
                '-o-user-select': '',
                '-khtml-user-select': '',
                '-webkit-user-select': '',
                '-ms-user-select': ''
            })
            .off('selectstart', false)
            .off('contextmenu', false)
            .off('keydown', false);
        //.off('mousedown', false);
    };

    $.fn.hideSelection = function () {
        if (window.getSelection) {
            if (window.getSelection().empty) {  // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {  // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {  // IE?
            document.selection.empty();
        }
    };

    var pxRegex = /px/, percentRegex = /%/, urlRegex = /url\(['"]*(.*?)['"]*\)/g;
    $.fn.getBackgroundSize = function (callback) {
        var img = new Image(), width, height, backgroundSize = this.css('background-size').split(' ');

        var getValue = function (value, parentValue) {
            if (pxRegex.test(value))
                return parseInt(value);
            if (percentRegex.test(value))
                return parentValue * (parseInt(value) / 100);
        };

        width = getValue(backgroundSize[0], this.parent().width());
        height = getValue(backgroundSize[1], this.parent().height());
        // Return cahced values, if width and height was set just call the callback and return
        if ((typeof width != 'undefined') && (typeof height != 'undefined')) {
            callback({width: width, height: height});
            return this;
        }

        var getAspectValue = function(value, otherValue, otherValueDafault){
            var aspectRatio = otherValue == otherValueDafault ? 100 : otherValue / otherValueDafault * 100;

            // Calc aspect by percentage and round to nearest .5 decimal
            return Number((Math.round(value * aspectRatio/ 100 * 2) / 2).toFixed(1));
        };

        img.onload = function () {
            if (typeof width == 'undefined')
                width = getAspectValue(this.width, height, this.height);
            if (typeof height == 'undefined')
                height = getAspectValue(this.height, width, this.width);
            callback({width: width, height: height});
        };
        img.src = this.css('background-image').replace(urlRegex, '$1');
        return this;
    };
})(jQuery);

$.event.freezeEvents = function(elem) {

    if (typeof($._funcFreeze)=="undefined") {
        $._funcFreeze = [];
    }

    if (typeof($._funcNull)=="undefined") {
        $._funcNull = function() {};
    }

    // don't do events on text and comment nodes
    if ( elem.nodeType == 3 || elem.nodeType == 8 ) {
        return;
    }

    var events = $._data(elem, "events");

    if (events) {
        $.each(events, function(type, definition) {
            $.each(definition, function(index, event) {
                if (event.handler != $._funcNull){
                    $._funcFreeze["events_freeze_" + event.guid] = event.handler;
                    event.handler = $._funcNull;
                }
            });
        });
    }
};

$.event.unFreezeEvents = function(elem) {

    // don't do events on text and comment nodes
    if ( elem.nodeType == 3 || elem.nodeType == 8 )
        return;

    var events = $._data(elem, "events");

    if (events) {
        $.each(events, function(type, definition) {
            $.each(definition, function(index, event) {
                if (event.handler == $._funcNull){
                    event.handler = $._funcFreeze["events_freeze_" + event.guid];
                }
            });
        });
    }
};

$.fn.freezeEvents = function() {
    return this.each(function(){
        $.event.freezeEvents(this);
    });
};

$.fn.unFreezeEvents = function() {
    return this.each(function(){
        $.event.unFreezeEvents(this);
    });
};

/**
 * $.disableScroll
 *
 * Disables scroll events from mousewheels, touchmoves and keypresses.
 * Use while jQuery is animating the scroll position for a guaranteed super-smooth ride!
 */

;(function($) {

	"use strict";

	// Privates
	var instance;

	var _handleKeydown = function(event) {

        if(typeof instance.opts.scrollCallback === 'function'){
            if(instance.opts.scrollCallback(event))
                return;
        }
		for (var i = 0; i < instance.opts.scrollEventKeys.length; i++) {
			if (event.keyCode === instance.opts.scrollEventKeys[i]) {
				event.preventDefault();
				return;
			}
		}
	};

	var _handleWheel = function(event) {
        if(typeof instance.opts.scrollCallback === 'function'){
            if(instance.opts.scrollCallback(event))
                return;
        }
		event.preventDefault();
	};

	// The object
	function UserScrollDisabler($container, options) {

		// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
		// left: 37, up: 38, right: 39, down: 40
		this.opts = $.extend({
			handleKeys : true,
			scrollEventKeys : [32, 33, 34, 35, 36, 37, 38, 39, 40],
            scrollCallback : null
		}, options);
		
		this.$container = $container;
		this.$document = $(document);

		this.disable();
	}
	UserScrollDisabler.prototype = {
		
		disable : function() {
			var t = this;
			t.$container.on("mousewheel.UserScrollDisabler DOMMouseScroll.UserScrollDisabler touchmove.UserScrollDisabler", _handleWheel);

			if(t.opts.handleKeys) {
				t.$document.on("keydown.UserScrollDisabler", function(event) {
					_handleKeydown.call(t, event);
				});
			}
		},
		
		undo : function() {
			var t = this;
			t.$container.off(".UserScrollDisabler");
			if(t.opts.handleKeys) {
				t.$document.off(".UserScrollDisabler");
			}
		}
	};

	// Plugin wrapper for object
	$.fn.disableScroll = function(method) {

		// If calling for the first time, instantiate the object and cache in this closure.
		// Plugin can therefore only be instantiated once per page.
		// Can pass options object in through the method parameter.
		if( ! instance && (typeof method === "object" || ! method)) {
			instance = new UserScrollDisabler(this, method); // this = jquery collection to act on = $(window), hopefully!
		}

		// Instance already created, and a method is being explicitly called, e.g. .disableScroll('undo');
		else if(instance && instance[method]) {
			instance[method].call(instance);
		}

		// No method called explicitly, so assume 'disable' is intended.
		// E.g. calling .disableScroll(); again after a prior instantiation and undo.
		else if(instance) {
			instance.disable.call(instance);
		}
	};
})(jQuery);

// Adds slide from left (YouTube style) effect for site menu.
$(document).ready(function() {
    // Disable bootstrap menu collapse animation.
    $('.navbar-collapse').on('hide.bs.collapse', function() {
        return false;
    });

    var slideMenu = '#slidemenu';
    var slideNav = '#slide-nav';
    var toggler = '.navbar-toggle';
    var pageWrapper = '#page-contents';
    var navigationWrapper = '.navbar-header';
    var scrollspyContainer = '#scrollspy-container';
    var heightCol = '#navbar-height-col';
    var menuWidth = '100%';
    var slideWidth = '80%';
    var menuNeg = '-100%';
    var slideNeg = '-80%';
    var slideElements = 'body, .navbar, '+navigationWrapper+', '+pageWrapper;
    
    //stick in the fixed 100% height behind the navbar but don't wrap it
    $(slideNav+'.navbar-inverse').after($("<div id=\""+heightCol.substr(1)+"\" class=\"inverse\"></div>"));
    $(slideNav+'.navbar-default').after($("<div id=\""+heightCol.substr(1)+"\"></div>"));
    /*$("<div id=\""+heightCol.substr(1)+"\" class=\"inverse\"></div>").appendTo(slideNav+'.navbar-inverse').after();
    $("<div id=\""+heightCol.substr(1)+"\"></div>").appendTo(slideNav+'.navbar-default');*/

    var $window = $(window);
    
    var toggleScroll = function(active){
        var state = active ? 'auto' : "hidden";
        $('body').css("overflow-x", state);
        $('body').css("overflow-y", state);

        if (active)
            $window.disableScroll('undo');
        else
            $window.disableScroll({ scrollCallback: function(e){
                // Prevent scrolling or swipig on entire window except the slidenav and dialogs.
                if($(e.target).closest(slideMenu).length > 0 || $(e.target).closest('.modal').length > 0)
                    return true;
            } });
    };
    function SlideToggle(eventSource, active)
    {
        //$('.navbar-collapse').css('min-height', Response.band(768) ? '' : jQuery(window).height());
        $('.navbar-collapse').css('max-height', jQuery(window).height());

        $(slideMenu).stop().animate({
            left: active ? menuNeg : '0px'
        });
        $(heightCol).stop().animate({
            left: active ? slideNeg : '0px'
        });
        $(pageWrapper+','+scrollspyContainer).stop().animate({
            left: active ? '0px' : slideWidth
        });
        $(navigationWrapper).stop().animate(
                {left: active ? '0px' : slideWidth},
        {complete: function() {
                // Toggle blur after slide in.
                if(!active)
                    blur(pageWrapper, !active);
                // Turn scrollbars on after collapsing.
                toggleScroll(active);
                $(slideNav).trigger('toggled',[!active]);
            }});

        // Toggle slide active state
        eventSource.toggleClass('slide-active', !active);
        $(slideMenu).toggleClass('slide-active', !active);
        $(slideElements).toggleClass('slide-active', !active);

        // Prevent touch movements on page content while slide active.
        // Turn scrollbars off prior collapsing.
        if (!active)
            toggleScroll(active);
    }

    // Handle click on menu slide button
    $(slideNav).on("click", toggler, function(e) {
        
        var element = $(this);
        var slideActive = element.hasClass('slide-active');
        $(slideNav).trigger('toggle',[slideActive]);
        
        setTimeout(function(){
            if ($(slideNav).hasClass('slide-active')){
                // Toggle blur before slide out.
                blur(pageWrapper, false, function() {
                    SlideToggle(element, slideActive);
                    if(!$('body > ul.scroll-to-top').hasClass('hide'))
                        $('body > ul.scroll-to-top').fadeIn();
                });
            }
            else{
                $('body > ul.scroll-to-top').fadeOut();
                SlideToggle(element, slideActive);
            }
        },50);
    });

    $(window).on('throttledresize', function() {
        // Clip navbar to screen height.
        //$('.navbar-collapse').css('min-height', Response.band(768) ? '' : jQuery(window).height());
        $('.navbar-collapse').css('max-height', jQuery(window).height());
        
        // If not in responsive view and navbar is active but hidden enable page content.
        if (Response.band(768) &&
                $('.navbar-toggle').is(':hidden') &&
                $(slideNav).hasClass('slide-active')) {
            
            if (!$('.modal-backdrop').length)
                // If no dialog active unblur and enable page content scrollbars.
                blur(pageWrapper, false, function() {
                    toggleScroll(true);
                });
            else
                // Don't unblur if dialog active.
                // Enable page content scrollbars after dialog is closed.
                $('.modal').one('hidden.bs.modal', function() {
                    toggleScroll(true);
                });
        }
        // If navbar is visible blur page content.
        else if ($(slideNav).hasClass('slide-active'))
            blur(pageWrapper, true, function() {
                toggleScroll(false);
            });
    });

    $(slideNav+", "+heightCol).on('swipeleft', function() {
        if($(slideNav).hasClass('slide-active'))
            $(".navbar-toggle").click();
    });

    // Get swipe event start coordinates
    touchPos = 21;
    $('body').on('touchstart', function(e) {
        touchPos = e.originalEvent.touches[0].pageX;
    });
    $('body').on('mousedown', function(e) {
        touchPos = e.originalEvent.clientX;
    });

    // Close slide-nav on content click.
    $(pageWrapper).on('mousedown', function(e) {
        if (!Response.band(768) && $(slideNav).hasClass('slide-active'))
            $(".navbar-toggle").click();
    });

    // Open slide-nav on swipe events from left.
    $('body').on('swiperight', function() {
        if (touchPos <= 20 && !Response.band(768) && !$(slideNav).hasClass('slide-active')) {
            $(".navbar-toggle").click();
        }
    });
});

(function($) {
 
    $.fn.parallax = function(options) {
 
        var windowHeight = $(window).height();
 
        // Set default settings
        var settings = $.extend({
            type            : 'header',
            speed           : 0.15,
            blurTarget      : '.container',
            blurTargets     : null,
            valueCallback   : null,
            parallaxCallback: null
        }, options);
 
        // Iterate over each object in collection
        return this.each( function() {
            // Save a reference to the element
            var $this = $(this);
            
            if($this.data('parallax')){
                $this.data('parallax')();
                return;
            }
            var $blurTarget = $this.getSelector(),
                $blurTargetTop;
            if(settings.blurTarget){
                $blurTarget = $blurTarget + ' ' + settings.blurTarget;
                $blurTargetTop = $($blurTarget).offset().top;
            }
            
            parallax = function(){

                var scrollTop = $(window).scrollTop();
                var offset = $this.offset().top;
                var height = $this.outerHeight();

                // Check if above or below viewport
                if (offset + height <= scrollTop || offset >= scrollTop + windowHeight)
                    return;
                var pos = scrollTop-height;
                
                var yBgPosition;
                switch (settings.type){
                    case 'header':
                        yBgPosition = Math.round(height/2+pos-pos/2);
                        break;
                    case 'content':
                        yBgPosition = Math.round((offset - scrollTop) * settings.speed);
                        break;
                }
                var yPos = yBgPosition  + 'px',
                    newPos;
                if(settings.valueCallback)
                    newPos = settings.valueCallback(yPos);
                
                // Apply the Y Background Position to Set the Parallax Effect
                $this.css('background-position', newPos ? newPos[0] + ' ' + newPos[1] : 'center ' + yPos);
                /*$this.stop().animate({
                    backgroundPositionX: (newPos ? newPos[1] : '50%'),
                    backgroundPositionY: yPos
                }, {duration: 200});*/
                
                if(settings.parallaxCallback)
                    settings.parallaxCallback($this, yBgPosition, height, scrollTop);

                var applyBlurEffect = function(progress, target){
                    var blurTarget = $(target),
                        blurState = null,
                        opacityState = null;
                    if(progress == -1 || progress > 100) {
                        //blurTarget.hide();
                        blurState = 0;
                        opacityState = 0;
                    }
                    // Get actual containing blur targets.
                    else if(progress <= 100){
                        blurTarget.show();
                        // Calc progress for Vague blur range of 0-30.
                        // Round values by two decimal places (.00)
                        blurState = Math.round( 30 / 100 * progress * 100) / 100;
                        opacityState = Math.round(((100 - progress) / 100) * 100) / 100;
                    }

                    blur(blurTarget, blurState);
                    // Apply opacity (rounding by two decimal places)
                    blurTarget.css({
                        opacity: opacityState/*,
                        marginTop: target.height() * (progress*-1/100)*/});

                    //console.log(target+': '+progress+' '+blurState+' '+opacityState);
                };

                var progress = 100 - 100*(pos*-1/height);
                /* Calculates accelerated expo easing */
                var accelerate = function(progress){
                    return progress * Math.pow(progress,progress/128);
                };

                // Blur targets out
                if(settings.blurTarget){
                    applyBlurEffect(accelerate(progress),$blurTarget);
                }

                if(settings.blurTargets && settings.blurTargets instanceof Array) {
                    var step = 100 / settings.blurTargets.length;
                    var storyBoards = [];
                    $.each(settings.blurTargets, function(i, o) {

                        // Prepare progress parameters.
                        var range = step * (i+1),
                            lastProcessed = i > 0 && storyBoards[i-1].progress >= 100,
                            currentProgress = progress;

                        // Switch to prevous storyboard if previous item is completed.
                        if(lastProcessed)
                            range -= step;

                        // Suspend progress until the accelerated progress range
                        // is greater than the real progress range.
                        else if(i > 0){
                            var acc = Math.pow(100,100/128),
                                // Minimum progress range for item.
                                minProgress = range - step;
                                // Max progress range before real progress starts.
                                maxProgress = 100 - acc;

                            // Adjust progress if in range of item.
                            if(currentProgress >= minProgress && currentProgress <= range) {
                                if (currentProgress < maxProgress)
                                    currentProgress = minProgress;
                                else
                                    currentProgress = minProgress + (currentProgress - maxProgress);
                            }
                        }
                        var innerProgress = range - currentProgress;

                        // Calc progress if item is in step range, otherwise hide;
                        innerProgress = ((range >= currentProgress) && (innerProgress <= step)) ?
                            // Calc percentage of inner progress.
                            100 - innerProgress / step * 100:
                            // Element is not in progress.
                            -1;

                        // Invert progress if previous item is completed. (fade in)
                        if(lastProcessed)
                            innerProgress = 100 - innerProgress;

                        storyBoards[i] = {
                            progress: innerProgress > -1 ? accelerate(innerProgress) : innerProgress,
                            selector: $this.getSelector() + ' ' + o
                        };
                    });
                    $.each(storyBoards, function(i,o){
                        var progress = o.progress <= 100 ? o.progress : 100;
                        applyBlurEffect(progress, o.selector);
                    });
                }
            };

            var animFrame = new Core.AnimationFrame(parallax);
            // Set up scroll handler
            $(document).scroll(animFrame.RequestTick);
            $(document).on('touchmove throttledresize', animFrame.RequestTick);
            $this.data('parallax', animFrame.RequestTick);
            animFrame.RequestTick();
        });
    };
}(jQuery));

(function($){
 
    smoothScroll = {
        topScrollOffset: -84,
        scrollTiming: 1500,
        pageLoadScrollDelay: 1000,
        easeIn: 'easeInQuint',
        easeOut: 'easeOutExpo',
        delayCallback: null,
        target: $($.browser.webkit ? 'body': 'html'),
        hashLinkClicked: function(e){
 
            // current path
            var temp    = window.location.pathname.split('#');
            var curPath = temp[0]/*smoothScroll.addTrailingSlash(temp[0])*/;
 
            // target path
            var link       = $(this).attr('href');
            var linkArray  = link.split('#');
            var navId      = (typeof linkArray[1] !== 'undefined') ? linkArray[1] : null;
            var targetPath = linkArray[0] ? linkArray[0]/*smoothScroll.addTrailingSlash()*/ : null;
 
            // ScrollTo the hash id if the target is on the same page
            if ((!targetPath || targetPath == curPath) && navId) {
                e.preventDefault();
                var delay = $(this).attr('scrollspy-delay');
                if(delay)
                    setTimeout( function(){
                        smoothScroll.scrollToElement('#'+navId);
                    }, delay);
                else
                    smoothScroll.scrollToElement('#'+navId);
 
            // otherwise add '_' to hash so the browser will ignore it.
            } else if (navId) {
                e.preventDefault();
                navId = smoothScroll.generateTempNavId(navId);
                window.location = targetPath+'#'+navId;
            }
        },
        
        addTrailingSlash: function(str){
            lastChar = str.substring(str.length-1, str.length);
            if (lastChar != '/')
                str = str+'/';
            return str;
        },
        scrollToElement: function(whereTo, delayed){
            if($(whereTo).length === 0)
                return;
            if(smoothScroll.delayCallback && (typeof smoothScroll.delayCallback == 'function')){
                if(typeof delayed === 'undefined')
                    delayed = true;
                var delay = smoothScroll.delayCallback(whereTo);
                if(delayed && delay){
                    setTimeout( function(){
                        smoothScroll.scrollToElement(whereTo, false);
                    }, delay);
                    return;
                }
            }
            var offset = $(whereTo).offset().top + (typeof smoothScroll.topScrollOffset ==
                'function' ? smoothScroll.topScrollOffset()
                : smoothScroll.topScrollOffset);
            if(offset < 0)
                offset = 0;

            $(window).smoothWheel({remove: true});
            // Animate half scroll distance with ease-in and remaining scroll distance with ease-out function.
            smoothScroll.target.stop().animate({scrollTop: offset-(offset-$(window).scrollTop()) / 2},
                smoothScroll.scrollTiming / 2,
                smoothScroll.easeIn,
                function(){
                    smoothScroll.target.animate({scrollTop: offset},
                        smoothScroll.scrollTiming / 2, 
                        smoothScroll.easeOut,
                        function(){
                            $(whereTo).trigger('smoothScrolled');
                            // When done, add hash to url (default click behaviour).
                            if(whereTo.substring(0,1) === '#')
                            window.location.hash = smoothScroll.generateTempNavId(whereTo.substring(1, whereTo.length));
                            $(window).smoothWheel();
                        });
                });
                
        },
        scrollBody: function(){
            target.mousewheel(function(event, delta) {
                
                if (delta < 0)
                    target.stop().animate({scrollTop:'+=100'}, 1500 );
                else
                    target.stop().animate({scrollTop:'+=100'}, 1500 );
                //e.preventDefault();
                return false;
            });
        },
        // jQuery scrollTo plugin
        /*scrollToElement: function(whereTo){
            $.scrollTo(whereTo, smoothScroll.scrollTiming, { offset: { top: smoothScroll.topScrollOffset }, easing: 'easeInOutQuart' });
        },*/
        generateTempNavId: function(navId){
            return '_'+navId;
        },
        getNavIdFromHash: function(){
            var hash = window.location.hash;
 
            if (smoothScroll.hashIsTempNavId()) {
                hash = hash.substring(2);
            }
 
            return hash;
        },
        hashIsTempNavId: function(){
            var hash = window.location.hash;
 
            return hash.substring(0,2) === '#_';
        },
 
        loaded: function(){
            if (smoothScroll.hashIsTempNavId()) {
                setTimeout(function(){
                    smoothScroll.scrollToElement('#'+smoothScroll.getNavIdFromHash());
                },
                smoothScroll.pageLoadScrollDelay);
            }
            $(window).smoothWheel();
        }
    };
    smoothScroll.target.bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(){
        smoothScroll.target.stop();
    });
})(jQuery);

(function ($) {
    var self = this,
        container,
        running=false,
        currentY = 0,
        targetY = 0,
        oldY = 0,
        maxScrollTop= 0,
        minScrollTop,
        direction,
        onRenderCallback=null,
        fricton = 0.95, // higher value for slower deceleration
        vy = 0,
        stepAmt = 1,
        minMovement= 0.1;

    var updateScrollTarget = function (amt) {
        targetY += amt;
        vy += (targetY - oldY) * stepAmt;
        oldY = targetY;
    };

    var render = new Core.AnimationFrame(function () {
        if (vy < -(minMovement) || vy > minMovement) {
            currentY = (currentY + vy);
            if (currentY > maxScrollTop) {
                currentY = vy = 0;
            } else if (currentY < minScrollTop) {
                vy = 0;
                currentY = minScrollTop;
            }
            container.scrollTop(-currentY);
            vy *= fricton;
            if(onRenderCallback){
                onRenderCallback();
            }
        }
    });

    var onWheel = function (e) {
        e.preventDefault();
        var evt = e.originalEvent;
        var delta = evt.detail ? evt.detail * -1 : evt.wheelDelta / 40;
        var dir = delta < 0 ? -1 : 1;
        if (dir != direction) {
            vy = 0;
            direction = dir;
        }
        //reset currentY in case non-wheel scroll has occurred (scrollbar drag, etc.)
        currentY = -container.scrollTop();
        updateScrollTarget(delta);
    };
    var arrowKeyScroll = function (event) {
        var ArrowKeys = {
            'up': '38',
            'down': '40'
        };
        if (event.keyCode == ArrowKeys.up || event.keyCode == ArrowKeys.down ) {
            if(document.activeElement){
                switch(document.activeElement.tagName){
                    case 'INPUT':
                    case 'TEXTAREA':
                    case 'SELECT':
                        return;
                    default:break;
                }
            }
            event.preventDefault();
            if (event.keyCode == ArrowKeys.up) {
                event.originalEvent.wheelDelta = 120;
            } else {
                event.originalEvent.wheelDelta = -120;
            }
            onWheel(event);
        }
    };
    /*
     * http://jsbin.com/iqafek/2/edit
     */
    var normalizeWheelDelta = function () {
        // Keep a distribution of observed values, and scale by the
        // 33rd percentile.
        var distribution = [],
            done = null,
            scale = 30;
        return function (n) {
            // Zeroes don't count.
            if (n === 0) return n;
            // After 500 samples, we stop sampling and keep current factor.
            if (done !== null) return n * done;
            var abs = Math.abs(n);
            // Insert value (sorted in ascending order).
            outer: do { // Just used for break goto
                for (var i = 0; i < distribution.length; ++i) {
                    if (abs <= distribution[i]) {
                        distribution.splice(i, 0, abs);
                        break outer;
                    }
                }
                distribution.push(abs);
            } while (false);
            // Factor is scale divided by 33rd percentile.
            var factor = scale / distribution[Math.floor(distribution.length / 3)];
            if (distribution.length == 500) done = factor;
            return n * factor;
        };
    }();
    /**
     * @param {Array} options Possible options are: onRender(func), remove(boolean)
     * @returns {*}
     */
    $.fn.smoothWheel = function () {
        var options = jQuery.extend({}, arguments[0]);
        return this.each(function (index, elm) {
            if(!('ontouchstart' in window)){
                container = $(this);
                container.bind("DOMMouseScroll mousewheel", onWheel);
                container.bind("keydown", arrowKeyScroll);
                //set target/old/current Y to match current scroll position to prevent jump to top of container
                targetY = oldY = container.scrollTop();
                currentY = -targetY;
                minScrollTop = container.get(0).clientHeight - container.get(0).scrollHeight;
                container.on('resize', function(){
                    minScrollTop = container.get(0).clientHeight - container.get(0).scrollHeight;
                });
                if(options.onRender)
                    onRenderCallback = options.onRender;
                if(options.remove){
                    render.RequestLoop(false);
                    container.unbind("DOMMouseScroll mousewheel", onWheel);
                    container.unbind("keydown", arrowKeyScroll);
                    vy = 0;
                }else if(!render.running)
                    render.RequestLoop(true);
            }
        });
    };
})(jQuery);

$(function()
{
    var targets = $('[rel~=tooltip]'),
            target = false,
            tooltip = false,
            title = false;

    targets.bind('mouseenter', function()
    {
        target = $(this);
        tip = target.attr('title');
        tooltip = $('<div id="tooltip"></div>');

        if (!tip || tip === '')
            return false;

        target.removeAttr('title');
        tooltip.css('opacity', 0)
                .html(tip)
                .appendTo('body');

        var init_tooltip = function()
        {
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css('max-width', $(window).width() / 2);
            else
                tooltip.css('max-width', 340);

            var pos_left = target.offset().left + (target.outerWidth() / 2) - (tooltip.outerWidth() / 2),
                pos_top = target.offset().top - tooltip.outerHeight() - 22;

            if (pos_left < 0){
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass('left');
            }
            else
                tooltip.removeClass('left');

            if (pos_left + tooltip.outerWidth() > $(window).width()){
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass('right');
            }
            else
                tooltip.removeClass('right');

            if (pos_top < 0){
                pos_top = target.offset().top + target.outerHeight();
                tooltip.addClass('top');
            }
            else
                tooltip.removeClass('top');

            tooltip.css({left: pos_left, top: pos_top})
                .animate({top: '+=10', opacity: 1}, 300, "easeOutQuint");
        };

        init_tooltip();
        $(window).resize(init_tooltip);

        var remove_tooltip = function()
        {
            tooltip.animate({top: '-=10', opacity: 0}, 200, "easeInQuint", function(){
                $(this).remove();
            });

            target.attr('title', tip);
        };

        target.bind('mouseleave', remove_tooltip);
        tooltip.bind('click', remove_tooltip);
    });
});

(function($) {
 
    $.fn.gmap = function(options) {
        
        var that = $(this);
        var style = that.data('gmap-style');
        
        // Set default settings
        var settings = $.extend({
            zoom: parseInt(that.attr("data-zoom"),10) || 6,
            center: new google.maps.LatLng(that.attr("data-latitude"), that.attr("data-longitude")),
            html: that.attr("data-title"),
            mapTypeControl: style ? false : true,
            popup: true,
            scaleControl: true,
            draggable: true,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }, options);
        
        var infoContent = that.html();
        var map = new google.maps.Map(that[0], settings);
        
        if (style) {
            var styledMap = new google.maps.StyledMapType(style, {name: "Styled Map"});
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }

        var mLat = that.attr("data-marker-latitude"),
            mLng = that.attr("data-marker-longitude");
    
        if(!mLat || !mLng)
            return;
        
        var infowindow = new google.maps.InfoWindow({
            content: infoContent
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(mLat,mLng),
            map: map,
            title: that.attr("data-title")
        });
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
    };
}(jQuery));
//# sourceMappingURL=WebStatic.js.map