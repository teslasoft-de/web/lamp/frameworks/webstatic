/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : main.ts
 Created    : 2016-09-12 01:53:43 +0200
 Updated    : 2016-09-12 01:53:43 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */
/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="Core/LogReader.ts"/>
var baseDir = '/WebStatic/js/';
//var baseDir = './../';
var bowerDir = baseDir + 'bower_components/';
var vendorDir = baseDir + 'vendor/';
requirejs.config({
    paths: {
        'text': bowerDir + 'requirejs-text/text',
        'durandal': bowerDir + 'Durandal/js',
        'plugins': bowerDir + 'Durandal/js/plugins',
        'transitions': bowerDir + 'Durandal/js/transitions',
        'knockout': bowerDir + 'knockout/dist/knockout',
        'bootstrap': bowerDir + 'bootstrap-sass/assets/javascripts/bootstrap.min',
        'bootstrap-select': bowerDir + 'bootstrap-select/dist/js/bootstrap-select.min',
        'bootstrap-maxlength': bowerDir + 'bootstrap-maxlength/dist/bootstrap-maxlength.min',
        'jquery': bowerDir + 'jquery/jquery',
        'dockspawn': vendorDir + 'dockspawn.amd.min',
        'dock_tree_vis': vendorDir + 'dock_tree_vis.amd',
        'jit': vendorDir + 'jit.amd.min',
        'logsaver': vendorDir + 'logsaver.amd.min',
    },
    shim: {
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        }
    }
});
requirejs.onResourceLoad = function (context, map, depArray) {
    console.log("Loading:" + map.name);
};
// Initialize log and load app.
define(['jquery', 'Core/LogReader'], function ($, LogReader) {
    var logReader = LogReader.LogReader.Instance;
    var messages = $('#splash_messages');
    logReader.LogWritten.Subscribe(function () {
        // Display log messages
        messages.text(LogReader.LogReader.Messages);
        messages[0].scrollTop = messages[0].scrollHeight;
    });
    LogReader.LogReader.Stop(); // Force localStorage cleanup
    logReader.Start();
    var app = require(['app'], function () {
    });
});
//# sourceMappingURL=main.js.map