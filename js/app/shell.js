/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : shell.ts
    Created    : 2016-09-13 10:31:57 +0200
    Updated    : 2016-09-13 10:31:57 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/
define(["require", "exports", 'plugins/router', 'knockout'], function (require, exports, _router, _ko) {
    "use strict";
    var ShellView = (function () {
        function ShellView() {
            this.Managers = _ko.observableArray();
        }
        ShellView.prototype.LoadManagers = function () {
            // Manual view model configuration for design mode
            // These values can also fetched from the service to create fully dynamic navigation.
            this.Managers.push();
        };
        ShellView.prototype.Activate = function () {
            this.LoadManagers();
            var routes = [
                { route: ['', 'SiteManager'], moduleId: 'SiteManager/SiteManager', title: 'Site Manager', nav: 1 },
                { route: ['Analytics'], moduleId: 'Analytics/Analytics', title: 'Analytics', nav: 1 },
                { route: ['Project'], moduleId: 'Project/Project', title: 'Project', nav: 1 },
                { route: ['Tickets'], moduleId: 'Tickets/Tickets', title: 'Tickets', nav: 1 },
            ];
            return exports.router.makeRelative({ moduleId: 'ViewModel' })
                .map(routes)
                .buildNavigationModel()
                .mapUnknownRoutes('SiteManager', 'not-found')
                .activate();
        };
        ShellView.prototype.CompositionComplete = function (view, parent) {
        };
        return ShellView;
    }());
    exports.ShellView = ShellView;
    exports.router = _router;
    //Export to the DOM as vm
    exports.vm = new ShellView();
    //The Durandal plugin-interface
    exports.activate = function () {
        return exports.vm.Activate();
    };
    exports.compositionComplete = function (view, parent) {
        return exports.vm.CompositionComplete(view, parent);
    };
});
//# sourceMappingURL=shell.js.map