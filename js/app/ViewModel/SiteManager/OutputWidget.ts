/// <reference path="../../../typings/tsd.d.ts"/>

import _ko = require('knockout');
import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';
import {LogReader} from '../../Core/LogReader';

export class OutputWidget extends WidgetContainer
{
    public LogMessages: KnockoutObservable<string> = _ko.observable<string>();

    constructor(
        dockManager: WidgetHost,
        element: string = 'output_widget',
        title: string = 'Output',
        icon: string = 'fa fa-info-circle',
        className: string = 'output-window.editor-host')
    {
        super(element, dockManager, title, icon, className);

        if (!this.IsDefault) {
            LogReader.Stop();
            // TODO: Start log reader
            return;
        }
    }

    Dock()
    {
        this.dockDown(null, 0.2);
        super.Dock();

        LogReader.Instance.LogWritten.Subscribe(this.UpdateLog);
        this.UpdateLog();
    }

    public AutoResize(view: HTMLElement): void
    {
        super.AutoResize(view);
        let $pre = $('#' + this.ElementID + ' pre');

        if (!$pre)
            return;

        let $view = $(view);
        $pre.css({width: $view.width(), height: $view.height()});

        $pre[0].scrollTop = $pre[0].scrollHeight;
    }

    private UpdateLog: ()=>void = ()=>
    {
        this.LogMessages(LogReader.Messages);
        let $pre = $('#' + this.ElementID + ' pre')[0];
        $pre.scrollTop = $pre.scrollHeight;
    }
}