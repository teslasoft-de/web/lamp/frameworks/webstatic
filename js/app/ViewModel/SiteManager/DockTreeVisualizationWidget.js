/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", 'dock_tree_vis', './../../Common/WidgetContainer'], function (require, exports, dock_tree_vis_1, WidgetContainer_1) {
    "use strict";
    var DockTreeVisualizationWidget = (function (_super) {
        __extends(DockTreeVisualizationWidget, _super);
        function DockTreeVisualizationWidget(dockManager, element, title, icon, className) {
            var _this = this;
            if (element === void 0) { element = 'infovis'; }
            if (title === void 0) { title = 'Dock Tree Visualizer'; }
            if (icon === void 0) { icon = 'fa fa-sitemap'; }
            if (className === void 0) { className = 'editor2-window.editor-host'; }
            _super.call(this, element, dockManager, title, icon, className, false);
            this.Initialized.Subscribe(function () {
                _this.WidgetHost.DockListener.ResumeLayout.Subscribe(function (manager) {
                    // TODO: Figure out how to resize
                    //this.AutoResizeDiv('infovis-canvaswidget');
                    //this.AutoResizeCanvas('infovis-canvas');
                });
            });
        }
        DockTreeVisualizationWidget.prototype.Dock = function () {
            this.dockFill();
            _super.prototype.Dock.call(this);
            this.AutoResizeDiv('infovis-canvaswidget');
            this.AutoResizeCanvas('infovis-canvas');
            dock_tree_vis_1.InitDebugTreeVis(this.WidgetHost.DockManager);
        };
        DockTreeVisualizationWidget.prototype.AutoResizeDiv = function (elementID) {
            var div = $(document.getElementById(elementID));
            if (!div)
                return;
            div.css({ width: $(this.PanelContainer.elementContent).width() });
            div.css({ height: $(this.PanelContainer.elementContent).height() });
        };
        DockTreeVisualizationWidget.prototype.AutoResizeCanvas = function (elementID) {
            var canvas = $(document.getElementById(elementID));
            if (!canvas)
                return;
            var contentElement = $(this.PanelContainer.elementContent);
            canvas.width(contentElement.width());
            canvas.height(contentElement.height());
            canvas.attr({ width: contentElement.width(), height: contentElement.height() });
        };
        return DockTreeVisualizationWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.DockTreeVisualizationWidget = DockTreeVisualizationWidget;
});
//# sourceMappingURL=DockTreeVisualizationWidget.js.map