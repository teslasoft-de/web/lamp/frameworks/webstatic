/// <reference path="../../../typings/tsd.d.ts"/>

import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';
import {OutputWidget} from './OutputWidget';

export class ProblemsWidget extends WidgetContainer
{
    constructor(
        dockManager:WidgetHost,
        element:string = 'problems_widget',
        title:string = 'Problems',
        icon:string = 'fa fa-exclamation-circle',
        className:string = 'problems-window')
    {
        super(element, dockManager, title, icon, className);

        this.DockParent = <any>OutputWidget;
    }

    Dock()
    {
        this.dockRight(null, 0.40);
        super.Dock();
    }
}