/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer', './DomainsWidget'], function (require, exports, WidgetContainer_1, DomainsWidget_1) {
    "use strict";
    var PropertiesWidget = (function (_super) {
        __extends(PropertiesWidget, _super);
        function PropertiesWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = 'properties_widget'; }
            if (title === void 0) { title = 'Properties'; }
            if (icon === void 0) { icon = 'fa fa-wrench'; }
            if (className === void 0) { className = 'properties-window'; }
            _super.call(this, element, dockManager, title, icon, className);
            this.DockParent = DomainsWidget_1.DomainsWidget;
        }
        PropertiesWidget.prototype.Dock = function () {
            this.dockDown(null, 0.6);
            _super.prototype.Dock.call(this);
        };
        return PropertiesWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.PropertiesWidget = PropertiesWidget;
});
//# sourceMappingURL=PropertiesWidget.js.map