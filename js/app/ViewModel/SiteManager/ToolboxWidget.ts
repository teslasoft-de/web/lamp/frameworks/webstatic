/// <reference path="../../../typings/tsd.d.ts"/>

import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';

export class ToolboxWidget extends WidgetContainer
{
    constructor(
        dockManager:WidgetHost,
        element:string = 'toolbox_widget',
        title:string = 'Toolbox',
        icon:string = 'fa fa-cog',
        className:string = 'toolbox-window')
    {
        super(element, dockManager, title, icon, className);
    }

    Dock()
    {
        this.dockRight(null, 0.20);
        super.Dock();
    }
}