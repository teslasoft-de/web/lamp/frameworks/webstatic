/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer'], function (require, exports, WidgetContainer_1) {
    "use strict";
    var DomainsWidget = (function (_super) {
        __extends(DomainsWidget, _super);
        function DomainsWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = 'domain_widget'; }
            if (title === void 0) { title = 'Domains'; }
            if (icon === void 0) { icon = 'fa fa-globe'; }
            if (className === void 0) { className = 'solution-window'; }
            _super.call(this, element, dockManager, title, icon, className);
        }
        DomainsWidget.prototype.Dock = function () {
            this.dockLeft(null, 0.15);
            _super.prototype.Dock.call(this);
        };
        return DomainsWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.DomainsWidget = DomainsWidget;
});
//# sourceMappingURL=DomainsWidget.js.map