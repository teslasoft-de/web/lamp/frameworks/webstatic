/// <reference path="../../../typings/tsd.d.ts"/>

import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';
import {DomainsWidget} from './DomainsWidget';

export class PropertiesWidget extends WidgetContainer
{
    constructor(
        dockManager:WidgetHost,
        element:string = 'properties_widget',
        title:string = 'Properties',
        icon:string = 'fa fa-wrench',
        className:string = 'properties-window')
    {
        super(element, dockManager, title, icon, className);

        this.DockParent = <any>DomainsWidget;
    }

    Dock()
    {
        this.dockDown(null, 0.6);
        super.Dock();
    }
}