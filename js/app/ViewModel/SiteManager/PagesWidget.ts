/// <reference path="../../../typings/tsd.d.ts"/>

import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';
import {DomainsWidget} from './DomainsWidget';

export class PagesWidget extends WidgetContainer
{
    constructor(
        dockManager:WidgetHost,
        element:string = 'pages_widget',
        title:string = 'Pages',
        icon:string = 'fa fa-list',
        className:string = 'outline-window')
    {
        super(element, dockManager, title, icon, className);

        this.DockParent = <any>DomainsWidget;
    }

    Dock()
    {
        this.dockFill();
        super.Dock();
    }
}
