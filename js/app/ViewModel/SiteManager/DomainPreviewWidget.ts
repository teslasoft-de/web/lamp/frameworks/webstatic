/// <reference path="../../../typings/tsd.d.ts"/>

import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';

export class DomainPreviewWidget extends WidgetContainer
{
    Url: string;
    ID: string;

    constructor(
        dockManager: WidgetHost,
        element: string = '',
        title: string = 'Domain Preview',
        icon: string = 'fa fa-globe',
        className: string = 'editor1-window.editor-host')
    {
        super('domain_preview_widget_' + element, dockManager, 'Domain Preview - ' + title, icon, className);
        this.Url = document.location.protocol.substr(0,document.location.protocol.indexOf(':')) + '://' + window.location.hostname;
        this.ID = 'domain_preview_widget_window';
    }

    Dock()
    {
        this.dockFill();
    }

    public AutoResize(view: HTMLElement): void
    {
        let iframe = <HTMLIFrameElement> document.getElementById(this.ID);

        if (!iframe)
            return;

        let $view = $(view);

        iframe.height = ($view.height()) + "px";
        iframe.width = ($view.width()) + "px";
    }
}