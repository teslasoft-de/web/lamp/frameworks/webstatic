import app = require('durandal/app');
import _ko = require('knockout');
import _observable = require('plugins/observable')

import {MulticastDelegate} from './../../Core/MulticastDelegate';
import {WidgetContainer} from './../../Common/WidgetContainer';

export class WidgetToolbarItem
{
    public Widget: WidgetContainer;
    public Type: {new(...any): WidgetContainer};
    public Instance: WidgetContainer;

    //public Instances:Array<WidgetContainer>;

    constructor(type: {new(...any): WidgetContainer}, widget: WidgetContainer)
    {
        this.Type = type;
        this.Widget = widget;

        _observable(this, 'Instance').subscribe((value)=>
        {
        })
    }

    private _Clicked = new MulticastDelegate<WidgetToolbarItem,MouseEvent>(this);
    public get Clicked(): MulticastDelegate<WidgetToolbarItem,MouseEvent> { return this._Clicked; }

    public Click: (e: MouseEvent) => void =
        (e)=> {
            this._Clicked.Invoke(e);
        }
}