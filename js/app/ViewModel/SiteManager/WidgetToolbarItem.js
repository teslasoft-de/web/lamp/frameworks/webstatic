define(["require", "exports", 'plugins/observable', './../../Core/MulticastDelegate'], function (require, exports, _observable, MulticastDelegate_1) {
    "use strict";
    var WidgetToolbarItem = (function () {
        //public Instances:Array<WidgetContainer>;
        function WidgetToolbarItem(type, widget) {
            var _this = this;
            this._Clicked = new MulticastDelegate_1.MulticastDelegate(this);
            this.Click = function (e) {
                _this._Clicked.Invoke(e);
            };
            this.Type = type;
            this.Widget = widget;
            _observable(this, 'Instance').subscribe(function (value) {
            });
        }
        Object.defineProperty(WidgetToolbarItem.prototype, "Clicked", {
            get: function () { return this._Clicked; },
            enumerable: true,
            configurable: true
        });
        return WidgetToolbarItem;
    }());
    exports.WidgetToolbarItem = WidgetToolbarItem;
});
//# sourceMappingURL=WidgetToolbarItem.js.map