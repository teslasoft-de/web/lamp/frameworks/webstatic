/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", 'knockout', './../../Common/WidgetContainer', '../../Core/LogReader'], function (require, exports, _ko, WidgetContainer_1, LogReader_1) {
    "use strict";
    var OutputWidget = (function (_super) {
        __extends(OutputWidget, _super);
        function OutputWidget(dockManager, element, title, icon, className) {
            var _this = this;
            if (element === void 0) { element = 'output_widget'; }
            if (title === void 0) { title = 'Output'; }
            if (icon === void 0) { icon = 'fa fa-info-circle'; }
            if (className === void 0) { className = 'output-window.editor-host'; }
            _super.call(this, element, dockManager, title, icon, className);
            this.LogMessages = _ko.observable();
            this.UpdateLog = function () {
                _this.LogMessages(LogReader_1.LogReader.Messages);
                var $pre = $('#' + _this.ElementID + ' pre')[0];
                $pre.scrollTop = $pre.scrollHeight;
            };
            if (!this.IsDefault) {
                LogReader_1.LogReader.Stop();
                // TODO: Start log reader
                return;
            }
        }
        OutputWidget.prototype.Dock = function () {
            this.dockDown(null, 0.2);
            _super.prototype.Dock.call(this);
            LogReader_1.LogReader.Instance.LogWritten.Subscribe(this.UpdateLog);
            this.UpdateLog();
        };
        OutputWidget.prototype.AutoResize = function (view) {
            _super.prototype.AutoResize.call(this, view);
            var $pre = $('#' + this.ElementID + ' pre');
            if (!$pre)
                return;
            var $view = $(view);
            $pre.css({ width: $view.width(), height: $view.height() });
            $pre[0].scrollTop = $pre[0].scrollHeight;
        };
        return OutputWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.OutputWidget = OutputWidget;
});
//# sourceMappingURL=OutputWidget.js.map