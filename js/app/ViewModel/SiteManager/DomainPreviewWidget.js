/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer'], function (require, exports, WidgetContainer_1) {
    "use strict";
    var DomainPreviewWidget = (function (_super) {
        __extends(DomainPreviewWidget, _super);
        function DomainPreviewWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = ''; }
            if (title === void 0) { title = 'Domain Preview'; }
            if (icon === void 0) { icon = 'fa fa-globe'; }
            if (className === void 0) { className = 'editor1-window.editor-host'; }
            _super.call(this, 'domain_preview_widget_' + element, dockManager, 'Domain Preview - ' + title, icon, className);
            this.Url = document.location.protocol.substr(0, document.location.protocol.indexOf(':')) + '://' + window.location.hostname;
            this.ID = 'domain_preview_widget_window';
        }
        DomainPreviewWidget.prototype.Dock = function () {
            this.dockFill();
        };
        DomainPreviewWidget.prototype.AutoResize = function (view) {
            var iframe = document.getElementById(this.ID);
            if (!iframe)
                return;
            var $view = $(view);
            iframe.height = ($view.height()) + "px";
            iframe.width = ($view.width()) + "px";
        };
        return DomainPreviewWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.DomainPreviewWidget = DomainPreviewWidget;
});
//# sourceMappingURL=DomainPreviewWidget.js.map