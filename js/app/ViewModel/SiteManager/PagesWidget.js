/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer', './DomainsWidget'], function (require, exports, WidgetContainer_1, DomainsWidget_1) {
    "use strict";
    var PagesWidget = (function (_super) {
        __extends(PagesWidget, _super);
        function PagesWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = 'pages_widget'; }
            if (title === void 0) { title = 'Pages'; }
            if (icon === void 0) { icon = 'fa fa-list'; }
            if (className === void 0) { className = 'outline-window'; }
            _super.call(this, element, dockManager, title, icon, className);
            this.DockParent = DomainsWidget_1.DomainsWidget;
        }
        PagesWidget.prototype.Dock = function () {
            this.dockFill();
            _super.prototype.Dock.call(this);
        };
        return PagesWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.PagesWidget = PagesWidget;
});
//# sourceMappingURL=PagesWidget.js.map