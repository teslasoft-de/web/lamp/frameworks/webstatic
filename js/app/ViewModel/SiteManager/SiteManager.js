/// <reference path="../../../typings/tsd.d.ts"/>
define(["require", "exports", 'durandal/app', 'knockout', "./../../Core/ErrorHandler", './../../Common/WidgetHost', './WidgetToolbarItem', './DomainsWidget', './PropertiesWidget', './PagesWidget', './ToolboxWidget', './OutputWidget', './ProblemsWidget', './DockTreeVisualizationWidget', './DomainPreviewWidget'], function (require, exports, app, _ko, ErrorHandler_1, Common, WidgetToolbarItem_1, DomainsWidget_1, PropertiesWidget_1, PagesWidget_1, ToolboxWidget_1, OutputWidget_1, ProblemsWidget_1, DockTreeVisualizationWidget_1, DomainPreviewWidget_1) {
    "use strict";
    var SiteManager = (function () {
        function SiteManager() {
            this.Title = app.title;
            this.Description = 'Site Manager';
            this.Widgets = _ko.observableArray();
        }
        SiteManager.prototype.CreateWidget = function (
            //container:{new(dockManager:Common.WidgetHost, elementID?:string, title?:string, icon?:string, className?:string):T;},
            container, elementID, title, icon, className) {
            var _this = this;
            var widgetContainer = new container(this.WidgetHost, elementID, title, icon, className);
            var widget = new WidgetToolbarItem_1.WidgetToolbarItem(container, widgetContainer);
            widget.Clicked.Subscribe(function (widget) {
                if (!widget.Instance) {
                    _this.LoadWidget(widget);
                }
            });
            this.Widgets.push(widget);
            return widgetContainer;
        };
        SiteManager.prototype.LoadWidget = function (toolbarItem) {
            if (toolbarItem.Instance)
                return;
            try {
                var widget = toolbarItem.Instance = this.WidgetHost.Create(toolbarItem.Type);
            }
            catch (e) {
                console.log("error -> " + e);
                app.showMessage(e);
                return;
            }
            widget.ElementID = toolbarItem.Widget.ElementID;
            widget.Title = toolbarItem.Widget.Title;
            widget.Icon = toolbarItem.Widget.Icon;
            widget.ClassName = toolbarItem.Widget.ClassName;
            var subscribeOnWidgetClosed = function () {
                widget.PanelContainer.Closed.Subscribe(function () {
                    toolbarItem.Instance = null;
                });
            };
            if (!this.WidgetHost.DockManager)
                widget.Initialized.Subscribe(function (w) {
                    subscribeOnWidgetClosed();
                });
            else {
                widget.Initialize();
                widget.Dock();
                subscribeOnWidgetClosed();
            }
        };
        SiteManager.prototype.Activate = function (id) {
            var _this = this;
            if (this.WidgetHost)
                return;
            try {
                // Create toolbar widgets
                this.WidgetHost = new Common.WidgetHost();
                this.CreateWidget(DomainsWidget_1.DomainsWidget);
                this.CreateWidget(PagesWidget_1.PagesWidget);
                this.CreateWidget(PropertiesWidget_1.PropertiesWidget);
                this.CreateWidget(OutputWidget_1.OutputWidget);
                this.CreateWidget(ProblemsWidget_1.ProblemsWidget);
                this.CreateWidget(ToolboxWidget_1.ToolboxWidget);
                this.CreateWidget(DomainPreviewWidget_1.DomainPreviewWidget /*, 'teslasoft_de', 'teslasoft.de'*/);
                this.CreateWidget(DockTreeVisualizationWidget_1.DockTreeVisualizationWidget);
                // Load add widgets. (Initialization on CompositionComplete when the view has been generated.)
                _ko.utils.arrayForEach(this.Widgets(), function (_widget) {
                    if (_widget.Widget.IsDefault)
                        _this.LoadWidget(_widget);
                });
            }
            catch (e) {
                new ErrorHandler_1.ErrorHandler(this, e, '', '').HandleError();
            }
        };
        SiteManager.prototype.Attached = function (view, parent) {
            try {
            }
            catch (e) {
                new ErrorHandler_1.ErrorHandler(this, e, '', '').HandleError();
            }
        };
        SiteManager.prototype.CompositionComplete = function (view, parent) {
            try {
                this.WidgetHost.Initialize($(view).find('#widget-host')[0]);
                this.WidgetHost.Dock();
            }
            catch (e) {
                new ErrorHandler_1.ErrorHandler(this, e, '', '').HandleError();
            }
        };
        return SiteManager;
    }());
    exports.SiteManager = SiteManager;
    //Export to the DOM as vm
    exports.vm = new SiteManager();
    // The Durandal plugin-interface
    exports.activate = function (id) {
        return exports.vm.Activate(id);
    };
    exports.attached = function (view, parent) {
        return exports.vm.Attached(view, parent);
    };
    exports.compositionComplete = function (view, parent) {
        return exports.vm.CompositionComplete(view, parent);
    };
});
//# sourceMappingURL=SiteManager.js.map