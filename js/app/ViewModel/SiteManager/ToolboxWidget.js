/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer'], function (require, exports, WidgetContainer_1) {
    "use strict";
    var ToolboxWidget = (function (_super) {
        __extends(ToolboxWidget, _super);
        function ToolboxWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = 'toolbox_widget'; }
            if (title === void 0) { title = 'Toolbox'; }
            if (icon === void 0) { icon = 'fa fa-cog'; }
            if (className === void 0) { className = 'toolbox-window'; }
            _super.call(this, element, dockManager, title, icon, className);
        }
        ToolboxWidget.prototype.Dock = function () {
            this.dockRight(null, 0.20);
            _super.prototype.Dock.call(this);
        };
        return ToolboxWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.ToolboxWidget = ToolboxWidget;
});
//# sourceMappingURL=ToolboxWidget.js.map