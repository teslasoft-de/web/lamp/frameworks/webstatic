/// <reference path="../../../typings/tsd.d.ts"/>

import app = require('durandal/app');
import _ko = require('knockout');

import {ErrorHandler} from "./../../Core/ErrorHandler";

import * as Common from './../../Common/WidgetHost'
import {WidgetContainer} from './../../Common/WidgetContainer';

import {WidgetToolbarItem} from './WidgetToolbarItem';
import {DomainsWidget} from './DomainsWidget';
import {PropertiesWidget} from './PropertiesWidget';
import {PagesWidget} from './PagesWidget';
import {ToolboxWidget} from './ToolboxWidget';
import {OutputWidget} from './OutputWidget';
import {ProblemsWidget} from './ProblemsWidget';
import {DockTreeVisualizationWidget} from './DockTreeVisualizationWidget';
import {DomainPreviewWidget} from './DomainPreviewWidget';

export class SiteManager
{
    public Title: string = app.title;
    public Description: string = 'Site Manager';
    public WidgetHost: Common.WidgetHost;

    public Widgets: KnockoutObservableArray<WidgetToolbarItem> = _ko.observableArray<WidgetToolbarItem>();

    constructor()
    {

    }

    public CreateWidget<T extends WidgetContainer>(
        //container:{new(dockManager:Common.WidgetHost, elementID?:string, title?:string, icon?:string, className?:string):T;},
        container: {new(...any): T;},
        elementID?: string,
        title?: string,
        icon?: string,
        className?: string)
    {
        let widgetContainer = new container(this.WidgetHost, elementID, title, icon, className);
        let widget = new WidgetToolbarItem(container, widgetContainer);

        widget.Clicked.Subscribe((widget: WidgetToolbarItem)=>
        {
            if (!widget.Instance) {
                this.LoadWidget(widget);
            }
        });

        this.Widgets.push(widget);

        return widgetContainer;
    }

    private LoadWidget(toolbarItem: WidgetToolbarItem)
    {
        if (toolbarItem.Instance)
            return;
        try {
            var widget = toolbarItem.Instance = this.WidgetHost.Create(toolbarItem.Type);
        }
        catch (e) {
            console.log("error -> " + e);
            app.showMessage(e);
            return;
        }

        widget.ElementID = toolbarItem.Widget.ElementID;
        widget.Title = toolbarItem.Widget.Title;
        widget.Icon = toolbarItem.Widget.Icon;
        widget.ClassName = toolbarItem.Widget.ClassName;

        let subscribeOnWidgetClosed: ()=>void = function ()
        {
            widget.PanelContainer.Closed.Subscribe(()=>
            {
                toolbarItem.Instance = null;
            });
        };

        if (!this.WidgetHost.DockManager)
            widget.Initialized.Subscribe((w)=>
            {
                subscribeOnWidgetClosed();
            });
        else {
            widget.Initialize();
            widget.Dock();
            subscribeOnWidgetClosed();
        }
    }

    public Activate(id)
    {
        if (this.WidgetHost)
            return;
        try {
            // Create toolbar widgets
            this.WidgetHost = new Common.WidgetHost();
            this.CreateWidget(DomainsWidget);
            this.CreateWidget(PagesWidget);

            this.CreateWidget(PropertiesWidget);

            this.CreateWidget(OutputWidget);
            this.CreateWidget(ProblemsWidget);

            this.CreateWidget(ToolboxWidget);

            this.CreateWidget(DomainPreviewWidget/*, 'teslasoft_de', 'teslasoft.de'*/);
            this.CreateWidget(DockTreeVisualizationWidget);

            // Load add widgets. (Initialization on CompositionComplete when the view has been generated.)
            _ko.utils.arrayForEach<WidgetToolbarItem>(this.Widgets(), (_widget: WidgetToolbarItem) =>
            {
                if (_widget.Widget.IsDefault)
                    this.LoadWidget(_widget);
            });
        }
        catch (e) {
            new ErrorHandler(this, e, '','').HandleError();
        }
    }

    public Attached(view, parent)
    {
        try {

        }
        catch (e) {
            new ErrorHandler(this, e, '','').HandleError();
        }
    }

    public CompositionComplete(view: HTMLElement, parent: HTMLElement)
    {
        try {
            this.WidgetHost.Initialize(<HTMLDivElement>$(view).find('#widget-host')[0]);
            this.WidgetHost.Dock();
        }
        catch (e) {
            new ErrorHandler(this, e, '','').HandleError();
        }
    }
}

//Export to the DOM as vm
export var vm = new SiteManager();

// The Durandal plugin-interface
export var activate = function (id)
{
    return vm.Activate(id);
};
export var attached = function (view, parent)
{
    return vm.Attached(view, parent);
};
export var compositionComplete = function (view, parent)
{
    return vm.CompositionComplete(view, parent);
};