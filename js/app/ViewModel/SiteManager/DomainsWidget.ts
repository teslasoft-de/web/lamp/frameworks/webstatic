/// <reference path="../../../typings/tsd.d.ts"/>

import app = require('durandal/app');
import {WidgetHost} from './../../Common/WidgetHost';
import {WidgetContainer} from './../../Common/WidgetContainer';

export class DomainsWidget extends WidgetContainer
{
    constructor(
        dockManager:WidgetHost,
        element:string = 'domain_widget',
        title:string = 'Domains',
        icon:string = 'fa fa-globe',
        className:string = 'solution-window')
    {
        super(element, dockManager, title, icon, className);
    }

    Dock()
    {
        this.dockLeft(null, 0.15);
        super.Dock();
    }
}