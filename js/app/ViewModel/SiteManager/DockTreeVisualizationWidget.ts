/// <reference path="../../../typings/tsd.d.ts"/>

import {InitDebugTreeVis} from 'dock_tree_vis';

import * as Common from './../../Common/WidgetHost'
import {WidgetContainer} from './../../Common/WidgetContainer';


export class DockTreeVisualizationWidget extends WidgetContainer
{
    constructor(
        dockManager:Common.WidgetHost,
        element:string = 'infovis',
        title:string = 'Dock Tree Visualizer',
        icon:string = 'fa fa-sitemap',
        className:string = 'editor2-window.editor-host')
    {
        super(element, dockManager, title, icon, className, false);

        this.Initialized.Subscribe(()=>
        {
            this.WidgetHost.DockListener.ResumeLayout.Subscribe((manager)=>{
                // TODO: Figure out how to resize
                //this.AutoResizeDiv('infovis-canvaswidget');
                //this.AutoResizeCanvas('infovis-canvas');
            })
        })
    }

    Dock()
    {
        this.dockFill();
        super.Dock();
        this.AutoResizeDiv('infovis-canvaswidget');
        this.AutoResizeCanvas('infovis-canvas');
        InitDebugTreeVis(this.WidgetHost.DockManager);
    }

    AutoResizeDiv(elementID:string)
    {
        let div = $(<HTMLDivElement> document.getElementById(elementID));

        if (!div)
            return;


        div.css({width: $(this.PanelContainer.elementContent).width()});
        div.css({height: $(this.PanelContainer.elementContent).height()});
    }

    AutoResizeCanvas(elementID:string)
    {
        let canvas = $(<HTMLCanvasElement> document.getElementById(elementID));

        if (!canvas)
            return;

        let contentElement = $(this.PanelContainer.elementContent);
        canvas.width(contentElement.width());
        canvas.height(contentElement.height());

        canvas.attr({ width:contentElement.width(), height: contentElement.height() });
    }
}