/// <reference path="../../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", './../../Common/WidgetContainer', './OutputWidget'], function (require, exports, WidgetContainer_1, OutputWidget_1) {
    "use strict";
    var ProblemsWidget = (function (_super) {
        __extends(ProblemsWidget, _super);
        function ProblemsWidget(dockManager, element, title, icon, className) {
            if (element === void 0) { element = 'problems_widget'; }
            if (title === void 0) { title = 'Problems'; }
            if (icon === void 0) { icon = 'fa fa-exclamation-circle'; }
            if (className === void 0) { className = 'problems-window'; }
            _super.call(this, element, dockManager, title, icon, className);
            this.DockParent = OutputWidget_1.OutputWidget;
        }
        ProblemsWidget.prototype.Dock = function () {
            this.dockRight(null, 0.40);
            _super.prototype.Dock.call(this);
        };
        return ProblemsWidget;
    }(WidgetContainer_1.WidgetContainer));
    exports.ProblemsWidget = ProblemsWidget;
});
//# sourceMappingURL=ProblemsWidget.js.map