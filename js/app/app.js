/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : app.ts
    Created    : 2016-09-14 00:23:20 +0200
    Updated    : 2016-09-13 10:31:57 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/
define(["require", "exports", 'durandal/system', 'durandal/app', 'durandal/viewLocator', "./Ajax/AppPackageRequest", 'bootstrap', 'bootstrap-maxlength', 'bootstrap-select'], function (require, exports, system, app, viewLocator, AppPackageRequest_1) {
    "use strict";
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");
    //specify which plugins to install and their configuration
    app.configurePlugins({
        router: true,
        dialog: true,
        observable: true
    });
    app.start().then(function () {
        //get app name from package.json
        new AppPackageRequest_1.AppPackageRequest().Deferred(function (data) {
            app.title = data.description;
        });
        viewLocator.useConvention('ViewModel', 'View');
        // Redirect view lookup's to view directory
        viewLocator.convertModuleIdToViewId = function (moduleId) {
            return moduleId.replace(/ViewModel/gi, "View");
        };
        app.setRoot('shell', 'entrance');
    });
});
//# sourceMappingURL=app.js.map