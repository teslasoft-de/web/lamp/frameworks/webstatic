/// <reference path="../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "plugins/dialog", 'durandal/app', "./Utility"], function (require, exports, dialog_1, app, Utility_1) {
    "use strict";
    var MessageBoxType = (function (_super) {
        __extends(MessageBoxType, _super);
        function MessageBoxType() {
            _super.apply(this, arguments);
        }
        MessageBoxType.Default = MessageBoxType.Value('default');
        MessageBoxType.Success = MessageBoxType.Value('success');
        MessageBoxType.Info = MessageBoxType.Value('info');
        MessageBoxType.Warning = MessageBoxType.Value('warning');
        MessageBoxType.Danger = MessageBoxType.Value('danger');
        return MessageBoxType;
    }(Utility_1.Enum));
    exports.MessageBoxType = MessageBoxType;
    var ErrorHandler = (function () {
        function ErrorHandler(_Source, _Error, _Message, _Title) {
            this._Source = _Source;
            this._Error = _Error;
            this._Message = _Message;
            this._Title = _Title;
        }
        Object.defineProperty(ErrorHandler.prototype, "Source", {
            get: function () { return this._Source; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHandler.prototype, "Error", {
            get: function () { return this._Error; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHandler.prototype, "Message", {
            get: function () { return this._Message; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ErrorHandler.prototype, "Title", {
            get: function () { return this._Title; },
            enumerable: true,
            configurable: true
        });
        ErrorHandler.prototype.LogError = function (error) {
            console.log("error -> " + error);
        };
        /**
         *
         * @param message
         * @param title
         * @param type
         * @constructor
         */
        ErrorHandler.prototype.ShowMessage = function (message, title, type) {
            if (type === void 0) { type = MessageBoxType.Danger; }
            var view = dialog_1.MessageBox.defaultViewMarkup;
            dialog_1.MessageBox.defaultViewMarkup = [
                '<div data-view="plugins/messageBox" data-bind="css: getClass(), style: getStyle()">',
                '<div class="modal-header panel-heading">',
                '<h3 data-bind="html: title"></h3>',
                '</div>',
                '<div class="modal-body">',
                '<p class="message" data-bind="html: message"></p>',
                '</div>',
                '<div class="modal-footer">',
                '<!-- ko foreach: options -->',
                '<button data-bind="click: function () { $parent.selectOption($parent.getButtonValue($data)); }, text: $parent.getButtonText($data), css: $parent.getButtonClass($index)"></button>',
                '<!-- /ko -->',
                '<div style="clear:both;"></div>',
                '</div>',
                '</div>'
            ].join('\n');
            app.showMessage(message, title, ['Close'], true, {
                'class': 'modal-content messageBox panel-' + type,
                style: { width: '600px' }
            }).done(function (d) {
                dialog_1.MessageBox.defaultViewMarkup = view;
            });
        };
        ErrorHandler.prototype.HandleError = function () {
            this.LogError(this._Message);
            this.ShowMessage(this._Message, this._Title);
        };
        ErrorHandler.Try = function (func, message, title) {
            try {
                if (func())
                    return true;
                new this(this, Utility_1.getClassName(func.prototype.constructor), message, title).HandleError();
            }
            catch (error) {
                new this(this, error, error.message, title).HandleError();
            }
            return false;
        };
        return ErrorHandler;
    }());
    exports.ErrorHandler = ErrorHandler;
});
//# sourceMappingURL=ErrorHandler.js.map