export function getClassName(inputClass)
{
    var funcNameRegex = /function (.{1,})\(/;
    var results = (funcNameRegex).exec((<any> inputClass).constructor.toString());
    return (results && results.length > 1) ? results[1] : "";
}

/*export function getClassName(inputClass)
 {
 var funcNameRegex = /function (.{1,})\(/;
 var results = (funcNameRegex).exec((<any> inputClass).constructor.toString());
 return (results && results.length > 1) ? results[1] : "";
 }
 (<any>Object.prototype).getName = function () { return getClassName(this);};*/

export abstract class Enum extends Object
{
    public get Value(): string { return this._Value; }

    constructor(private _Value: string) {
        super()
    }

    toString() { return this._Value; }

    /**
     * Creates a static get property for the specified enum which will override the enum property in the derived class.
     * @param value
     * @returns {TEnum}
     * @constructor
     */
    public static Enum<TEnum extends Enum>(value: string): TEnum {
        let instance = new (<any>this)(value);

        Object.defineProperty(this, instance._Value, {
            get         : () => {
                return instance;
            },
            set         : (value) => {},
            enumerable  : true,
            configurable: true
        });

        return instance;
    }

    /**
     * Creates a static get string property for the specified enum value which will override the enum property in the derived class.
     * @param value
     * @returns {string}
     * @constructor
     */
    public static Value(value: string): string {

        Object.defineProperty(this, value, {
            get         : () => {
                return value;
            },
            set         : (value) => {},
            enumerable  : true,
            configurable: true
        });

        return value;
    }
}