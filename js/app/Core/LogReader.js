/**
 * Created by Cosmo on 05.09.2016.
 */
define(["require", "exports", './MulticastDelegate', 'logsaver'], function (require, exports, MulticastDelegate_1, logsaver) {
    "use strict";
    /**
     * Provides log write events and functions for extracting specific log data.
     */
    var LogReader = (function () {
        function LogReader() {
            this._LogWritten = new MulticastDelegate_1.MulticastDelegate(this);
        }
        Object.defineProperty(LogReader, "Instance", {
            get: function () {
                return this._Instance ? this._Instance : this._Instance = new LogReader();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogReader.prototype, "LogWritten", {
            /**
             * Event for capturing log writes.
             * @returns {MulticastDelegate<LogReader, EventArgs>}
             * @constructor
             */
            get: function () {
                return this._LogWritten;
            },
            enumerable: true,
            configurable: true
        });
        LogReader.prototype.Start = function () {
            logsaver.startSavingLog([{ keyForLocalStorage: LogReader._LocalStorageKey }]);
            this._OriginalLog = console.log;
            var _this = this;
            console.log = function log() {
                _this._OriginalLog.apply(console, arguments);
                _this.OnLogWritten();
            };
        };
        LogReader.prototype.OnLogWritten = function () {
            this._LogWritten.Invoke();
        };
        LogReader.Stop = function () {
            logsaver.stopSavingLog();
            logsaver.clearSavedLog();
        };
        Object.defineProperty(LogReader, "Messages", {
            /**
             * Returns the all log entry messages without the type name concatenated with new line.
             * @returns {string}
             */
            get: function () {
                // Get the log message part after the date.
                var lines = localStorage[LogReader._LocalStorageKey].replace(/\r\n/, /\n/);
                return lines.split('\n').map(function (line, index, array) {
                    // Strip type
                    var match = line.match(/::\s(.*)/);
                    return match ? match[1] : '';
                }).join('\n');
            },
            enumerable: true,
            configurable: true
        });
        LogReader._LocalStorageKey = 'console.log.saved';
        return LogReader;
    }());
    exports.LogReader = LogReader;
});
//# sourceMappingURL=LogReader.js.map