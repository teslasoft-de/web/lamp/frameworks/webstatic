export class EventArgs {
    public static Empty: EventArgs = new EventArgs();
}

export interface IEventDelegate
{
    <TSender extends any, TEventArgs extends EventArgs>(sender: TSender, eventArgs?: TEventArgs): void
}

export interface IMulticastDelegate<
    TSender extends any,
    TEventArgs extends EventArgs>
{
    Subscribe(handler: IEventDelegate): void;
    Unsubscribe(handler: IEventDelegate): void;
    Invoke(eventArgs?: TEventArgs): void;
}

export class MulticastDelegate<
    TSender extends any,
    TEventArgs extends EventArgs>
implements IMulticastDelegate<TSender, TEventArgs>
{
    get Target(): TSender {
        return this._Target;
    }

    constructor(private _Target: TSender)
    {
    }

    private _Handlers: IEventDelegate[] = [];

    public get Handlers(): IEventDelegate[]
    {
        return this._Handlers;
    }

    public Subscribe(handler: IEventDelegate): void
    {
        this._Handlers.push(handler);
    }

    public Unsubscribe(handler: IEventDelegate): void
    {
        this._Handlers = this._Handlers.filter(h => h !== handler);
    }

    public Invoke(eventArgs?: TEventArgs): void
    {
        this._Handlers.slice(0).forEach(h => h(this._Target, (eventArgs ? eventArgs : EventArgs.Empty)));
    }
}