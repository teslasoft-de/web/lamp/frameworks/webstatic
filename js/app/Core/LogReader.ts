/**
 * Created by Cosmo on 05.09.2016.
 */

/// <reference path="../../typings/tsd.d.ts"/>

import {MulticastDelegate, IMulticastDelegate, EventArgs} from './MulticastDelegate';
import * as logsaver from 'logsaver';

/**
 * Provides log write events and functions for extracting specific log data.
 */
export class LogReader
{
    public static get Instance(): LogReader
    {
        return this._Instance ? this._Instance : this._Instance = new LogReader();
    }

    private static _Instance: LogReader;

    static _LocalStorageKey: string = 'console.log.saved';
    private _OriginalLog;

    private _LogWritten = new MulticastDelegate<LogReader,EventArgs>(this);

    /**
     * Event for capturing log writes.
     * @returns {MulticastDelegate<LogReader, EventArgs>}
     * @constructor
     */
    public get LogWritten(): IMulticastDelegate<LogReader,EventArgs>
    {
        return this._LogWritten;
    }

    public Start(): void
    {
        logsaver.startSavingLog([{keyForLocalStorage: LogReader._LocalStorageKey}]);

        this._OriginalLog = console.log;
        let _this = this;

        console.log = function log()
        {
            _this._OriginalLog.apply(console, arguments);
            _this.OnLogWritten();
        }
    }

    protected OnLogWritten(): void
    {
        this._LogWritten.Invoke();
    }

    public static Stop(): void
    {
        logsaver.stopSavingLog();
        logsaver.clearSavedLog();
    }

    /**
     * Returns the all log entry messages without the type name concatenated with new line.
     * @returns {string}
     */
    public static get Messages(): string
    {
        // Get the log message part after the date.
        let lines = localStorage[LogReader._LocalStorageKey].replace(/\r\n/, /\n/);
        return lines.split('\n').map((line, index, array)=> {
            // Strip type
            let match = line.match(/::\s(.*)/);
            return match ? match[1] : '';
        }).join('\n');
    }
}
