/// <reference path="../../typings/tsd.d.ts"/>

import {MessageBox} from "plugins/dialog";
import app = require('durandal/app');

import {getClassName, Enum} from "./Utility";

export interface IErrorHandler
{
    Message: string
    Title: string
    HandleError(): void
    LogError(error: string): void
    ShowMessage(error: string, title: string)
}

export class MessageBoxType extends Enum {
    static Default = MessageBoxType.Value('default');
    static Success = MessageBoxType.Value('success');
    static Info = MessageBoxType.Value('info');
    static Warning = MessageBoxType.Value('warning');
    static Danger = MessageBoxType.Value('danger');
}
export class ErrorHandler<TSource, TErrorObject> implements IErrorHandler
{
    get Source(): TSource { return this._Source; }

    get Error(): TErrorObject { return this._Error; }

    get Message(): string { return this._Message; }

    get Title(): string { return this._Title; }

    constructor(
        protected _Source: TSource,
        protected _Error: TErrorObject,
        protected _Message: string,
        protected _Title: string)
    {}

    public LogError(error: string): void
    {
        console.log("error -> " + error);
    }

    /**
     *
     * @param message
     * @param title
     * @param type
     * @constructor
     */
    public ShowMessage(message: string, title: string, type:string|MessageBoxType = MessageBoxType.Danger): void
    {
        let view = (<any>MessageBox).defaultViewMarkup;
        (<any>MessageBox).defaultViewMarkup = [
            '<div data-view="plugins/messageBox" data-bind="css: getClass(), style: getStyle()">',
                '<div class="modal-header panel-heading">',
                    '<h3 data-bind="html: title"></h3>',
                '</div>',
                '<div class="modal-body">',
                    '<p class="message" data-bind="html: message"></p>',
                '</div>',
                '<div class="modal-footer">',
                    '<!-- ko foreach: options -->',
                    '<button data-bind="click: function () { $parent.selectOption($parent.getButtonValue($data)); }, text: $parent.getButtonText($data), css: $parent.getButtonClass($index)"></button>',
                    '<!-- /ko -->',
                    '<div style="clear:both;"></div>',
                '</div>',
            '</div>'
        ].join('\n');

        app.showMessage(message, title, ['Close'], true, {
            'class': 'modal-content messageBox panel-' + type,
            style  : {width: '600px'}
        }).done((d) =>{
            (<any>MessageBox).defaultViewMarkup = view;
        });
    }

    public HandleError(): void
    {
        this.LogError(this._Message);
        this.ShowMessage(this._Message, this._Title);
    }

    public static Try<TResult>(func: ()=>TResult, message: string, title: string)
    {
        try {
            if (func())
                return true;

            new this(this, getClassName(func.prototype.constructor), message, title).HandleError();
        }
        catch (error) {
            new this(this, error, error.message, title).HandleError();
        }
        return false;
    }
}