/// <reference path="../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./MulticastDelegate", "./ErrorHandler", "./Utility"], function (require, exports, MulticastDelegate_1, ErrorHandler_1, Utility_1) {
    "use strict";
    var AjaxSettings;
    (function (AjaxSettings) {
        var Methods = (function (_super) {
            __extends(Methods, _super);
            function Methods() {
                _super.apply(this, arguments);
            }
            Methods.POST = Methods.Value('POST');
            Methods.GET = Methods.Value('GET');
            Methods.PUT = Methods.Value('PUT');
            return Methods;
        }(Utility_1.Enum));
        AjaxSettings.Methods = Methods;
        var DataTypes = (function (_super) {
            __extends(DataTypes, _super);
            function DataTypes() {
                _super.apply(this, arguments);
            }
            DataTypes.xml = DataTypes.Value('xml');
            DataTypes.json = DataTypes.Value('json');
            DataTypes.html = DataTypes.Value('html');
            DataTypes.script = DataTypes.Value('script');
            return DataTypes;
        }(Utility_1.Enum));
        AjaxSettings.DataTypes = DataTypes;
    })(AjaxSettings = exports.AjaxSettings || (exports.AjaxSettings = {}));
    /**
     * @see http://api.jquery.com/jQuery.ajax
     */
    var AjaxRequest = (function () {
        function AjaxRequest(_Name, url, _Cached) {
            var _this = this;
            if (_Cached === void 0) { _Cached = false; }
            this._Name = _Name;
            this._Cached = _Cached;
            this._Settings = {};
            this._RequestComplete = new MulticastDelegate_1.MulticastDelegate(this);
            this._RequestFinished = new MulticastDelegate_1.MulticastDelegate(this);
            this._RequestFailed = new MulticastDelegate_1.MulticastDelegate(this);
            this.OnSuccess = function (data, textStatus, jqXHR) {
                console.log(_this._Name + ':Success');
                return _this._Data = data;
            };
            this.OnError = function (jqXHR, textStatus, errorThrown) {
                var errorType = _this.ErrorHandler();
                _this._ErrorHandler = new errorType(_this, jqXHR, textStatus, errorThrown);
                // Handle error by default with custom error handler.
                // TODO: Search for all base classes.
                if (_this._ErrorHandler.constructor.prototype instanceof AjaxErrorHandler)
                    _this._ErrorHandler.HandleError();
                _this._RequestFailed.Invoke(new AjaxRequestFailedEventArgs(_this._ErrorHandler));
                return _this._ErrorHandler;
            };
            this.OnComplete = function (data, textStatus, objectStatus) {
                _this._RequestComplete.Invoke(new AjaxRequestCompletedEventArgs(data, textStatus, objectStatus));
            };
            /**
             * Raises the AjaxRequest.RequestFinished event when AjaxRequest.Execute was called.
             * @param data
             * @constructor
             */
            this.OnFinished = function (data) {
                _this._RequestFinished.Invoke(new AjaxRequestFinishedEventArgs(data));
            };
            this._Settings.url = url;
        }
        Object.defineProperty(AjaxRequest.prototype, "Name", {
            get: function () { return this._Name; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "Cached", {
            get: function () { return this._Cached; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "Settings", {
            get: function () { return this._Settings; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "Promise", {
            get: function () { return this._Promise; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "Error", {
            get: function () { return this._ErrorHandler; },
            enumerable: true,
            configurable: true
        });
        //noinspection JSMethodCanBeStatic
        /**
         * When overwritten, it returns the constructor of the error handler type.
         * @returns {AjaxErrorHandler}
         */
        AjaxRequest.prototype.ErrorHandler = function () { return AjaxErrorHandler; };
        Object.defineProperty(AjaxRequest.prototype, "RequestComplete", {
            get: function () { return this._RequestComplete; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "RequestFinished", {
            get: function () { return this._RequestFinished; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequest.prototype, "RequestFailed", {
            get: function () { return this._RequestFailed; },
            enumerable: true,
            configurable: true
        });
        AjaxRequest.CreateCache = function (target) {
            var cache = {};
            return function (key) {
                if (!cache[key]) {
                    cache[key] = $.Deferred(function (defer) {
                        target(defer).then(function () {
                            // TODO: Cleanup
                        });
                    }).promise();
                }
                return cache[key];
            };
        };
        AjaxRequest.prototype.Initialize = function (request) {
            if (this._Cached) {
                if (!AjaxRequest._Cache[this._Name]) {
                    AjaxRequest._Cache[this._Name] = AjaxRequest.CreateCache(function (defer) {
                        return request.then(defer.resolve, defer.reject);
                    });
                }
                this._Promise = AjaxRequest._Cache[this._Name](this._Settings.url)
                    .then(this.OnSuccess, this.OnError)
                    .always(this.OnComplete);
                console.log(this._Name + ':Cached');
            }
            else {
                this._Promise = request
                    .then(this.OnSuccess, this.OnError)
                    .always(this.OnComplete);
                console.log(this._Name + ':Created');
            }
        };
        AjaxRequest.prototype.Execute = function () {
            var _this = this;
            if (ErrorHandler_1.ErrorHandler.Try(function () { return _this._Promise; }, 'Cannot execute ajax request until Initialize has not been called.', this._Name))
                this._Promise = this._Promise.done(this.OnFinished);
            return this._Promise;
        };
        AjaxRequest.prototype.Deferred = function (success, fail) {
            var _this = this;
            if (ErrorHandler_1.ErrorHandler.Try(function () { return _this._Promise; }, 'Cannot execute ajax request until Initialize has not been called.', this._Name)) {
                this._Promise = this._Promise.done(success);
                if (fail)
                    this._Promise = this._Promise.fail(fail);
            }
            return this._Promise;
        };
        AjaxRequest._Cache = [];
        return AjaxRequest;
    }());
    exports.AjaxRequest = AjaxRequest;
    var AjaxRequestCompletedEventArgs = (function () {
        function AjaxRequestCompletedEventArgs(_Data, _TextStatus, _ObjectStatus) {
            this._Data = _Data;
            this._TextStatus = _TextStatus;
            this._ObjectStatus = _ObjectStatus;
        }
        Object.defineProperty(AjaxRequestCompletedEventArgs.prototype, "Data", {
            get: function () { return this._Data; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequestCompletedEventArgs.prototype, "TextStatus", {
            get: function () { return this._TextStatus; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequestCompletedEventArgs.prototype, "ObjectStatus", {
            get: function () { return this._ObjectStatus; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequestCompletedEventArgs, "Empty", {
            get: function () {
                return this._Empty;
            },
            enumerable: true,
            configurable: true
        });
        AjaxRequestCompletedEventArgs._Empty = function () {
            return this._Empty
                ? this._Empty
                : this._Empty = new AjaxRequestCompletedEventArgs(null, null, null);
        };
        return AjaxRequestCompletedEventArgs;
    }());
    exports.AjaxRequestCompletedEventArgs = AjaxRequestCompletedEventArgs;
    var AjaxRequestFinishedEventArgs = (function () {
        function AjaxRequestFinishedEventArgs(_Data) {
            this._Data = _Data;
        }
        Object.defineProperty(AjaxRequestFinishedEventArgs.prototype, "Data", {
            get: function () { return this._Data; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequestFinishedEventArgs, "Empty", {
            get: function () {
                return this._Empty;
            },
            enumerable: true,
            configurable: true
        });
        AjaxRequestFinishedEventArgs._Empty = function () {
            return this._Empty
                ? this._Empty
                : this._Empty = new AjaxRequestFinishedEventArgs(null);
        };
        return AjaxRequestFinishedEventArgs;
    }());
    exports.AjaxRequestFinishedEventArgs = AjaxRequestFinishedEventArgs;
    var AjaxRequestFailedEventArgs = (function () {
        function AjaxRequestFailedEventArgs(_Error) {
            this._Error = _Error;
        }
        Object.defineProperty(AjaxRequestFailedEventArgs.prototype, "Error", {
            get: function () { return this._Error; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxRequestFailedEventArgs, "Empty", {
            get: function () {
                return this._Empty;
            },
            enumerable: true,
            configurable: true
        });
        AjaxRequestFailedEventArgs._Empty = function () {
            return this._Empty
                ? this._Empty
                : this._Empty = new AjaxRequestFinishedEventArgs(null);
        };
        return AjaxRequestFailedEventArgs;
    }());
    exports.AjaxRequestFailedEventArgs = AjaxRequestFailedEventArgs;
    var AjaxErrorHandler = (function (_super) {
        __extends(AjaxErrorHandler, _super);
        function AjaxErrorHandler(source, error, _TextStatus, _ErrorThrown) {
            _super.call(this, source, error, 'Unexpected Server Response: ' + error.status + ' - ' + _ErrorThrown + '\n' +
                'URL: ' + source.Settings.url, source.Name + ' - Ajax Error');
            this._TextStatus = _TextStatus;
            this._ErrorThrown = _ErrorThrown;
        }
        Object.defineProperty(AjaxErrorHandler.prototype, "TextStatus", {
            get: function () { return this._TextStatus; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AjaxErrorHandler.prototype, "ErrorThrown", {
            get: function () { return this._ErrorThrown; },
            enumerable: true,
            configurable: true
        });
        return AjaxErrorHandler;
    }(ErrorHandler_1.ErrorHandler));
    exports.AjaxErrorHandler = AjaxErrorHandler;
});
//# sourceMappingURL=AjaxRequest.js.map