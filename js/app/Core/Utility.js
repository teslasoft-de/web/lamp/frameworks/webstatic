var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports"], function (require, exports) {
    "use strict";
    function getClassName(inputClass) {
        var funcNameRegex = /function (.{1,})\(/;
        var results = (funcNameRegex).exec(inputClass.constructor.toString());
        return (results && results.length > 1) ? results[1] : "";
    }
    exports.getClassName = getClassName;
    /*export function getClassName(inputClass)
     {
     var funcNameRegex = /function (.{1,})\(/;
     var results = (funcNameRegex).exec((<any> inputClass).constructor.toString());
     return (results && results.length > 1) ? results[1] : "";
     }
     (<any>Object.prototype).getName = function () { return getClassName(this);};*/
    var Enum = (function (_super) {
        __extends(Enum, _super);
        function Enum(_Value) {
            _super.call(this);
            this._Value = _Value;
        }
        Object.defineProperty(Enum.prototype, "Value", {
            get: function () { return this._Value; },
            enumerable: true,
            configurable: true
        });
        Enum.prototype.toString = function () { return this._Value; };
        /**
         * Creates a static get property for the specified enum which will override the enum property in the derived class.
         * @param value
         * @returns {TEnum}
         * @constructor
         */
        Enum.Enum = function (value) {
            var instance = new this(value);
            Object.defineProperty(this, instance._Value, {
                get: function () {
                    return instance;
                },
                set: function (value) { },
                enumerable: true,
                configurable: true
            });
            return instance;
        };
        /**
         * Creates a static get string property for the specified enum value which will override the enum property in the derived class.
         * @param value
         * @returns {string}
         * @constructor
         */
        Enum.Value = function (value) {
            Object.defineProperty(this, value, {
                get: function () {
                    return value;
                },
                set: function (value) { },
                enumerable: true,
                configurable: true
            });
            return value;
        };
        return Enum;
    }(Object));
    exports.Enum = Enum;
});
//# sourceMappingURL=Utility.js.map