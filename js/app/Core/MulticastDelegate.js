define(["require", "exports"], function (require, exports) {
    "use strict";
    var EventArgs = (function () {
        function EventArgs() {
        }
        EventArgs.Empty = new EventArgs();
        return EventArgs;
    }());
    exports.EventArgs = EventArgs;
    var MulticastDelegate = (function () {
        function MulticastDelegate(_Target) {
            this._Target = _Target;
            this._Handlers = [];
        }
        Object.defineProperty(MulticastDelegate.prototype, "Target", {
            get: function () {
                return this._Target;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MulticastDelegate.prototype, "Handlers", {
            get: function () {
                return this._Handlers;
            },
            enumerable: true,
            configurable: true
        });
        MulticastDelegate.prototype.Subscribe = function (handler) {
            this._Handlers.push(handler);
        };
        MulticastDelegate.prototype.Unsubscribe = function (handler) {
            this._Handlers = this._Handlers.filter(function (h) { return h !== handler; });
        };
        MulticastDelegate.prototype.Invoke = function (eventArgs) {
            var _this = this;
            this._Handlers.slice(0).forEach(function (h) { return h(_this._Target, (eventArgs ? eventArgs : EventArgs.Empty)); });
        };
        return MulticastDelegate;
    }());
    exports.MulticastDelegate = MulticastDelegate;
});
//# sourceMappingURL=MulticastDelegate.js.map