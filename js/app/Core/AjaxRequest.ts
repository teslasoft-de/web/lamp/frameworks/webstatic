/// <reference path="../../typings/tsd.d.ts"/>

import {MulticastDelegate, EventArgs} from "./MulticastDelegate";
import {ErrorHandler} from "./ErrorHandler";
import {Enum} from "./Utility";

export interface AjaxSettings extends JQueryAjaxSettings {}
export namespace AjaxSettings {
    export class Methods extends Enum
    {
        static POST = Methods.Value('POST');
        static GET = Methods.Value('GET');
        static PUT = Methods.Value('PUT');
    }

    export class DataTypes extends Enum
    {
        static xml = DataTypes.Value('xml');
        static json = DataTypes.Value('json');
        static html = DataTypes.Value('html');
        static script = DataTypes.Value('script');
    }
}

export interface CacheHandler
{
    <TResult>(defer: JQueryDeferred<TResult>): JQueryPromise<TResult>;
}

export interface RequestHandler
{
    <TResult>(key: string): JQueryDeferred<TResult>;
}

interface IAjaxRequest {
    Settings: JQueryAjaxSettings
    Name: string
    Cached: boolean
    Initialize(request: JQueryXHR)
}

/**
 * @see http://api.jquery.com/jQuery.ajax
 */
export abstract class AjaxRequest<TResult, TError extends AjaxErrorHandler<IAjaxRequest>> implements IAjaxRequest
{
    protected _Settings: AjaxSettings = <AjaxSettings>{};
    protected _Promise: JQueryPromise<TResult>;
    protected _Data: TResult;
    protected _ErrorHandler: TError;

    get Name(): string { return this._Name; }

    get Cached(): boolean { return this._Cached; }

    get Settings(): JQueryAjaxSettings { return this._Settings; }

    get Promise(): JQueryPromise<TResult> { return this._Promise; }

    get Error(): TError { return this._ErrorHandler; }

    //noinspection JSMethodCanBeStatic
    /**
     * When overwritten, it returns the constructor of the error handler type.
     * @returns {AjaxErrorHandler}
     */
    protected ErrorHandler(): AjaxErrorHandlerConstructor { return AjaxErrorHandler; }

    private _RequestComplete = new MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestCompletedEventArgs<TResult>>(this);
    public get RequestComplete(): MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestCompletedEventArgs<TResult>> { return this._RequestComplete; }

    private _RequestFinished = new MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestFinishedEventArgs<TResult>>(this);
    public get RequestFinished(): MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestFinishedEventArgs<TResult>> { return this._RequestFinished; }

    private _RequestFailed = new MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestFailedEventArgs<TError>>(this);
    public get RequestFailed(): MulticastDelegate<AjaxRequest<TResult, TError>,AjaxRequestFailedEventArgs<TError>> { return this._RequestFailed; }

    constructor(private _Name: string, url: string, protected _Cached: boolean = false)
    {
        this._Settings.url = url;
    }

    protected static CreateCache<TResult>(target: CacheHandler): RequestHandler
    {
        let cache = {};
        return (key: string) => {
            if (!cache[key]) {
                cache[key] = $.Deferred<TResult>((defer)=> {
                    target(defer).then(()=> {
                        // TODO: Cleanup
                    });
                }).promise();
            }
            return (<JQueryDeferred<TResult>>cache[key]);
        }
    }

    static _Cache: Array<RequestHandler> = [];

    public Initialize(request: JQueryXHR)
    {
        if (this._Cached) {
            if (!AjaxRequest._Cache[this._Name]) {
                AjaxRequest._Cache[this._Name] = AjaxRequest.CreateCache<TResult>((defer) => {
                    return request.then<TResult>(defer.resolve, defer.reject)
                });
            }
            this._Promise = AjaxRequest._Cache[this._Name](this._Settings.url)
                .then(this.OnSuccess, this.OnError)
                .always(this.OnComplete);

            console.log(this._Name + ':Cached');
        }
        else {
            this._Promise = request
                .then<TResult>(this.OnSuccess, this.OnError)
                .always(this.OnComplete);

            console.log(this._Name + ':Created');
        }
    }

    public Execute(): JQueryPromise<TResult>
    {
        if (ErrorHandler.Try(()=> this._Promise,
                'Cannot execute ajax request until Initialize has not been called.', this._Name))
            this._Promise = this._Promise.done(this.OnFinished);

        return this._Promise;
    }

    public Deferred(success: (result: TResult)=>void, fail?: (error: TError) =>void): JQueryPromise<TResult>
    {
        if (ErrorHandler.Try(()=> this._Promise,
                'Cannot execute ajax request until Initialize has not been called.', this._Name)) {
            this._Promise = this._Promise.done(success);
            if(fail)
                this._Promise = this._Promise.fail(fail);
        }

        return this._Promise;
    }

    protected OnSuccess: (data: TResult, textStatus: string, jqXHR: JQueryXHR) => TResult = (data, textStatus, jqXHR) =>
    {
        console.log(this._Name + ':Success');
        return this._Data = data;
    };

    protected OnError: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => TError =
        (jqXHR, textStatus, errorThrown) =>
        {
            let errorType = this.ErrorHandler();
            this._ErrorHandler = new errorType<IAjaxRequest,TError>(this, jqXHR, textStatus, errorThrown);
            // Handle error by default with custom error handler.
            // TODO: Search for all base classes.
            if (this._ErrorHandler.constructor.prototype instanceof AjaxErrorHandler)
                this._ErrorHandler.HandleError();
            this._RequestFailed.Invoke(new AjaxRequestFailedEventArgs(this._ErrorHandler));
            return this._ErrorHandler;
        };

    protected OnComplete: (data: TResult|JQueryXHR, textStatus: string, objectStatus: JQueryXHR|string) => void =
        (data, textStatus, objectStatus) =>
        {
            this._RequestComplete.Invoke(new AjaxRequestCompletedEventArgs(data, textStatus, objectStatus));
        };

    /**
     * Raises the AjaxRequest.RequestFinished event when AjaxRequest.Execute was called.
     * @param data
     * @constructor
     */
    protected OnFinished: (data: TResult) => void =
        (data) =>
        {
            this._RequestFinished.Invoke(new AjaxRequestFinishedEventArgs(data));
        }
}

export class AjaxRequestCompletedEventArgs<TResult> implements EventArgs
{
    get Data(): TResult|JQueryXHR { return this._Data; }

    get TextStatus(): string { return this._TextStatus; }

    get ObjectStatus(): any { return this._ObjectStatus; }

    constructor(
        private _Data: TResult|JQueryXHR, private _TextStatus: string,
        private _ObjectStatus: JQueryXHR|string)
    {}

    private static _Empty: any = function <TResult>() {
        return this._Empty
            ? this._Empty
            : this._Empty = new AjaxRequestCompletedEventArgs<TResult>(null, null, null);
    };

    static get Empty(): any {
        return this._Empty;
    }
}

export class AjaxRequestFinishedEventArgs<TResult> implements EventArgs
{
    get Data(): TResult { return this._Data; }

    constructor(private _Data: TResult) {}

    private static _Empty: any = function <TResult>() {
        return this._Empty
            ? this._Empty
            : this._Empty = new AjaxRequestFinishedEventArgs<TResult>(null);
    };

    static get Empty(): any {
        return this._Empty;
    }
}

export class AjaxRequestFailedEventArgs<TError extends AjaxErrorHandler<IAjaxRequest>> implements EventArgs
{
    get Error(): TError { return this._Error; }

    constructor(private _Error: TError) {}

    private static _Empty: any = function <TError extends AjaxErrorHandler<IAjaxRequest>>() {
        return this._Empty
            ? this._Empty
            : this._Empty = new AjaxRequestFinishedEventArgs<TError>(null);
    };

    static get Empty(): any {
        return this._Empty;
    }
}

export interface AjaxErrorHandlerConstructor
{
    new<TSource extends IAjaxRequest, TError extends AjaxErrorHandler<IAjaxRequest>>(
        source: TSource,
        error: JQueryXHR,
        textStatus: string,
        errorThrown: string): TError;
}

export class AjaxErrorHandler<TRequest extends IAjaxRequest> extends ErrorHandler<TRequest, JQueryXHR>
{
    get TextStatus(): string { return this._TextStatus; }

    get ErrorThrown(): string { return this._ErrorThrown; }

    constructor(source: TRequest, error: JQueryXHR, protected _TextStatus: string, protected _ErrorThrown: string) {
        super(
            source,
            error,
            'Unexpected Server Response: ' + error.status + ' - ' + _ErrorThrown + '\n' +
            'URL: ' + source.Settings.url,
            source.Name + ' - Ajax Error');
    }
}