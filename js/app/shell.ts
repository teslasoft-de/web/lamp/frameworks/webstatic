/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : shell.ts
    Created    : 2016-09-13 10:31:57 +0200
    Updated    : 2016-09-13 10:31:57 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/

/// <reference path="../typings/tsd.d.ts"/>

import _router = require('plugins/router');
import _ko = require('knockout');

export interface IShellViewManager
{
    hash: string
    name: string
    isActive: boolean
}

export class ShellView
{
    public Managers: KnockoutObservableArray<IShellViewManager> = _ko.observableArray<IShellViewManager>();

    constructor()
    {

    }

    private LoadManagers()
    {
        // Manual view model configuration for design mode
        // These values can also fetched from the service to create fully dynamic navigation.
        this.Managers.push(
            //{hash:'#SiteManager', name:'Site Manager', isActive: false}
        );
    }

    public Activate()
    {
        this.LoadManagers();
        var routes = [
            {route: ['', 'SiteManager'], moduleId: 'SiteManager/SiteManager', title: 'Site Manager', nav: 1},
            {route: ['Analytics'], moduleId: 'Analytics/Analytics', title: 'Analytics', nav: 1},
            {route: ['Project'], moduleId: 'Project/Project', title: 'Project', nav: 1},
            {route: ['Tickets'], moduleId: 'Tickets/Tickets', title: 'Tickets', nav: 1},
            //{ route: ['benchmark/:id'], moduleId: 'Benchmark', title: 'Benchmark', hash: '#benchmark/:id' }
        ];
        return router.makeRelative({moduleId: 'ViewModel'})
            .map(routes)
            .buildNavigationModel()
            .mapUnknownRoutes('SiteManager', 'not-found')
            .activate();
    }

    public CompositionComplete(view: HTMLElement, parent: HTMLElement)
    {

    }
}

export var router = _router;
//Export to the DOM as vm
export var vm = new ShellView();

//The Durandal plugin-interface
export var activate = function ()
{
    return vm.Activate();
};

export var compositionComplete = function (view, parent)
{
    return vm.CompositionComplete(view, parent);
};