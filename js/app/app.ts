/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
    Project    : WebStatic - 0.8.9
    File       : app.ts
    Created    : 2016-09-14 00:23:20 +0200
    Updated    : 2016-09-13 10:31:57 +0200

    Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
    Company    : Teslasoft
    Copyright  : © 2016 Teslasoft, Christian Kusmanow
*/

/// <reference path="../typings/tsd.d.ts"/>

import 'bootstrap';
import 'bootstrap-maxlength';
import 'bootstrap-select';

import * as system from 'durandal/system';
import * as app from 'durandal/app';
import * as viewLocator from 'durandal/viewLocator';
import {AppPackageRequest} from "./Ajax/AppPackageRequest";

//>>excludeStart("build", true);
system.debug(true);
//>>excludeEnd("build");

//specify which plugins to install and their configuration
app.configurePlugins({
    router    : true,
    dialog    : true,
    observable: true
});

app.start().then(function ()
{
    //get app name from package.json
    new AppPackageRequest().Deferred(
        (data)=> {
            app.title = data.description;
        });

    viewLocator.useConvention('ViewModel', 'View');
    // Redirect view lookup's to view directory
    viewLocator.convertModuleIdToViewId = function (moduleId)
    {
        return moduleId.replace(/ViewModel/gi, "View");
    };

    app.setRoot('shell', 'entrance');
});