/**
 * Created by Cosmo on 30.08.2016.
 */
/// <reference path="../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", 'dockspawn', './../Core/Utility', './../Core/MulticastDelegate'], function (require, exports, dockspawn, Utility, MulticastDelegate_1) {
    "use strict";
    var PanelContainerBase = (function (_super) {
        __extends(PanelContainerBase, _super);
        function PanelContainerBase(elementID, dockManager, title) {
            var element = document.getElementById(elementID);
            if (!element)
                throw new Error("Widget container element not found: '" + elementID + "'");
            _super.call(this, element, dockManager.DockManager, title);
        }
        return PanelContainerBase;
    }(dockspawn.PanelContainer));
    exports.PanelContainerBase = PanelContainerBase;
    var PanelClosedEventArgs = (function (_super) {
        __extends(PanelClosedEventArgs, _super);
        function PanelClosedEventArgs(MouseEvent) {
            _super.call(this);
            this.MouseEvent = MouseEvent;
        }
        return PanelClosedEventArgs;
    }(MulticastDelegate_1.EventArgs));
    exports.PanelClosedEventArgs = PanelClosedEventArgs;
    var PanelSelectEventArgs = (function (_super) {
        __extends(PanelSelectEventArgs, _super);
        function PanelSelectEventArgs(TabPage) {
            _super.call(this);
            this.TabPage = TabPage;
        }
        return PanelSelectEventArgs;
    }(MulticastDelegate_1.EventArgs));
    exports.PanelSelectEventArgs = PanelSelectEventArgs;
    var PanelContainer = (function (_super) {
        __extends(PanelContainer, _super);
        function PanelContainer() {
            _super.apply(this, arguments);
            this._closed = new MulticastDelegate_1.MulticastDelegate(this);
            this._selected = new MulticastDelegate_1.MulticastDelegate(this);
        }
        Object.defineProperty(PanelContainer.prototype, "Closed", {
            get: function () {
                return this._closed;
            },
            enumerable: true,
            configurable: true
        });
        PanelContainer.prototype.onCloseButtonClicked = function (e) {
            _super.prototype.onCloseButtonClicked.call(this, e);
            this.TriggerClosed(e);
        };
        PanelContainer.prototype.TriggerClosed = function (e) {
            this._closed.Invoke(new PanelClosedEventArgs(e));
        };
        Object.defineProperty(PanelContainer.prototype, "Selected", {
            get: function () {
                return this._selected;
            },
            enumerable: true,
            configurable: true
        });
        PanelContainer.prototype.onSelected = function (page) {
            this._selected.Invoke(new PanelSelectEventArgs(page));
        };
        return PanelContainer;
    }(PanelContainerBase));
    exports.PanelContainer = PanelContainer;
    /**
     *
     */
    var WidgetContainer = (function () {
        /**
         *
         * @param elementID
         * @param dockManager
         * @param title
         * @param icon
         * @param className
         * @param isDefault
         */
        function WidgetContainer(elementID, dockManager, title, icon, className, isDefault) {
            if (isDefault === void 0) { isDefault = true; }
            this._Initialized = new MulticastDelegate_1.MulticastDelegate(this);
            this.ElementID = elementID;
            this.WidgetHost = dockManager;
            this.Title = title;
            this.Icon = icon;
            this.ClassName = className;
            this.IsDefault = isDefault;
            this.View = Utility.getClassName(this);
        }
        Object.defineProperty(WidgetContainer.prototype, "PanelContainer", {
            get: function () {
                return this._PanelContainer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetContainer.prototype, "Initialized", {
            get: function () {
                return this._Initialized;
            },
            enumerable: true,
            configurable: true
        });
        WidgetContainer.prototype.Initialize = function (initEventHandlerCallback) {
            var _this = this;
            this._PanelContainer = new PanelContainer(this.ElementID, this.WidgetHost, this.Title);
            this._PanelContainer.setTitleIcon(this.Icon);
            if (initEventHandlerCallback)
                initEventHandlerCallback();
            this._PanelContainer.Selected.Subscribe(function (page) {
                // TODO: Create selected Event
                _this.AutoResize(_this.PanelContainer.elementContent);
            });
            /*this._PanelContainer.Closed.Subscribe((page) =>
             {
             // TODO: Create closed Event
             });*/
            this._PanelContainer.Closed.Subscribe(function (e) {
                _this._PanelContainer = null;
                _this.DockNode = null;
                _this.WidgetHost.Widgets.destroy(_this);
            });
            this.OnInitialized(this);
        };
        WidgetContainer.prototype.OnInitialized = function (sender) {
            var _this = this;
            this.WidgetHost.DockListener.ResumeLayout.Subscribe(function (manager) {
                _this.AutoResize(_this.PanelContainer.elementContent);
            });
            this._Initialized.Invoke();
        };
        WidgetContainer.prototype.AutoResize = function (view) {
            var div = $('#' + this.ElementID);
            if (!div)
                return;
            var $view = $(view);
            div.css({ width: $view.width(), height: $view.height() });
        };
        /**
         * Returns the dock parent node for this instance.
         * @param container
         * @returns dockspawn.DockNode
         * @constructor
         */
        WidgetContainer.prototype.FindDockParentNode = function (container) {
            // Return the container dock node if specified.
            if (container) {
                // Break if container was not jet added
                if (!container.DockNode)
                    throw new Error("Widget '" + this.Title + "' cannot docked to widget '" + container.Title + "' because it is itself not docked.");
                return container.DockNode;
            }
            // Get dock parent if defined in this instance.
            var dockParent;
            if (this.DockParent)
                dockParent = this.WidgetHost.Find(this.DockParent);
            // Return dock managers dock node if no dock parent was found.
            return dockParent
                ? dockParent.DockNode
                : this.WidgetHost.DockManager.context.model.documentManagerNode;
        };
        /**
         *
         * @param container
         * @param ratio
         * @returns {DockNode}
         */
        WidgetContainer.prototype.dockLeft = function (container, ratio) {
            return this.DockNode = this.WidgetHost.DockManager.dockLeft(this.FindDockParentNode(container), this._PanelContainer, ratio);
        };
        /**
         *
         * @param container
         * @param ratio
         * @returns {DockNode}
         */
        WidgetContainer.prototype.dockRight = function (container, ratio) {
            return this.DockNode = this.WidgetHost.DockManager.dockRight(this.FindDockParentNode(container), this._PanelContainer, ratio);
        };
        /**
         *
         * @param container
         * @param ratio
         * @returns {DockNode}
         */
        WidgetContainer.prototype.dockUp = function (container, ratio) {
            return this.DockNode = this.WidgetHost.DockManager.dockUp(this.FindDockParentNode(container), this._PanelContainer, ratio);
        };
        /**
         *
         * @param container
         * @param ratio
         * @returns {DockNode}
         */
        WidgetContainer.prototype.dockDown = function (container, ratio) {
            return this.DockNode = this.WidgetHost.DockManager.dockDown(this.FindDockParentNode(container), this._PanelContainer, ratio);
        };
        /**
         *
         * @param container
         * @returns {DockNode}
         */
        WidgetContainer.prototype.dockFill = function (container) {
            return this.DockNode = this.WidgetHost.DockManager.dockFill(this.FindDockParentNode(container), this._PanelContainer);
        };
        /**
         * Default dock operation.
         */
        WidgetContainer.prototype.Dock = function () {
            this.AutoResize(this._PanelContainer.elementContent);
        };
        return WidgetContainer;
    }());
    exports.WidgetContainer = WidgetContainer;
});
//# sourceMappingURL=WidgetContainer.js.map