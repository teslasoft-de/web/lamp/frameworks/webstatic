/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : WidgetHost.ts
 Created    : 2016-08-30 18:31:01 +0200
 Updated    : 2016-08-30 18:31:01 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", 'knockout', 'dockspawn', './../Core/MulticastDelegate', './WidgetContainer'], function (require, exports, _ko, dockspawn, MulticastDelegate_1, WidgetContainer_1) {
    "use strict";
    var DockManager = (function (_super) {
        __extends(DockManager, _super);
        function DockManager() {
            _super.apply(this, arguments);
        }
        return DockManager;
    }(dockspawn.DockManager));
    var TabHost = (function (_super) {
        __extends(TabHost, _super);
        function TabHost() {
            _super.apply(this, arguments);
        }
        TabHost.prototype.onTabPageSelected = function (page) {
            _super.prototype.onTabPageSelected.call(this, page);
            page.container.onSelected(page);
        };
        TabHost.prototype.setActiveTab = function (container) {
            _super.prototype.setActiveTab.call(this, container);
        };
        return TabHost;
    }(dockspawn.TabHost));
    dockspawn.TabHost = TabHost;
    var TabHandle = (function (_super) {
        __extends(TabHandle, _super);
        function TabHandle() {
            _super.apply(this, arguments);
        }
        TabHandle.prototype.onCloseButtonClicked = function (e) {
            this.parent.container.TriggerClosed(e);
            _super.prototype.onCloseButtonClicked.call(this, e);
        };
        return TabHandle;
    }(dockspawn.TabHandle));
    dockspawn.TabHandle = TabHandle;
    var TabPage = (function (_super) {
        __extends(TabPage, _super);
        function TabPage() {
            _super.apply(this, arguments);
        }
        return TabPage;
    }(dockspawn.TabPage));
    dockspawn.TabPage = TabPage;
    var DocumentTabPage = (function (_super) {
        __extends(DocumentTabPage, _super);
        function DocumentTabPage() {
            _super.apply(this, arguments);
        }
        return DocumentTabPage;
    }(dockspawn.DocumentTabPage));
    dockspawn.DocumentTabPage = DocumentTabPage;
    var LayoutEventArgs = (function (_super) {
        __extends(LayoutEventArgs, _super);
        function LayoutEventArgs(dockManager) {
            _super.call(this);
            this.dockManager = dockManager;
        }
        LayoutEventArgs.Empty = new LayoutEventArgs(null);
        return LayoutEventArgs;
    }(MulticastDelegate_1.EventArgs));
    var DockListener = (function () {
        function DockListener() {
            this._SuspendLayout = new MulticastDelegate_1.MulticastDelegate(this);
            this._ResumeLayout = new MulticastDelegate_1.MulticastDelegate(this);
        }
        DockListener.prototype.onDock = function (dockManager, dockNode) {
            //var rootNode = dockManager.context.model.rootNode;
        };
        DockListener.prototype.onUndock = function (dockManager, dockNode) {
            //var rootNode = dockManager.context.model.rootNode;
        };
        Object.defineProperty(DockListener.prototype, "SuspendLayout", {
            get: function () { return this._SuspendLayout; },
            enumerable: true,
            configurable: true
        });
        DockListener.prototype.onSuspendLayout = function (dockManager) { this._SuspendLayout.Invoke(new LayoutEventArgs(dockManager)); };
        Object.defineProperty(DockListener.prototype, "ResumeLayout", {
            get: function () { return this._ResumeLayout; },
            enumerable: true,
            configurable: true
        });
        DockListener.prototype.onResumeLayout = function (dockManager) { this._ResumeLayout.Invoke(new LayoutEventArgs(dockManager)); };
        return DockListener;
    }());
    /**
     *
     */
    var WidgetHost = (function () {
        /**
         *
         */
        function WidgetHost() {
            this.Widgets = _ko.observableArray();
            this._Initialized = new MulticastDelegate_1.MulticastDelegate(this);
        }
        Object.defineProperty(WidgetHost.prototype, "DockListener", {
            get: function () {
                return this._DockListener;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetHost.prototype, "Initialized", {
            get: function () {
                return this._Initialized;
            },
            enumerable: true,
            configurable: true
        });
        /**
         *
         * @param manager
         * @constructor
         */
        WidgetHost.prototype.Initialize = function (manager) {
            var _this = this;
            var dockManager = this.DockManager = new DockManager(manager);
            dockManager.initialize();
            // Let the dock manager element fill in the entire screen
            var onResized = function (e) {
                _this.DockManager.resize(window.innerWidth - (manager.clientLeft + manager.offsetLeft), window.innerHeight - (manager.clientTop + manager.offsetTop));
            };
            window.onresize = onResized;
            onResized(null);
            this._DockListener = new DockListener();
            dockManager.addLayoutListener(this._DockListener);
            this._Initialized.Invoke();
        };
        /**
         *
         * @param elementID
         * @param title
         * @returns {WidgetContainer}
         * @constructor
         * @param icon
         * @param className
         */
        WidgetHost.prototype.Add = function (elementID, title, icon, className) {
            var widget = new WidgetContainer_1.WidgetContainer(elementID, this, title, icon, className);
            this.Widgets.push(widget);
            return widget;
        };
        /**
         *
         * @param container
         * @returns {T} WidgetContainer
         * @constructor
         * @param elementID
         * @param title
         * @param icon
         * @param className
         */
        WidgetHost.prototype.Create = function (container, elementID, title, icon, className) {
            var widget = new container(this, elementID, title, icon, className);
            this.Widgets.push(widget);
            return widget;
        };
        /**
         *
         * @param type
         * @returns {WidgetContainer}
         * @constructor
         * @param elementID
         */
        WidgetHost.prototype.Find = function (type, elementID) {
            var widget = null;
            _ko.utils.arrayForEach(this.Widgets(), function (_widget) {
                // Search for type and compare element IDs if provided.
                if (_widget instanceof type && !elementID || _widget.ElementID == elementID)
                    widget = _widget;
            });
            return widget;
        };
        WidgetHost.prototype.Dock = function () {
            _ko.utils.arrayForEach(this.Widgets(), function (_widget) {
                _widget.Initialize();
                _widget.Dock();
            });
            _ko.utils.arrayForEach(this.Widgets(), function (_widget) {
                _widget.AutoResize(_widget.PanelContainer.elementContent);
            });
        };
        return WidgetHost;
    }());
    exports.WidgetHost = WidgetHost;
});
//# sourceMappingURL=WidgetHost.js.map