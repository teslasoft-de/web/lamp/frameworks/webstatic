/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : WidgetHost.ts
 Created    : 2016-08-30 18:31:01 +0200
 Updated    : 2016-08-30 18:31:01 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */

/// <reference path="../../typings/tsd.d.ts"/>

import _ko = require('knockout');

//import * as d from 'dockspawn';
import dockspawn = require('dockspawn');

import {MulticastDelegate, IMulticastDelegate, EventArgs} from './../Core/MulticastDelegate';
import {WidgetContainer} from './WidgetContainer';
import {PanelContainer} from './WidgetContainer';
import DockNode = dockspawn.DockNode;

class DockManager extends dockspawn.DockManager
{
    addLayoutListener: (listener: DockListener) => void;
    PanelContainer: PanelContainer;
}

class TabHost extends dockspawn.TabHost
{

    onTabPageSelected(page: dockspawn.TabPage): void
    {
        super.onTabPageSelected(page);
        page.container.onSelected(page)
    }

    setActiveTab(container: dockspawn.PanelContainer): void
    {
        super.setActiveTab(container);
    }
}
dockspawn.TabHost = TabHost;

class TabHandle extends dockspawn.TabHandle
{
    onCloseButtonClicked(e): void
    {
        (<PanelContainer>this.parent.container).TriggerClosed(e);
        super.onCloseButtonClicked(e);
    }
}
dockspawn.TabHandle = TabHandle;

class TabPage extends dockspawn.TabPage
{
    container: PanelContainer;
}
dockspawn.TabPage = TabPage;

class DocumentTabPage extends dockspawn.DocumentTabPage
{
    handle: TabHandle;
}
dockspawn.DocumentTabPage = DocumentTabPage;

class LayoutEventArgs extends EventArgs
{
    constructor(public dockManager: DockManager)
    {
        super();
    }

    public static Empty: LayoutEventArgs = new LayoutEventArgs(null);
}

class DockListener implements dockspawn.DockListener
{
    onDock(dockManager, dockNode)
    {
        //var rootNode = dockManager.context.model.rootNode;
    }

    onUndock(dockManager, dockNode)
    {
        //var rootNode = dockManager.context.model.rootNode;
    }

    private _SuspendLayout = new MulticastDelegate<DockListener,LayoutEventArgs>(this);
    public get SuspendLayout(): IMulticastDelegate<DockListener,LayoutEventArgs> { return this._SuspendLayout; }

    onSuspendLayout(dockManager: DockManager) { this._SuspendLayout.Invoke(new LayoutEventArgs(dockManager)); }

    private _ResumeLayout = new MulticastDelegate<DockListener,LayoutEventArgs>(this);
    public get ResumeLayout(): IMulticastDelegate<DockListener,LayoutEventArgs> { return this._ResumeLayout; }

    onResumeLayout(dockManager: DockManager) { this._ResumeLayout.Invoke(new LayoutEventArgs(dockManager)); }
}

/**
 *
 */
export class WidgetHost
{
    public Widgets: KnockoutObservableArray<WidgetContainer> = _ko.observableArray<WidgetContainer>();

    private _DockListener: DockListener;
    public get DockListener()
    {
        return this._DockListener;
    }

    public DockManager: DockManager;

    private _Initialized = new MulticastDelegate<WidgetHost,EventArgs>(this);
    public get Initialized(): MulticastDelegate<WidgetHost,EventArgs>
    {
        return this._Initialized;
    }

    /**
     *
     */
    constructor()
    {
    }

    /**
     *
     * @param manager
     * @constructor
     */
    public Initialize(manager: HTMLDivElement): void
    {
        let dockManager = this.DockManager = new DockManager(manager);

        dockManager.initialize();
        // Let the dock manager element fill in the entire screen
        var onResized = (e) =>
        {
            this.DockManager.resize(
                window.innerWidth - (manager.clientLeft + manager.offsetLeft),
                window.innerHeight - (manager.clientTop + manager.offsetTop));
        };

        window.onresize = onResized;
        onResized(null);

        this._DockListener = new DockListener();
        dockManager.addLayoutListener(this._DockListener);

        this._Initialized.Invoke();
    }

    /**
     *
     * @param elementID
     * @param title
     * @returns {WidgetContainer}
     * @constructor
     * @param icon
     * @param className
     */
    public Add(elementID: string, title?: string, icon?: string, className?: string): WidgetContainer
    {
        let widget = new WidgetContainer(elementID, this, title, icon, className);
        this.Widgets.push(widget);
        return widget;
    }

    /**
     *
     * @param container
     * @returns {T} WidgetContainer
     * @constructor
     * @param elementID
     * @param title
     * @param icon
     * @param className
     */
    public Create<T extends WidgetContainer>(
        container: {new(...any): T;},
        elementID?: string,
        title?: string,
        icon?: string,
        className?: string)
    {
        let widget = new container(this, elementID, title, icon, className);
        this.Widgets.push(widget);
        return widget;
    }

    /**
     *
     * @param type
     * @returns {WidgetContainer}
     * @constructor
     * @param elementID
     */
    public Find(type: ()=>WidgetContainer, elementID?: string)
    {
        let widget: WidgetContainer = null;
        _ko.utils.arrayForEach<WidgetContainer>(this.Widgets(), function (_widget: WidgetContainer)
        {
            // Search for type and compare element IDs if provided.
            if (_widget instanceof type && !elementID || _widget.ElementID == elementID)
                widget = _widget;
        });
        return widget;
    }

    public Dock()
    {
        _ko.utils.arrayForEach<WidgetContainer>(this.Widgets(), function (_widget)
        {
            _widget.Initialize();
            _widget.Dock();
        });
        _ko.utils.arrayForEach<WidgetContainer>(this.Widgets(), function (_widget)
        {
            _widget.AutoResize(_widget.PanelContainer.elementContent);
        });
    }
}