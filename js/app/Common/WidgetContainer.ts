/**
 * Created by Cosmo on 30.08.2016.
 */
/// <reference path="../../typings/tsd.d.ts"/>

import dockspawn = require('dockspawn');
import {WidgetHost} from './WidgetHost';
import * as Utility from './../Core/Utility';
import {MulticastDelegate, IMulticastDelegate, EventArgs} from './../Core/MulticastDelegate';

export class PanelContainerBase extends dockspawn.PanelContainer
{
    elementContent: HTMLElement;
    dockManager: WidgetHost;
    title: string;

    constructor(
        elementID: string, dockManager: WidgetHost, title: string)
    {
        var element = <HTMLElement> document.getElementById(elementID);
        if (!element)
            throw new Error("Widget container element not found: '" + elementID + "'");

        super(element, dockManager.DockManager, title);
    }
}

export class PanelClosedEventArgs extends EventArgs
{
    constructor(public MouseEvent: MouseEvent)
    {
        super();
    }
}
export class PanelSelectEventArgs extends EventArgs
{
    constructor(public TabPage: dockspawn.TabPage)
    {
        super();
    }
}

export class PanelContainer extends PanelContainerBase
{
    private _closed = new MulticastDelegate<PanelContainer,PanelClosedEventArgs>(this);

    public get Closed(): IMulticastDelegate<PanelContainer,PanelClosedEventArgs>
    {
        return this._closed
    }

    onCloseButtonClicked(e: MouseEvent): void
    {
        super.onCloseButtonClicked(e);
        this.TriggerClosed(e);
    }

    public TriggerClosed(e: MouseEvent)
    {
        this._closed.Invoke(new PanelClosedEventArgs(e))
    }

    private _selected = new MulticastDelegate<PanelContainer, PanelSelectEventArgs>(this);

    public get Selected(): IMulticastDelegate<PanelContainer, PanelSelectEventArgs>
    {
        return this._selected;
    }

    onSelected(page: dockspawn.TabPage): void
    {
        this._selected.Invoke(new PanelSelectEventArgs(page));
    }
}

/**
 *
 */
export class WidgetContainer
{
    DockParent: ()=>WidgetContainer;
    DockNode: dockspawn.DockNode;

    public ElementID: string;
    public WidgetHost: WidgetHost;
    public Title: string;
    public Icon: string;
    public ClassName: string;
    public IsDefault: boolean;
    public View: string;

    private _PanelContainer: PanelContainer;
    public get PanelContainer(): PanelContainer
    {
        return this._PanelContainer;
    }

    private _Initialized = new MulticastDelegate<WidgetContainer,EventArgs>(this);
    public get Initialized(): MulticastDelegate<WidgetContainer,EventArgs>
    {
        return this._Initialized;
    }

    /**
     *
     * @param elementID
     * @param dockManager
     * @param title
     * @param icon
     * @param className
     * @param isDefault
     */
    constructor(
        elementID: string, dockManager: WidgetHost, title: string, icon: string, className: string,
        isDefault: boolean = true)
    {
        this.ElementID = elementID;
        this.WidgetHost = dockManager;
        this.Title = title;
        this.Icon = icon;
        this.ClassName = className;
        this.IsDefault = isDefault;
        this.View = Utility.getClassName(this);
    }

    public Initialize(initEventHandlerCallback?: Function): void
    {
        this._PanelContainer = new PanelContainer(this.ElementID, this.WidgetHost, this.Title);
        this._PanelContainer.setTitleIcon(this.Icon);

        if (initEventHandlerCallback)
            initEventHandlerCallback();

        this._PanelContainer.Selected.Subscribe((page) =>
        {
            // TODO: Create selected Event
            this.AutoResize(this.PanelContainer.elementContent);
        });
        /*this._PanelContainer.Closed.Subscribe((page) =>
         {
         // TODO: Create closed Event
         });*/

        this._PanelContainer.Closed.Subscribe((e)=>
        {
            this._PanelContainer = null;
            this.DockNode = null;
            this.WidgetHost.Widgets.destroy(this);
        });
        this.OnInitialized(this);
    }

    public OnInitialized(sender: WidgetContainer): void
    {
        this.WidgetHost.DockListener.ResumeLayout.Subscribe((manager)=>
        {
            this.AutoResize(this.PanelContainer.elementContent);
        });
        this._Initialized.Invoke();
    }

    public AutoResize(view?: HTMLElement): void
    {
        let div = $('#' + this.ElementID);

        if (!div)
            return;
        let $view = $(view);
        div.css({width: $view.width(), height: $view.height()});
    }

    /**
     * Returns the dock parent node for this instance.
     * @param container
     * @returns dockspawn.DockNode
     * @constructor
     */
    private FindDockParentNode(container?: WidgetContainer)
    {
        // Return the container dock node if specified.
        if (container) {
            // Break if container was not jet added
            if (!container.DockNode)
                throw new Error("Widget '" + this.Title + "' cannot docked to widget '" + container.Title + "' because it is itself not docked.");
            return container.DockNode;
        }

        // Get dock parent if defined in this instance.
        var dockParent;
        if (this.DockParent)
            dockParent = this.WidgetHost.Find(<any>this.DockParent);

        // Return dock managers dock node if no dock parent was found.
        return dockParent
            ? dockParent.DockNode
            : this.WidgetHost.DockManager.context.model.documentManagerNode;
    }

    /**
     *
     * @param container
     * @param ratio
     * @returns {DockNode}
     */
    dockLeft(container?: WidgetContainer, ratio?: number): dockspawn.DockNode
    {
        return this.DockNode = this.WidgetHost.DockManager.dockLeft(this.FindDockParentNode(container),
            this._PanelContainer,
            ratio);
    }

    /**
     *
     * @param container
     * @param ratio
     * @returns {DockNode}
     */
    dockRight(container?: WidgetContainer, ratio?: number): dockspawn.DockNode
    {
        return this.DockNode = this.WidgetHost.DockManager.dockRight(this.FindDockParentNode(container),
            this._PanelContainer,
            ratio);
    }

    /**
     *
     * @param container
     * @param ratio
     * @returns {DockNode}
     */
    dockUp(container?: WidgetContainer, ratio?: number): dockspawn.DockNode
    {
        return this.DockNode = this.WidgetHost.DockManager.dockUp(this.FindDockParentNode(container),
            this._PanelContainer,
            ratio);
    }

    /**
     *
     * @param container
     * @param ratio
     * @returns {DockNode}
     */
    dockDown(container?: WidgetContainer, ratio?: number): dockspawn.DockNode
    {
        return this.DockNode = this.WidgetHost.DockManager.dockDown(this.FindDockParentNode(container),
            this._PanelContainer,
            ratio);
    }

    /**
     *
     * @param container
     * @returns {DockNode}
     */
    dockFill(container?: WidgetContainer): dockspawn.DockNode
    {
        return this.DockNode = this.WidgetHost.DockManager.dockFill(this.FindDockParentNode(container),
            this._PanelContainer);
    }

    /**
     * Default dock operation.
     */
    public Dock()
    {
        this.AutoResize(this._PanelContainer.elementContent);
    }
}
