/// <reference path="../../typings/tsd.d.ts"/>

import {AjaxSettings, AjaxRequest, AjaxErrorHandler, AjaxErrorHandlerConstructor} from "../Core/AjaxRequest";

export class AppPackageRequest extends AjaxRequest<any,AppPackageRequestErrorHandler>
{
    constructor()
    {
        super('App Package Request', baseDir + 'package.json');
        this._Settings.method = AjaxSettings.Methods.GET;
        this._Settings.dataType = AjaxSettings.DataTypes.json;
        this._Cached = true;

        // Initialize base with the deferred JQueryXHR object.
        super.Initialize($.ajax(this._Settings.url, this._Settings));
    }

    protected ErrorHandler(): AjaxErrorHandlerConstructor {
        return AppPackageRequestErrorHandler;
    }
}

export class AppPackageRequestErrorHandler extends AjaxErrorHandler<AppPackageRequest>
{
    public ShowMessage(message: string, title: string): void {
        message = '<blockquote>Error while reading app package from ' + this._Source.Settings.url + '.</blockquote>' + message;
        return super.ShowMessage(message, title);
    }
}