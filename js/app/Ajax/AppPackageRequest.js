/// <reference path="../../typings/tsd.d.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../Core/AjaxRequest"], function (require, exports, AjaxRequest_1) {
    "use strict";
    var AppPackageRequest = (function (_super) {
        __extends(AppPackageRequest, _super);
        function AppPackageRequest() {
            _super.call(this, 'App Package Request', baseDir + 'package.json');
            this._Settings.method = AjaxRequest_1.AjaxSettings.Methods.GET;
            this._Settings.dataType = AjaxRequest_1.AjaxSettings.DataTypes.json;
            this._Cached = true;
            // Initialize base with the deferred JQueryXHR object.
            _super.prototype.Initialize.call(this, $.ajax(this._Settings.url, this._Settings));
        }
        AppPackageRequest.prototype.ErrorHandler = function () {
            return AppPackageRequestErrorHandler;
        };
        return AppPackageRequest;
    }(AjaxRequest_1.AjaxRequest));
    exports.AppPackageRequest = AppPackageRequest;
    var AppPackageRequestErrorHandler = (function (_super) {
        __extends(AppPackageRequestErrorHandler, _super);
        function AppPackageRequestErrorHandler() {
            _super.apply(this, arguments);
        }
        AppPackageRequestErrorHandler.prototype.ShowMessage = function (message, title) {
            message = '<blockquote>Error while reading app package from ' + this._Source.Settings.url + '.</blockquote>' + message;
            return _super.prototype.ShowMessage.call(this, message, title);
        };
        return AppPackageRequestErrorHandler;
    }(AjaxRequest_1.AjaxErrorHandler));
    exports.AppPackageRequestErrorHandler = AppPackageRequestErrorHandler;
});
//# sourceMappingURL=AppPackageRequest.js.map