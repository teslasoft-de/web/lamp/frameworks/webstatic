/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : minify.js
 Created    : 2016-08-30 18:15:18 +0200
 Updated    : 2016-08-30 18:15:18 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */

/*!
 * minify.js - grunt-contrib-uglify task proxy
 * Minify automates grunt uglify task creation by using target names as task names
 * and enables file names and more properties in generated file banners
 * Requires uglify-save-license
 * Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * Usage:
 *  # Specify default options
 *  require('./minify.js')(grunt, {
 *      dest: 'dist',
 *      map: true
 *  });
 *
 *  # Using default options
 *  minify: {
 *      ## Single Source (TARGET without extension. *.min.js will be appended automatically, if specified minified thinks it is multi source!)
 *      'TARGET': ['SOURCE.js'], // -> TARGET.min.js
 *      ### Example
 *      'dockspawn.amd': ['vendor/dockspawn.amd.js'],
 *
 *      ## Multisource (TARGET with extension, if not specified minified thinks it is single source!)
 *      'TARGET.min.js': ['SOURCE.js,PART1,PART2,PART3,...'],
 *      ### Example
 *      '<%= outputDir %><%= outputFile %>.min.js': ['<%= concat.dist.dest %>,<%= concat.dist.src %>'],
 *  },
 *  # Overwrite default options
 *  minify: {
 *      SINGLE_SOURCE:{
 *          options: {
 *              dest: 'vendor',
 *              map: true
 *          },
 *          src:['SOURCE.js'],
 *          dest:'TARGET'  // -> TARGET.min.js
 *      },
 *      MULTISOURCE_SOURCE:{
 *          options: {
 *              dest: 'vendor',
 *              map: true
 *          },
 *          src:['SOURCE.js,PART1,PART2,PART3,...'],
 *          dest:'TARGET.min.js'
 *      },
 *  }
 */

/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

import grunt = require("grunt");
import ITask = grunt.task.ITask;
import IProjectConfig = grunt.config.IProjectConfig;
import ITaskOptions = grunt.task.ITaskOptions;
import IFilesConfig = grunt.file.FilesConfig;

import {GruntConfig} from "./GruntConfig";
import {GruntTask, IGruntTask} from "./GruntTask";

module Grunt
{
    export interface MinifyOptions extends ITaskOptions
    {
        dest: string,
        map: boolean
    }

    class Minify extends GruntTask<GruntConfig,MinifyOptions>
    {
        constructor(config: GruntConfig, options: MinifyOptions)
        {
            super('minify', 'Minify multiple sourced files and preserve headers.', config, options);
        }

        protected run(task: grunt.task.IMultiTask<ITask>): void
        {
            // evaluate possible template strings in target name
            var target: string = <any>grunt.template.process(task.target),
                targetKey = target,
                multiSource = target.match(/\.min\.js$/),
                match = target.match(/(.*[/])*([^/]*)\.(\w+)*$/);
            // avoid invalid target identifiers
            if (match)
                targetKey = ((match[1] || '') + match[2]).replace(/[./_]/g, '-');

            // iterate through target files (tasks)
            task.files.forEach((file: IFilesConfig) => {

                let // if file is minified get sources array
                    source = multiSource ? file.orig.src[0].split(',') : file.src,
                    filepath = multiSource ? source[0] : file.orig.src[0];

                if (!grunt.file.exists(filepath))
                    grunt.fail.fatal('Source file "' + filepath + '" not found.', 1);

                let // get first element (source)
                    sourcePath = multiSource ? source[0] : file.src[0],
                    // use the target name if specified otherwise create
                    targetPath = multiSource ? target : sourcePath.replace(/^.*\/(.*)\.js$/,
                        this.options.dest + '$1.min.js');

                // store some information about this file in config
                grunt.config('uglifyTargets.' + targetKey, {
                    path    : multiSource ? sourcePath[0] : sourcePath,
                    filename: multiSource
                        ? source.slice(1).map((path: string) => {
                        return path.split('/').pop()
                    }).join(', ')
                        : sourcePath.split('/').pop()
                });

                // set uglify banner options
                let uglifyOptions: IProjectConfig = {
                    preserveComments: require('uglify-save-license'),
                    banner          : '<% var subtask = uglifyTargets[grunt.task.current.target]; %>' +
                    '/*! <%= subtask.filename %>, <%= pkg.homepage %> <%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd, HH:MM") %>) */\n'
                };

                // set uglify sourcemap options
                if (this.options.map) {
                    let mapOptions: IProjectConfig = {
                        sourceMap              : function (path: string) {
                            return path.replace(/.js$/, ".map")
                        },
                        sourceMapIncludeSources: true
                        //sourceMapIn : 'dist/<%= pkg.name %>.js.map'
                    };
                    for (let _option in mapOptions)
                        uglifyOptions[_option] = mapOptions[_option];
                }
                grunt.config('uglify.options', uglifyOptions);

                // create and run an uglify target for this file
                grunt.config('uglify.' + targetKey + '.files', [{
                    src : [sourcePath],
                    dest: targetPath
                }]);
                grunt.task.run('uglify:' + targetKey);
            });
        }
    }
    export var minify: IGruntTask = GruntTask.init(Minify);
}
export = Grunt