/*
 This file and its contents are limited to the author only.
 See the file "LICENSE" for the full license governing this code.
 Differing and additional copyright notices are defined below.
 ----------------------------------------------------------------
 Project    : WebStatic - 0.8.9
 File       : banner.js
 Created    : 2016-08-30 18:29:08 +0200
 Updated    : 2016-08-30 18:15:18 +0200

 Author     : Christian Kusmanow <christian.kusmanow@teslasoft.de>
 Company    : Teslasoft
 Copyright  : © 2016 Teslasoft, Christian Kusmanow
 */

/*!
 * banner.js - grunt-banner task proxy
 * This plugin updates file headers and preserves the original timestamps.
 * Author: Christian Kusmanow <christian.kusmanow@teslasoft.de>
 *
 */

/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

//const util = require('util');
import fs = require('fs');
import {Stats} from "fs";
import moment = require('moment');
import grunt = require("grunt");
import ITask = grunt.task.ITask;
import IFilesConfig = grunt.file.FilesConfig;
import ITaskOptions = grunt.task.ITaskOptions;

import {GruntConfig} from "./GruntConfig";
import {GruntTask, IGruntTask} from "./GruntTask";

module Grunt
{
    export interface BannerOptions extends ITaskOptions
    {
    }

    class Banner extends GruntTask<GruntConfig,BannerOptions>
    {
        constructor(config: GruntConfig, options: BannerOptions)
        {
            super('banner', 'Create and execute usebanner tasks.', config, options);
            config._ = ['grunt-banner'];
        }

        protected run(task: grunt.task.IMultiTask<ITask>): void
        {
            // get (extended) options
            //var options = <BannerOptions> task.options();

            let target = task.target;
            // Fetch Grunt Config
            let pkg = <any>grunt.config.get('pkg');
            let author = pkg.authors[0]; // TODO: Make authors configurable.

            // File
            let targetKey: string,
                name: string,
                stats: Stats,
                created: string,
                modified: string;

            this.filter((file) => {

                targetKey = file;
                let match = file.match(/(.*[/])*([^/]*)\.(\w+)*$/);

                // avoid invalid target identifiers
                if (match)
                    targetKey = ((match[1] ? match[1] : '') + match[2]).replace(/[./_]/g, '-');

                targetKey = target + '-' + targetKey;

                name = file.split('/').pop();
                stats = fs.statSync(file);

                //grunt.log.write(util.inspect(stats));
                //grunt.log.write(util.inspect(stats.mtime));

                created = moment(stats.birthtime).format('YYYY-MM-DD HH:mm:ss ZZ');
                modified = moment(stats.mtime).format('YYYY-MM-DD HH:mm:ss ZZ');

                grunt.config('usebanner.' + targetKey, {
                    options: {
                        position : 'replace',
                        replace  : true,
                        banner   : '/*\n' +
                        ' This file and its contents are limited to the author only.\n' +
                        ' See the file "LICENSE" for the full license governing this code.\n' +
                        ' Differing and additional copyright notices are defined below.\n' +
                        ' ----------------------------------------------------------------\n' +
                        '    Project    : <%= pkg.name %> - <%= pkg.version %>\n' +
                        '    File       : ' + name + '\n' +
                        '    Created    : ' + created + '\n' +
                        '    Updated    : ' + modified + '\n' +
                        '\n' +
                        '    Author     : ' + author.name + ' <' + author.email + '>\n' +
                        '    Company    : ' + author.company + '\n' +
                        '    Copyright  : © <%= grunt.template.today("yyyy") %> ' + author.company + ', ' + author.name + '\n' +
                        '*/\n',
                        linebreak: true
                    },
                    files  : {
                        src: [file]
                    }
                });
                grunt.task.run('usebanner:' + targetKey);

                grunt.registerTask('usebanner-' + targetKey, function () {

                    //stats = fs.statSync(file);
                    //runt.log.write(util.inspect(stats.mtime));

                    // Reset file timestamps
                    fs.utimesSync(file, stats.atime, stats.mtime);

                    //stats = fs.statSync(file);
                    //grunt.log.write(util.inspect(stats.mtime));
                });
                grunt.config('usebanner-' + targetKey + '.' + 'mtime', {});
                grunt.task.run('usebanner-' + targetKey);
            });
        };
    }
    export var banner: IGruntTask = GruntTask.init(Banner);
}
export = Grunt