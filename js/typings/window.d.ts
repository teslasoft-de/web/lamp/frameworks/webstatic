interface Window{
    webkitRequestAnimationFrame: (callback:FrameRequestCallback) => number;
    mozRequestAnimationFrame: (callback:FrameRequestCallback) => number;
    oRequestAnimationFrame: (callback:FrameRequestCallback) => number;
}
declare var window:Window;