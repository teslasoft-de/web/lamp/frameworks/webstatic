/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

module Grunt
{
    export class WebStatic
    {
        constructor(grunt: grunt)
        {
            const config = new (require('./GruntConfig').GruntConfig)(
                grunt, 'lib/', 'dist/', 'WebStatic');

            config._banner(config._sourceDir('Core/*.ts'));
            config._jshint();
            config._jscs();
            config._concat(config._sourceDir([
                'Core/AnimationFrame.js',
                'blur.js',
                'plugins.jquery.js',
                'eventFreezer.jquery.js',
                'disableScroll.jquery.js',
                'slide-menu.jquery.js',
                'parallax.jquery.js',
                'smoothscroll.jquery.js',
                'smoothwheel.jquery.js',
                'tooltip.jquery.js',
                'gmap.js']));

            config._bowercopy({
                destPrefix: 'img/',
                // Remove downloaded bower components after copying dist contents
                //clean: true
            }, {
                Apaxy: {
                    'icons/': 'Apaxy/apaxy/theme/icons/'
                }
            });
            config._minify({'<%= outDir %><%= outFile %>.min.js': ['<%= concat.dist.dest %>,<%= concat.dist.src %>']});
            config._watch();

            config.init();

            grunt.registerTask('build_dependencies', ['bowercopy:Apaxy']);

            grunt.registerTask('filebanner', ['banner']);
            grunt.registerTask('build', ['jshint', 'concat', 'minify', 'watch']);
            //grunt.registerTask('default', ['concurrent:check', 'concurrent:build']);
        }
    }
}
export = Grunt.WebStatic;