# 3rd Party Tools
# Load autoprefixer
require 'autoprefixer-rails'
# Load conditional styles processor
require 'jacket'

# Require any additional compass plugins here.
require 'compass/import-once/activate'

# Set this to the root of your project when deployed:
http_path = "../"
css_dir = http_path + "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"
http_fonts_dir = "fonts"
fonts_dir = http_path + http_fonts_dir

cache_path = "x:/.sass-cache"

environment = :dev
#environment = :production

# You can select your preferred output style here (can be overridden via the command line):
#output_style = :expanded
#output_style = :nested
#output_style = :compact
output_style = (environment == :production) ? :compressed : :compact

# Enable sourcemaps on everythig but production
sourcemap = (environment == :production) ? false : true

# Enable sourcemaps on debian
#sourcemap = true

# Enable sourcemaps on windows
enable_sourcemaps = true

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
