/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

module Grunt
{
    import IProjectConfig = grunt.config.IProjectConfig;
    export class App
    {
        constructor(grunt: grunt)
        {
            const GruntConfig = require('./GruntConfig').GruntConfig,
                config = new GruntConfig(grunt, 'app/', 'vendor/', null),
                app = config._sourceDir,
                vendor = config._outDir,
                bower = config.Dir('bower', 'bower_components/');

            config._banner();

            config._bowercopy({
                destPrefix: vendor(''),
                // Remove downloaded bower components after copying dist contents
                //clean: true
            }, {
                durandal            : {
                    '../compass/sass/modules/layout/_durandal.scss': 'Durandal/css/durandal.css'
                },
                dockspawn           : {
                    //'jquery-<%= bwr.dependencies.jquery %>.min.js': 'jquery/dist/jquery.min.js',
                    '../img/'                                       : 'dock-spawn/js/out/images/',
                    '../compass/sass/modules/layout/_infovis.scss'  : 'dock-spawn/js/out/demos/ide/infovis/jit-base.css',
                    '../compass/sass/modules/layout/_spacetree.scss': 'dock-spawn/js/out/demos/ide/infovis/Spacetree.css'
                },
                elusiveicons         : {
                    '../fonts/'                                                           : 'elusive-icons/fonts/',
                    '../compass/sass/fonts/vector-icons/elusive-icons/'                   : 'elusive-icons/scss/_*',
                    '../compass/sass/fonts/vector-icons/elusive-icons/_elusive-icons.scss': 'elusive-icons/scss/elusive-icons.scss'
                },
                fontawesome        : {
                    '../fonts/'                                                         : 'font-awesome/fonts/',
                    '../compass/sass/fonts/vector-icons/font-awesome/'                  : 'font-awesome/scss/_*',
                    '../compass/sass/fonts/vector-icons/font-awesome/_font-awesome.scss': 'font-awesome/scss/font-awesome.scss'
                },
                bootstrap           : {
                    '../fonts/'                                    : 'bootstrap-sass/assets/fonts',
                    '../compass/sass/vendor/bootstrap/stylesheets/': 'bootstrap-sass/assets/stylesheets/'
                },
                bootstrap_bootswatch: {

                    '../compass/sass/vendor/bootstrap/stylesheets/bootstrap/_variables.scss'                  : 'bootswatch/superhero/_variables.scss',
                    '../compass/sass/vendor/bootstrap/stylesheets/bootstrap/themes/superhero/_bootswatch.scss': 'bootswatch/superhero/_bootswatch.scss'
                },
                bootstrap_select    : {
                    '../compass/sass/vendor/bootstrap/plugins/_bootstrap-select.scss': 'bootstrap-select/sass/bootstrap-select.scss',
                    '../compass/sass/vendor/bootstrap/plugins/_variables.scss'       : 'bootstrap-select/sass/variables.scss'
                },
            });

            config._compass('development', {outputStyle: 'compact', specify: ['app.scss', 'screen.scss']});
            config._compass('production', {outputStyle: 'compressed', specify: ['app.scss', 'screen.scss']});

            config._replace('dockspawn',
                bower('dock-spawn/js/out/js/dockspawn.js'),
                bower('dock-spawn/js/out/js/'),
                {
                    'icon-remove': /icon-remove-sign/g,
                    '="fa fa-': /=\s*"icon-/g
                });

            config._wrap_amd('amd_logsaver', bower('logsaver/logsaver.js'), vendor('logsaver.amd.js'), 'this');
            config._wrap_amd('amd_dockspawn',
                bower('dock-spawn/js/out/js/dockspawn.js'),
                vendor('dockspawn.amd.js'),
                'dockspawn');
            config._wrap_amd('amd_dockspawn_tree_vis',
                bower('dock-spawn/js/out/demos/ide/infovis/dock_tree_vis.js'),
                vendor('dock_tree_vis.amd.js'),
                '{InitDebugTreeVis: InitDebugTreeVis}',
                'var $jit = require("jit");');
            config._wrap_amd('amd_jit', bower('jit/Jit/jit.js'), vendor('jit.amd.js'), '$jit');

            config._jshint();
            config._jscs();

            config._minify({
                'logsaver'     : [vendor('logsaver.amd.js')],
                'dockspawn.amd': [vendor('dockspawn.amd.js')],
                'jit.amd'      : [vendor('jit.amd.js')]
            });

            config._watch(null, ['jshint', 'minify']);
            config._concurrent(null, ['minify', 'watch']);

            config._auto_install('bootstrap_maxlength',bower('bootstrap-maxlength'));
            config._run_grunt('bootstrap_maxlength',bower('bootstrap-maxlength/Gruntfile.js'),['concat', 'uglify']);

            config.init();

            grunt.registerTask('build_dependencies',
                ['bowercopy:fontawesome', 'bowercopy:elusiveicons', 'bowercopy:bootstrap', 'bowercopy:bootstrap_bootswatch',
                 'replace', 'auto_install', 'run_grunt', 'wrap', 'minify', 'compass:production']);

            grunt.registerTask('filebanner', ['banner']);
            grunt.registerTask('build', ['jshint', 'minify', 'watch']);
            //grunt.registerTask('default', ['concurrent:check', 'concurrent:build']);
        }
    }
}
export = Grunt.App;