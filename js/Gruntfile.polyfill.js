/**
 * Created by Cosmo on 14.10.2014.
 */
/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />
"use strict";
var Grunt;
(function (Grunt) {
    var Polyfill = (function () {
        function Polyfill(grunt) {
            var GruntConfig = require('./GruntConfig').GruntConfig, config = new GruntConfig(grunt, 'lib/Core/', 'dist/'), core = config._sourceDir, dist = config._outDir, bower = config.Dir('bower', 'bower_components/');
            config.init();
            config.extend({
                uglify: {
                    options: {
                        preserveComments: 'some'
                    },
                    tests: {
                        files: {
                            'bower_components/conditionizr/dist/detects.min.js': [bower('conditionizr/src/detects.js')]
                        }
                    },
                    polyfill: {
                        files: {
                            'bower_components/modernizr/modernizr.min.js': [bower('modernizr/modernizr.js')],
                            'dist/Core/Polyfill.min.js': ['lib/Core/Polyfill.js']
                        }
                    }
                },
                concat: {
                    options: {
                        separator: '\r\n\r\n'
                    },
                    tests: {
                        options: {
                            stripBanners: { block: true }
                        },
                        src: bower('conditionizr/detects/*.js'),
                        dest: bower('conditionizr/src/detects.js')
                    },
                    polyfill: {
                        src: [
                            bower('modernizr/modernizr.js'),
                            bower('respond/dest/respond.src.js'),
                            bower('conditionizr/dist/conditionizr.js'),
                            bower('conditionizr/detects/*.js'),
                            core('Polyfill.js')],
                        dest: dist('WebStatic.polyfill.js'),
                        nonull: true
                    },
                    polyfill_min: {
                        src: [
                            bower('modernizr/modernizr.min.js'),
                            bower('respond/dest/respond.min.js'),
                            bower('conditionizr/dist/conditionizr.min.js'),
                            bower('conditionizr/dist/detects.min.js'),
                            core('Polyfill.min.js')],
                        dest: dist('WebStatic.polyfill.min.js'),
                        nonull: true
                    },
                    vendor_html5shiv: {
                        src: [
                            bower('html5shiv/dist/html5shiv-printshiv.min.js')],
                        dest: 'vendor/html5shiv-printshiv.min.js',
                        nonull: true
                    }
                }
            });
            grunt.registerTask('tests', ['concat:tests', 'uglify:tests']);
            grunt.registerTask('polyfill', ['uglify:polyfill', 'concat:polyfill', 'concat:polyfill_min']);
            grunt.registerTask('vendor', ['concat:vendor_html5shiv']);
        }
        return Polyfill;
    }());
    Grunt.Polyfill = Polyfill;
})(Grunt || (Grunt = {}));
module.exports = Grunt.Polyfill;
//# sourceMappingURL=Gruntfile.polyfill.js.map