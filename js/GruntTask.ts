/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

//const util = require('util');
import grunt = require("grunt");
import ITask = grunt.task.ITask;
import IProjectConfig = grunt.config.IProjectConfig;
import FilesConfig = grunt.file.FilesConfig;
import FileModule = grunt.file.FileModule;
import ITaskOptions = grunt.task.ITaskOptions;

import {GruntConfig} from "./GruntConfig";

module Grunt
{
    export interface IGruntTask {
        <TConfig extends GruntConfig, TOptions extends ITaskOptions>(config: TConfig, options?: TOptions): void;
    }
    export interface Task {
        new (config: GruntConfig, options: ITaskOptions): GruntTask<GruntConfig,ITaskOptions>
    }

    export abstract class GruntTask<TConfig extends GruntConfig, TOptions extends ITaskOptions>
    {
        private _task: grunt.task.IMultiTask<ITask>;

        get name(): string { return this._name; }

        get description(): string { return this._description; }

        get task() { return this._task; }

        get config(): TConfig { return this._config; }

        /**
         * Retuns (extended) task options
         * @returns {TOptions}
         */
        get options(): TOptions { return <TOptions>this._task.options(); }

        /**
         * Sets the options object of this task within the grunt config.
         * @param value
         */
        set options(value: TOptions) { (<IProjectConfig>this._config)[this._name]['options'] = value; }

        constructor(
            protected _name: string,
            protected _description: string,
            protected _config: TConfig,
            options: TOptions)
        {
            // set default options
            this.options = options;
        }

        public static init(task: Task): IGruntTask
        {
            return function <TConfig extends GruntConfig, TOptions extends ITaskOptions>(
                config: TConfig,
                options: TOptions)
            {
                let _task = new task(config, options);
                // register task
                grunt.registerMultiTask(_task._name, _task._description, function () {
                    ((task: grunt.task.IMultiTask<ITask>) => {
                        _task._init(task);
                    })(this)
                });
            }
        }

        public _init(task: grunt.task.IMultiTask<ITask>): void
        {
            this._task = task;
            this.run(task);
        }

        protected abstract run(task: grunt.task.IMultiTask<ITask>): void;

        protected filter(callback: (file: string)=>void)
        {
            // iterate through target files (tasks)
            return this.task.files.forEach((file: FilesConfig) => {
                (<grunt.file.IFilesArray>(file.src.length > 0 ? file.src : file.orig.src).filter((filepath: string) => {
                    //grunt.log.write(util.inspect(filepath));
                    if (!grunt.file.exists(filepath)) {
                        grunt.log.warn('Source file "' + filepath + '" not found.');
                        return false;
                    }
                    return true;
                })).forEach(callback);
            });
        }
    }
}
export = Grunt